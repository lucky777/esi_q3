<img src="./img/lucky777.png" width="77" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">lucky777</p>

# DON3 synthèse

# Create

```sql
-- CREATE TABLE STUDENT WITH ID, NAME? GRADE, SECTION AND GROUP
CREATE TABLE student(
    s_id CHAR(5) NOT NULL CONSTRAINT s_id_pk PRIMARY KEY,
    s_name VARCHAR(30) NOT NULL,
    s_grade INTEGER NOT NULL DEFAULT 1
                CONSTRAINT s_valid_grade_CK CHECK(s_grade IN [1, 2, 3]),
    s_section CHAR(1) NOT NULL
                CONSTRAINT s_valid_section_CK
                CHECK(s_section IN ['G', 'I', 'R']),
    s_group CHAR(4) NOT NULL,
    CONSTRAINT s_valid_group CHECK(
        (s_grade=1 AND (s_group='A%' OR s_group='B%')) OR
        (s_grade=2 AND (s_group='C%' OR s_group='D%')) OR
        (s_grade=3 AND (s_group='E%' OR s_group='F%'))
    )
);
```

```sql
-- CREATE TABLE RESULT WITH ID, SNUM, SUBJECT, NUMERATOR, DENOMINATOR
CREATE TABLE result(
    r_id CHAR(3) NOT NULL CONSTRAINT r_id_PK PRIMARY KEY,
    r_sno CHAR(5) NOT NULL CONSTRAINT s_sno_FK REFERENCES student(s_id),
    r_subject VARCHAR(5) NOT NULL, -- Subject of lesson
    r_num INTEGER NOT NULL DEFAULT 0, -- numerator of result
    r_den INTEGER NOT NULL DEFAULT 20, -- denominator of result
    CONSTRAINT r_num_CK CHECK(r_num <= r_den) -- 21/20 invalid
);
```

```sql
-- CREATE A USELESS TABLE JUST TO DELETE IT AFTER ;-)
CREATE TABLE useless(
    useless_attr INTEGER,
    useless_attr2 INTEGER
);
```

<div style="page-break-after: always;"></div>

# Alter

```sql
-- DELETE COLUMN
ALTER TABLE useless DROP COLUMN useless_attr2;
-- DELETE TABLE
DROP TABLE useless;
-- ADD CONSTRAINT
ALTER TABLE result ADD CONSTRAINT r_note_positive_CK CHECK(r_num >= 0);
-- INSERT LINES
INSERT INTO student VALUES('57444', 'Caillou Imparfait', 2, 'R', 'C211');
INSERT INTO student VALUES('56777', 'lucky muphin', 2, 'R', 'C211');
INSERT INTO result VALUES('001', '57444', 'DON3', 16);
INSERT INTO result VALUES('002', '56777', 'CAI2', 18);
INSERT INTO result VALUES('003', '56777', 'ATLR5', 11);
-- DELETE LAST INSERTED LINE
DELETE FROM result WHERE r_id = '003';
COMMIT; -- SAVE DATA
```

# View

```sql
-- CREATE VIEW WHERE CAILLOU CAN ONLY SEE HIS RESULTS
CREATE VIEW Caillou AS
    SELECT * FROM result WHERE r_sno = '57444';
```

# Grant

```sql
-- GIVE EVERYONE ALL PERMISSIONS
GRANT ALL ON student TO Public;
-- ALLOW STUDENT g57444 TO SELECT HIS RESULTS
GRANT SELECT ON Caillou TO g57444;
-- DELETE ALL PERMISSIONS
REVOKE ALL ON student TO Public;
-- STUDENT g57444 CAN STILL SELECT HIS RESULTS !!
```

```sql
-- ADD VIEW FOR DEV TEACHERS
CREATE VIEW DEV AS
    SELECT * FROM result WHERE r_subject = 'DEV%'
    WITH CHECK OPTION;
```

<div style="page-break-after: always;"></div>

```sql
-- GIVE ALL PERMISSIONS TO ABSIL ONLY IN DEV VIEW
GRANT ALL ON DEV TO Absil WITH GRANT OPTION;
```

# Function

```sql
CREATE OR REPLACE FUNCTION
                        my_avg(s_id student.s_id%TYPE, subject VARCHAR(5))
        RETURN INTEGER AS
    t result%ROWTYPE; -- ONE LINE OF CURSOR OF TYPE result_ROW
    num INTEGER;
    den INTEGER;
    CURSOR tests IS SELECT *
                    FROM result
                        WHERE (r_sno = s_id AND r_subject = subject);
BEGIN
    num := 0;
    den := 0;
    FETCH tests INTO t; -- FETCH A NEW LINE OF THE CURSOR
    WHILE tests%FOUND LOOP -- WHILE NOT FINISHED CHECK NEW ROW
        num := num + t.r_num;
        den := den + t.r_den;
        FETCH tests INTO t;
    END LOOP;
    CLOSE tests;
    RETURN num*20/den; -- RETURN AVERAGE OF ALL RESULTS
END;
```

> `NOTE:` Instead of:
> ```sql
> FETCH tests INTO t;
>     WHILE tests%FOUND LOOP
>         num := num + t.r_num;
>         den := den + t.r_den;
>         FETCH tests INTO t;
>     END LOOP;
>     CLOSE tests;
> ```
> We could have used a for each:
> ```sql
> FOR t IN tests LOOP
>   num := num + t.r_num;
>   den := den + t.r_den;
> END LOOP;
> ```

# Trigger

```sql
-- WITH THIS TRIGGER, EVERYONE WILL PASS IN DEV ;-)
CREATE OR REPLACE TRIGGER self_defence
BEFORE OF UPDATE OF DEV OR INSERT ON DEV
FOR EACH ROW
    IF NEW.num < NEW.den/2 THEN -- IF SOMEONE FAILED A TEST
        NEW.num := NEW.den/2    -- CHANGE HIS RESULT SO HE PASS
    END IF;
END;
```