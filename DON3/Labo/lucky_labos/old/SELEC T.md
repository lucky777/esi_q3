# SQL

# Labo 1

## 1.

```sql
SELECT *
 FROM gcuv.employe
    WHERE empsal > 85000;
```

## 2.

```sql
SELECT empnom, empdpt
 FROM gcuv.employe
    WHERE upper(empnom) LIKE '%ON%';
```

## 3.

```sql
SELECT count(*)
 FROM gcuv.employe
    WHERE empsexe = 'F';
```

## 4.

```sql
SELECT count(empno), empdpt
 FROM gcuv.employe
    GROUP BY empdpt;
```

## 5.

```sql
SELECT AVG(empsal) sal, empdpt
 FROM gcuv.employe
    GROUP BY empdpt
    HAVING AVG(empsal) > 85000;

SELECT dptno,dptlib,AVG(empsal) FROM adt.departement d
    JOIN adt.employe e ON d.dptno = e.empdpt
    GROUP BY dptno,dptlib
    HAVING AVG(empsal) > 85000;
```

## 6.

```sql
SELECT empnom
 FROM gcuv.employe
    GROUP BY empnom
    HAVING count(empnom) > 1;
```

## 7.

```sql
SELECT dptadm, count(*)
 FROM gcuv.departement
    GROUP BY dptadm
    HAVING count(*) > 2;
```

## 8.

```sql
SELECT count(distinct dptmgr)
 FROM gcuv.departement;
```

## 9.

```sql
SELECT e.empnom, d.dptlib
 FROM gcuv.employe e
    JOIN gcu.departement d ON e.empdpt = d.dptno;
```

## 10.

```sql
SELECT e.empnom
 FROM gcuv.departement d
    JOIN gcuv.employe e ON d.dptmgr = e.empno
```

## 11.

```sql
SELECT e.empnom, d.dptlib
 FROM gcuv.departement d
    JOIN gcuv.employe e ON d.dptmgr = e.empno
```
## 12.

```sql
SELECT empnom
 FROM gcuv.employe
    JOIN gcuv.departement ON empno = dptmgr
    WHERE dptno IN (SELECT dptno FROM gcuv.departement 
                        JOIN gcuv.employe ON dptno = empdpt
                        GROUP BY dptno
                        HAVING COUNT(empno) > 5);
```

## 13.

```sql
SELECT dptlib
 FROM departement
    WHERE dptadm IS NOT NULL;
```

## 14.

```sql
SELECT empnom
 FROM gcuv.employe
    WHERE empsal = (SELECT MAX(empsal) FROM gcuv.employe);
```

## 15.

```sql
SELECT empnom, empsal
 FROM gcuv.employe
    WHERE empsexe LIKE 'F' AND empsal > (SELECT AVG(empsal) FROM gcuv.employe WHERE empsexe LIKE 'M');
```

## 16.

```sql
SELECT dptlib, dptno FROM gcuv.departement d
    JOIN gcuv.employe e ON d.dptno = e.empdpt
    GROUP BY dptlib, dptno
    HAVING COUNT(empno) >= (SELECT COUNT(empno)
                             FROM gcuv.employe emp
                                WHERE emp.empdpt = d.dptno
                                GROUP BY emp.empdpt);
```

## 17.
```sql
SELECT dptlib, dptno FROM gcuv.departement d
    JOIN gcuv.employe e ON d.dptno = e.empdpt
    GROUP BY dptlib, dptno
    HAVING SUM(empsal) >= (SELECT SUM(empsal)
                            FROM gcuv.employe emp
                            WHERE emp.empdpt = d.dptno
                            GROUP BY emp.empdpt);
```

# Labo 2

## 1.

```sql
SELECT COUNT(*) - COUNT(dptadm)
FROM Departement;
```

> Renvoie tous les départements non administrés.

## 2.

```sql
SELECT e.empno, e.empnom
 FROM Employe e
    JOIN Departement ON dptno = e.empdpt
    JOIN Employe m ON dptmgr = m.empno
    WHERE UPPER(m.empnom) = 'MAES';
```

> Renvoie les numéros et noms des employés dont le manager s'appelle `MAES`.

## 3.

```sql
SELECT dptlib, dptno
 FROM Departement
    JOIN Employe ON dptno = empdpt
    WHERE empsexe = 'F'
    GROUP BY dptlib, dptno
    HAVING COUNT(*) >= ALL (SELECT COUNT(*)
                             FROM Employe
                             WHERE empsexe = 'F'
                             GROUP BY empdpt);
```

> Renvoie le numéro et nom de département dans lequel il y a le plus de `femmes`.

## 4.

```sql
SELECT DISTINCT empdpt
 FROM Employe
    WHERE empsal > 110000
    GROUP BY empdpt, empsexe
    HAVING COUNT(*) > 3;
```

> 

# Labo 3

## 1.

```sql
SELECT d1.dptadm, d1.dptlib
 FROM gcuv.departement d1
    LEFT JOIN gcuv.departement d2 ON d1.dptadm = d2.dptno;
```

## 2.

```sql
SELECT dp.dptlib dpt_pas_administrant FROM adt.departement d 
    RIGHT OUTER JOIN adt.departement dp
    ON d.dptadm = dp.dptno
    WHERE d.dptno IS NULL;
```

## 3.

```sql
SELECT d2.dptlib, count(d1.dptno)
 FROM gcuv.departement d1
    RIGHT OUTER JOIN gcuv.departement d2 ON d1.dptadm = d2.dptno
    GROUP BY d2.dptlib;
```

## 4.

# Labo 4

## 1. Nom et prénom des anciennes.
```sql
SELECT nom, prenom
 FROM gcuv.ancien
     WHERE sexe == 2;
```
## 2. Nombre d’anciens résidant à Uccle.
```sql
SELECT count(*)
 FROM gcuv.ancien
     WHERE adLoc == 'Uccle';
```
## 3. Nombre d’anciens par sexe.
```sql
SELECT sexe, COUNT(*) AS NumEmployee
 FROM gcuv.ancien
     GROUP BY sexe
```
## 4. Nom et prénom des anciens résidant en région flamande.
```sql