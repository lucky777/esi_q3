# DON3 Labos

`ATTENTION: Aucune réponse n'a été validée par un enseignant!`

# Labo 1

<img src="./sources/labo1_tables.png" width="500" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">Labo 1: Tables</p>

1. Lister les noms des employés qui gagnent plus de 85 000 unités:

```sql
SELECT empNom
 FROM employe
    WHERE empSal > 85000;
```

2. Lister les noms des employés dont le nom contient “ON” avec leur numéro de département:

```sql
SELECT empNom, empNo
 FROM employe
    WHERE empNom LIKE '%ON%';
```

3. Compter le nombre de femmes employées dans la société:

```sql
SELECT COUNT(*)
 FROM employe
    WHERE empSexe LIKE 'F';
```

4. Donner le nombre d’employés par numéro de département:
> Pour obtenir les départements sans employés dans la liste il est nécessaire d’utiliser une jointure externe, ce qui constitue le sujet du labo3

```sql
SELECT count(emp.employe), dpt.dptNo
 FROM employe emp
    JOIN departement dpt ON emp.empNo = dpt.dptNo
    GROUP BY dpt.dptNo;
```

5. Donner les départements dont la moyenne des salaires dépasse 85 000 unités:

```sql
SELECT dpt.dptNo, AVG(emp.empSal)
 FROM departement dpt
    JOIN employe emp ON dpt.dptNo = emp.empNo
    GROUP BY dpt.dptNo
    HAVING AVG(emp.empSal) > 85000;
```

6. Donner les noms d’employés correspondant à des homographes:

```sql
SELECT emp.empNo
 FROM employe emp
    GROUP BY emp.empNo
    HAVING count(*) > 1;
```

7. Lister les n° de département dirigeant plus de 2 départements:

```sql
SELECT dpt.dptAdm
 FROM departement dpt
    GROUP BY dpt.dptAdm
    HAVING count(*) >= 2;
```

8. Donner le nombre de managers différents:

> La solution la plus simple est fournie par une
expression dialectale d’oracle

```sql
SELECT DISTINCT dptMgr
 FROM departement dpt;
```

9. Lister les noms des employés avec le nom de leur département:

```sql
SELECT emp.empNom, dpt.dptLib
 FROM employe emp
    JOIN departement dpt ON emp.empDpt = dpt.dptNo
```

10. Lister les noms des employés qui sont managers:

```sql
SELECT emp.empNom
 FROM employe emp
    JOIN departement dpt ON emp.empNo = dpt.dptMgr
    GROUP BY dpt.dptMgr;
```

11. Même exercice que 4, mais donnez le libellé du département:

`(Donner le nombre d’employés par numéro de département, avec son libellé)`

```sql
SELECT dpt.dptNo, dpt.dptLib, COUNT(emp.empNo) AS employes
 FROM gcuv.departement dpt
    JOIN gcuv.employe emp ON emp.empDpt = dpt.dptNo
    GROUP BY dpt.dptNo, dpt.dptLib;
```

12. Donner les noms des managers des départements ayant plus de 5 employés:

```sql
SELECT mgr.empNom, COUNT(emp.empNo)
 FROM departement dpt
    JOIN employe mgr ON mgr.empNo = dpt.dptMgr
    JOIN employe emp ON emp.empDpt = dpt.dptNo
    GROUP BY mgr.empNom
    HAVING COUNT(emp.empDpt) > 5;
```

13. Donner les libellés des départements administrés par un autre département:

```sql
SELECT dpt.dptLib
 FROM departement dpt
    WHERE dptAdm IS NOT NULL;
```

14. Donner le nom de(s) l’employé(s) ayant le plus haut salaire:

```sql
SELECT emp.empNom
 FROM employe emp
    WHERE emp.empSal >= (SELECT MAX(sal.empSal)
                          FROM employe sal);
```

15. Donner la liste des femmes gagnant plus que la moyenne des hommes:

```sql
SELECT woman.empNom, woman.empSexe, woman.empSal AS salaire
 FROM employe woman
    WHERE woman.empSexe = 'F'
    AND woman.empSal > (SELECT AVG(man.empSal)
                           FROM employe man
                                WHERE man.empSexe = 'M');
```

16. Donner le libellé du(es) département(s) employant le plus de personnel:

```sql
SELECT dpt.dptNo, dpt.dptLib, COUNT(emp.empNo) AS nombre_enmployes
 FROM departement dpt
    JOIN employe emp ON emp.empDpt = dpt.dptNo
    GROUP BY dpt.dptNo, dpt.dptLib
    HAVING COUNT(emp.empNo) >= (SELECT MAX(COUNT(*))
                                 FROM employe e
                                    JOIN departement d
                                        ON e.empDpt = d.dptNo
                                    GROUP BY d.dptNo);
```

17. Donner le libellé du(es) département(s) ayant la masse salariale la plus élevée:

```sql
SELECT dpt.dptNo, dpt.dptLib, SUM(emp.empSal)
 FROM departement dpt
    JOIN employe emp ON emp.empDpt = dpt.dptNo
    GROUP BY dpt.dptNo, dpt.dptLib
    HAVING SUM(emp.empSal) >= (SELECT MAX(SUM(e.empSal))
                                FROM employe e
                                    JOIN departement d
                                        ON e.empDpt = d.dptNo
                                    GROUP BY d.dptNo);
```

18. Donner la liste des employés qui ne sont pas des managers:

```sql
SELECT emp.empNo, emp.empNom AS not_manager
 FROM employe emp
    JOIN departement dpt ON emp.empDpt = dpt.dptNo
    WHERE emp.empNo = dpt.dptMgr;
```

19. Donner la liste des employés gagnant plus de la moyenne des salaires de leur département:

```sql
SELECT emp.empNo, emp.empNom, emp.empSal
 FROM employe emp
    JOIN departement dpt ON emp.empDpt = dpt.dptNo
    WHERE emp.empSal > (SELECT AVG(e.empSal)
                         FROM employe e
                            JOIN departement d
                                ON e.empDpt = d.dptNo);
```

# Labo 2

`Expliquez la sémantique de chacune des requêtes suivantes:`

```sql
-- 1
SELECT COUNT(*) - COUNT(dptAdm)
 FROM departement
```

> Affiche le nombre de départements qui ne sont **pas** administrés

```sql
-- 2
SELECT emp.empNo, emp.empNom
 FROM employe emp
   JOIN departement dpt ON dpt.dptNo = emp.empDpt
   JOIN employe m ON dpt.dptMgr = m.empNo
 WHERE UPPER(m.empNom) = 'MAES';
```

> Affiche le numéro et nom des employés qui ont comme manager monsieur 'MAES'

```sql
-- 3
SELECT dpt.dptLib, dpt.dptNo
 FROM departement dpt
   JOIN employe emp ON dpt.dptNo = emp.empDpt
 WHERE sexe = 'F'
 GROUP BY dpt.dptLib, dpt.dptNo
 HAVING COUNT(*) >= ALL(SELECT COUNT(*)
                         FROM employe e
                         WHERE e.empSexe = 'F'
                         GROUP BY e.empDpt);
```

> Affiche le nom et numéro du *(des)* département *(s)* qui comporte *(ent)* le plus d'employées femmes

```sql
-- 4
SELECT DISTINCT emp.empDpt
 FROM employe emp
 WHERE emp.empSal > 110000
 GROUP BY emp.empDpt, emp.empSexe
 HAVING COUNT(*) > 3;
```

> Affiche le numéro des départements dans lesquels il y a plus de trois employés de même sexe qui ont chacun un salaire de plus de 110 000

# Labo 3

## Exercice 1

1. Lister les libellés de département avec en regard, *(s'il existe)*, le libellé du département qui l’administre:

```sql
SELECT dpt.dptLib AS administrés, administrant.dptLib AS administrants
 FROM departement dpt
   LEFT OUTER JOIN departement administrant
      ON dpt.dptAdm = administrant.dptNo
   ORDER BY dpt.dptLib; -- On trie alphabétiquement par administrés
```

2. Obtenir la liste des départements qui n’en administrent pas d’autres:

```sql
SELECT dpt.dptNo, dpt.dptLib
 FROM departement dpt
   LEFT OUTER JOIN departement administrant
      ON administrant.dptNo = dpt.dptAdm
   WHERE dpt.dptAdm IS NOT NULL;
```

3.  Donner par département le libellé de département et le nombre de départements dirigés:

> N’oubliez pas les départements qui n’en dirigent aucun autre

```sql
SELECT dpt.dptLib, COUNT(managed.dptNo) AS nombre_administrés
 FROM departement dpt
   RIGHT OUTER JOIN departement managed ON dpt.dptNo = managed.dptAdm
 GROUP BY dpt.dptLib;
```

4. Donner la liste des managers avec le nombre de personnes qu’ils dirigent:

> N’oubliez pas les éventuels managers qui ne dirigent que des départements sans employés

```sql
SELECT man.empNom AS manager, COUNT(emp.empNo) AS nombre_employés_dirigés
 FROM employe man
    JOIN departement dpt ON dpt.dptMgr = man.empNo
    LEFT OUTER JOIN employe emp ON emp.empDpt = dpt.dptNo
    GROUP BY man.empNom;
```

## Exercice 2

```sql
-- 1
SELECT emp.empNo, emp.empNom
 FROM employe emp
   LEFT JOIN departement dpt ON dpt.dptMgr = emp.empNo
 WHERE dpt.dptMgr IS NULL;
```

> Affiche le numéro et le nom de tous les employés qui ne sont **pas** manager

```sql
-- 2
SELECT dpt.dptNo, dpt.dptLib, COUNT(emp.empNo)
 FROM departement dpt
   LEFT JOIN employe emp ON dpt.dptNo = emp.empDpt
 GROUP BY dpt.dptNo, dpt.dptLib;
```

> Affiche les départements et leur nombre d'employés, même s'ils n'en ont aucun

## Exercice 3

1. Obtenir la liste des noms de managers dirigeant un
département autre que celui auquel ils sont affectés:

```sql
SELECT man.empNom, man.empDpt, dpt.dptMgr
 FROM employe man
    JOIN departement dpt ON dpt.dptMgr = man.empNo
    WHERE man.empDpt != dpt.dptNo
    GROUP BY man.empNom, man.empDpt, dpt.dptMgr;
```

2. Obtenir le salaire moyen d'un manager:

```sql
SELECT AVG(man.empSal)
 FROM employe man
    JOIN departement dpt ON dpt.dptMgr = man.empNo;
```

3. Obtenir le salaire moyen d’un employé qui n’est pas un manager:

```sql
SELECT AVG(emp.empSal)
 FROM gcuv.employe emp
    LEFT JOIN gcuv.departement dpt ON emp.empNo = dpt.dptMgr
    WHERE dptMgr IS NULL;
```

4. Obtenir la liste des employés dirigeant plus d’un département:

```sql
SELECT emp.empNom
 FROM employe emp
    JOIN departement dpt ON emp.empNo = dpt.dptMgr
    GROUP BY emp.empNom
    HAVING COUNT(*) > 1;
```

5. Lister les managers [n°, nom] qui n’appartiennent pas à un département qu’ils dirigent:

```sql
SELECT man.empNo, man.empNom
 FROM employe man
    JOIN departement dpt ON man.empDpt = dpt.dptNo
    WHERE man.empNo != dpt.dptMgr;
```

6.  Obtenir par département le nombre de femmes:

> N’oubliez pas les départements avec 0 femmes

```sql
SELECT dpt.dptLib, COUNT(*) AS nombre_de_femmes
 FROM gcuv.departement dpt
    LEFT JOIN gcuv.employe emp ON emp.empDpt = dpt.dptNo
    WHERE emp.empSexe = 'F'
    GROUP BY dpt.dptLib;
```

# Labo 4

<img src="./sources/labo4_tables.png" width="550" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">Labo 4: Tables</p>

1. Nom et prénom des anciennes:

```sql
SELECT a.nom, a.prenom
 FROM ancien a
   WHERE sexe == 2;
```

2. Nombre d'anciens résidant à Uccle:

```sql
SELECT COUNT(*)
 FROM ancien a
   WHERE a.adLoc = 'Uccle'
   OR a.adCp IN (SELECT cp
                FROM LocaliteBelge
                  WHERE localite = 'Uccle');
```

3. Nombre d'anciens par sexe:

```sql
SELECT a.sexe, COUNT(*)
 FROM ancien a
   GROUP BY a.sexe;
```

4. Nom et prénom des anciens résidant en région flamande:

```sql
SELECT a.nom, a.prenom
 FROM ancien a
   WHERE a.adLoc IN (SELECT localite
                      FROM localitebelge
                        JOIN regionbelge ON region = id
                        WHERE id = 2);
```

5. Nom et prénom des anciens résidant hors de l’UE, ordonnés sur le nom de l’ancien:

```sql
-- Coming soon...
```