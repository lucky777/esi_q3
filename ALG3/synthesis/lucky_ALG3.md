<img src="./img/lucky777.png" width="77" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">lucky777</p>

# ALG3

- [Linked List](#linked-list)
- [Stack](#stack)
- [Queue](#queue)
- [Map](#map)
  * [Roy Warshall](#roy-warshall)
- [Recursivity](#recursivity)
- [Tree](#tree)
  * [Tree browsing](#tree-browsing)
    + [deep](#deep)
    + [wide](#wide)
    + [Contagion](#contagion)
- [Backtracking](#backtracking)

---

# Linked List

```cpp
class ListElement<T> {
private:
    T value
    ListElement<T> next
public:
    //constructors
    ListElement<T>(T val, ListElement<T elt);
    ListElement<T>(T val); //next to nothing
    //getters
    T get_value();
    ListElement<T> get_next(); //NULL if no next
    //setters
    void set_value(T val);
    void set_next(ListElement<T> elt);
}
```

<div style="page-break-after: always;"></div>

```cpp
#include "ListElement.h"

class LinkedList<T> {
private:
    ListElement<T> first; //first element of the list
public:
    //constructors
    LinkedList<T>(); //creates an empty list
    //getter
    ListElement<T> get_first();
    //setters
    void set_first(ListElement<T> l);
    //methods
    bool is_empty();
    void insert_head(T val); //inserts at first position
    void delete_head(); //deletes the element at first position
    void insert_after(ListElement<T> elt, T value);
    void delete_after(ListElement<T> elt);
}
```

# Stack

```cpp
class Stack<T> {
private:
    List<T> data;
public:
    void push(T elt); //adds elt at the end of the stack
    T pop(); //removes the last element of the stack
    T top(); //returns the value of the last element
    bool is_empty();
}
```

# Queue

```cpp
class Queue<T> {
private:
    List<T> data;
public:
    void enqueue(T elt); //adds elt at the end of the queue
    T dequeue(); //removes the first element inserted in the queue
    T head(); //returns the value of the first element inserted
    bool is_empty();
}
```

<div style="page-break-after: always;"></div>

# Map

```cpp
class MapElement<K, T> {
private:
    K key;
    T value;
public:
    //setters
    void set_key(K key);
    void set_value(T value);
    //getters
    K get_key();
    T get_value();
}
```

```cpp
class Map<K, T> {
private:
    List<MapElement<K, T>> map;
public:
    //setter
    void set_element(K key, T value); //adds or modify an element
    //getter
    T get_value(K key);
    void delete_element(K key);
    bool contain(K key); //true if an element with this key exists
    unsigned size(); //size of the map
    Liste<K> key_list(); //returns a list with all the keys
}
```

## Roy Warshall

```cpp
for(int i=0; i<foo; i++) {
    for(int j=0; j<foo; j++) {
        for(int k=0; k<foo; k++) {
            tab[j][k] = tab[j][k] || (tab[j][i] && tab[i][k]);
        }
    }
}
```

<div style="page-break-after: always;"></div>

# Recursivity

`n! = n * (n-1)!`

`0! = 1`

```cpp
int factorial(int n) {
    if(n==0) return 1;
    return (n * factorial(n-1));
}

int main() {
    std::cout << factorial(7) << std::endl;
    // 7 * 6 * 5 * 4 * 3 * 2 *  1 * 1
}
```

# Tree

```cpp
class Node<T> {
private:
    T value;
    List<Node<T>> children;
public:
    //constructor
    Node<T>(T value);
    //setters
    void set_value(T value);
    void set_child(int index, Node<T> child);
    //getters
    T get_value();
    Node<T> get_child(int index);
    //methods
    void add_child(Node<T> new_child);
    void delete_child(int index);
}
```

<div style="page-break-after: always;"></div>

```cpp
#include "Node.h"

class Tree<T> {
private:
    Node<T> root;
public:
    Tree<T>();
    Node<T> get_root();
    void set_root(Node<T>);
}
```

## Tree browsing

### deep

```cpp
foo deep(Tree t) {
    return deep_rec(Node t.get_root(), foo...);
}

foo deep_rec(Node n, Foo foo...) {
    if(n.get_nb_children() == 0) return foo; //leaf
    //pre
    for(Node child : n.get_children()) {
        foo = deep_rec(child, foo...);
        //in
    }
    //post
    return foo;
}
```

### wide

```cpp
foo wide(Tree t) {
    Queue<Node> queue;
    queue.enqueue(t.get_root());
    while(!queue.is_empty()) {
        Node tmp = queue.dequeue();
        //treatments
        for(Node child : tmp.get_children()) {
            queue.enqueue(child);
        }
    }
    return foo;
}
```

<div style="page-break-after: always;"></div>

### Contagion

```cpp
foo contagion(Node start) {
    Map<Node, bool> state; //false = "waiting"     true = "treated"
    state[start] = false;
    Queue<Node> queue;
    queue.enqueue(start);
    while(!queue.is_empty()) {
        Node tmp = queue.enqueue();
        //treating node...
        state[tmp] = true;
        for(Node neigh : tmp.get_neigh()) {
            if(!state[neigh]) {
                queue.enqueue(neigh);
            }
        }
    }

}
```

# Backtracking

```cpp
int facade() {
    //initialistation
    if(bt()) {
        return foo;
    } else {
        return NULL;
    }
}

bool bt() {
    //parametres
    for(Foo foo : foo) {
        //met le premier truc foo
        if(isOk(foo)) {
            if(finished())
                return true;
            return bt();
        }
        //on revient en arrière et on teste le foo suivant
    }
    return false;
}
```