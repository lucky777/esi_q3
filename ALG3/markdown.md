<img src="./sources/lucky777.png" width="77" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">lucky777</p>

# Title

```cpp
tab[] facade(tab[]) {
    if(backTracking(tab[], 0)) {
        return tab[];
    }
    return NULL;
}

bool bt(tab[], int index) {
    tab color=['R', 'G', 'B'];
    for(char c : color) {
        tab[index] = c;
        if(isOk(tab[], index)) {
            if(index == tab.size()-1 || bt(tab[], index+1)) return true;
            index--;
        }
    }
    return false;
}

bool isOk(tab[], index) {
    for(int i=index/2; i>0; i--) {
        if(tab[index] == tab[index - i] && tab[index] == tab[index - 2*i]) return false;
    }
    return true;
}
```



[R, B, V, G, R, B, G, B, R]