package be.he2b.esi.alg3.solutions.graphes;

import be.he2b.esi.alg3.File;
import be.he2b.esi.alg3.FileList;

public class Ex17Composantesconnexes {

    static int nbComposantesConnexes(boolean[][] adj) {
        boolean[] visités = new boolean[adj.length];
        int nbComposantesConnexes = 0;
        int sommet;
        while ((sommet = trouverUnSommetNonVisité(visités)) >= 0) {
            nbComposantesConnexes++;
            contagion(adj, sommet, visités);
        }
        return nbComposantesConnexes;
    }

    /**
     * Déterminer l'indice d'un sommet non-visité On retourne -1 si aucun
     */
    static int trouverUnSommetNonVisité(boolean[] visités) {
        for (int pos = 0; pos < visités.length; pos++) {
            if (!visités[pos]) {
                return pos;
            }
        }
        return -1;
    }

    /**
     * Visiter le sommet /depart/ et sa composante connexe et mettre à jour le
     * tableau /visités/.
     *
     * @param adj la matrice d'adjaence supposée symétrique
     */
    static void contagion(boolean[][] adj, Integer depart, boolean[] visités) {
        File<Integer> file = new FileList<>();
        file.enfiler(depart);
        visités[depart] = true;
        while (!file.estVide()) {
            Integer sommet = file.défiler();
            // je traite le sommet... rien à faire !
            for (int autreSommet = 0; autreSommet < adj.length; autreSommet++) {
                if (!visités[autreSommet] && adj[sommet][autreSommet]) {
                    file.enfiler(autreSommet);
                    visités[autreSommet] = true;
                }
            }

        }
    }

    public static void main(String[] args) {
        int nbComposantesConnexes = nbComposantesConnexes(new boolean[][]{
            {false, true, false},
            {true, false, false},
            {false, false, false}
        });
        System.out.println(nbComposantesConnexes);
    }

}
