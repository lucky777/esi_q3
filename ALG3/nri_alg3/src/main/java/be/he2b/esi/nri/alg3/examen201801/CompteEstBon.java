/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.nri.alg3.examen201801;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nri
 */
public class CompteEstBon {

    public static void main(String[] args) {
        List<Integer> entiers = List.of(1,2,3,4,5);
        compteEstBon(entiers, 10);
    }
/**
 * 
 * @param nombres
 * @param total 
 */
    static void compteEstBon(List<Integer> nombres, int total) {
        int sommeCourante = 0;
        int utilisés = 0;
        ArrayList<Integer> listeCourante = new ArrayList<>();
        compteEstBonBt(nombres, utilisés, total, sommeCourante, listeCourante);
    }

    private static void compteEstBonBt(List<Integer> nombres, int utilisés, int total, int sommeCourante, ArrayList<Integer> listeCourante) {
        for (int i = utilisés; i < nombres.size(); i++) {
            int nbCourant = nombres.get(i);
            if (sommeCourante + nbCourant <= total) {
                listeCourante.add(nbCourant);
                if (sommeCourante + nbCourant == total) {
                    afficherListe(listeCourante, total);
                } else {
                    compteEstBonBt(nombres, i + 1, total, sommeCourante + nbCourant, listeCourante);
                }
                listeCourante.remove(listeCourante.size() - 1);
            }

        }
    }

    private static void afficherListe(ArrayList<Integer> listeCourante, int total) {
        if (!listeCourante.isEmpty()) {
            System.out.print(total + " = " + listeCourante.get(0));
        }
        for (int i = 1; i < listeCourante.size(); i++) {
            System.out.print(" + " + listeCourante.get(i));
        }
        System.out.println();
    }
}
