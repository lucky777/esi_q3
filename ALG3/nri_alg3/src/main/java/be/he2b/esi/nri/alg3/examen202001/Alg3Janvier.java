package be.he2b.esi.nri.alg3.examen202001;

import be.he2b.esi.alg3.ElementListe;
import be.he2b.esi.alg3.ListeChainee;
import java.util.Collection;
import java.util.List;

public class Alg3Janvier {

    public static void main(String[] args) {
        System.out.println("Question 1 : voir syllabus");
        System.out.println();
        
        System.out.println("Question 2 : ");
        Alternances.afficherAlternances(new int[]{1, 5, 9}, new int[]{7, 8, 11});
        System.out.println();

        System.out.println("Question 3a :");
        ListeChainee<String> l = makeLC(List.of("abc", "abc", "xyz", "def", "abc", "def"));
        ListeChainee reponse = Structures.extrait("abc", l);
        System.out.println("reponse = " + reponse);
        System.out.println("l = " + l);
        System.out.println();

        System.out.println("Question 3b :");
        ListeChainee<Integer> l1 = makeLC(List.of(3, 6, 6, 8, 12, 15));
        ListeChainee<Integer> l2 = makeLC(List.of(1, 4, 12, 42, 56));
        Structures.fusionInverseLC(l1, l2);
        System.out.println("l1 = " + l1);
        System.out.println("l2 = " + l2);
    }

    
    /**
     * helper function to create a ListeChainée from an arbitrary Collection.
     * @param <T>
     * @param coll
     * @return 
     */
    static <T> ListeChainee<T> makeLC(Collection<T> coll) {
        ListeChainee<T> lc = new ListeChainee<>();
        ElementListe<T> last = null;
        for (T elt : coll) {
            if (last == null) {
                lc.insérerTête(elt);
                last = lc.getPremier();
            } else {
                lc.insérerAprès(last, elt);
                last = last.getSuivant();
            }
        }
        return lc;
    }

}
