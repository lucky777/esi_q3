/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 * @param <T> le type des éléments de la pile
 */
public class FileList<T> implements File<T> {

    private final ListeBD<T> lc;

    public FileList() {
        lc = new ListeBD<>();
    }

    @Override
    public void enfiler(T val) {
        lc.insérerFin(val);
    }

    @Override
    public T défiler() {
        T val = this.tête();
        lc.supprimerTête();
        return val;
    }

    @Override
    public T tête() {
        if (this.estVide()) {
            throw new NullPointerException("La file est vide"); // todo: vérifier la bonne exception
        }
        return lc.getPremier().getValeur();
    }

    @Override
    public boolean estVide() {
        return lc.estVide();
    }

}
