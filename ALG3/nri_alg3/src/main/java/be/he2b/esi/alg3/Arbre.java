package be.he2b.esi.alg3;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 * @param <T> Le type de la valeur dans les noeuds.
 */
public class Arbre<T> {

    Noeud<T> racine;

    public Arbre() {
    }

    public Noeud<T> getRacine() {
        return racine;
    }

    public void setRacine(Noeud<T> racine) {
        this.racine = racine;
    }

}
