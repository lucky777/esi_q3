package be.he2b.esi.alg3.solutions.graphes;

import java.util.Arrays;

/**
 *
 * @author nri
 */
public class Graphes {

    /**
     *
     *
     * @param src array to clone
     * @return a new clone of the provided array
     */
    public static boolean[][] cloneArray(boolean[][] src) {
        int length = src.length;
        boolean[][] target = new boolean[length][src[0].length];
        for (int i = 0; i < length; i++) {
            System.arraycopy(src[i], 0, target[i], 0, src[i].length);
        }
        return target;
    }

    /**
     *
     *
     * @param m Accessibility matrix to show
     */
    public static void printAccessibilityMatrix(boolean[][] m) {
        System.out.println(Arrays.deepToString(m).replace("], ", "]\n ").replace("true", "T").replace("false", " "));
    }

}
