/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Résout le problème des huit Reines via un algo de backtracking.
 *
 * @author nri
 */
public class Ex01_HuitReines {

    private static void testerHuitReines() {
        //afficherSolution(reines(8));
        for (int[] sol : reinesAll(8)) {
            reinesAfficherSolution(sol);
        }
    }

    /**
     * Trouver une solution au problème des n reines.
     *
     * Une solution est donnée comme un tableau d'entiers: - la position est le
     * numéro de colonne, - la valeur est le numéro de ligne où la reine est
     * placée.
     *
     * @param n nombre de reines
     * @return la solution trouvée (null si aucune)
     */
    public static int[] reines(int n) {
        int[] reines = new int[n];
        List<int[]> sols = new ArrayList<>();
        reinesBacktracking(reines, 0, sols, false);
        reines = sols.isEmpty() ? null : sols.get(0);
        return reines;
    }

    /**
     * Trouver toutes les solutions au problème des n reines
     *
     * @param n nombre de reines
     * @return les solutions
     */
    public static List<int[]> reinesAll(int n) {
        List<int[]> sols = new ArrayList<>();
        reinesBacktracking(new int[n], 0, sols, true);
        return sols;
    }

    /**
     * Méthode récursive réalisant le backtracking pour les "n Reines".
     *
     * @param reines tableau de n entrées représentant un début de solution
     * @param col nombre de colonnes déjà remplies
     * @param sols liste des solutions complètes déjà construites
     * @param toutesLesSolutions indique si on veut toutes les solutions ou une
     * seule
     */
    private static void reinesBacktracking(int[] reines, int col, List<int[]> sols, boolean toutesLesSolutions) {
        boolean stopLooking = false;
        int lg = 0;
        while (!stopLooking && lg < reines.length) {
            reines[col] = lg;
            if (reinesEstAcceptable(reines, col)) {
                if (col == reines.length - 1) {
                    if (!toutesLesSolutions) {
                        stopLooking = true;
                    }
                    sols.add(Arrays.copyOf(reines, reines.length));
                } else {
                    reinesBacktracking(reines, col + 1, sols, toutesLesSolutions);
                }
            }
            lg++;
        }
    }

    /**
     * Vérifier si les positions des reines sont compatibles On suppose que les
     * reines jusque col-1 sont compatibles, on vérifie juste la reine de la
     * colonne donnée en paramètre.
     *
     * @param reines
     * @param col
     * @return
     */
    private static boolean reinesEstAcceptable(int[] reines, int col) {
        int autreCol = 0;
        while (autreCol < col
                && // same line ?
                reines[col] != reines[autreCol]
                && // same diagonal ?
                (col - autreCol) != Math.abs(reines[col] - reines[autreCol])) {
            autreCol++;
        }
        return !(autreCol < col);
    }

    public static void reinesAfficherSolution(int[] sol) {
        int n = sol.length;
        System.out.println(Arrays.toString(sol));

//        |---|
//        | | |
//        |---|
//        | | |
//        |---|
        // headers
        reinesAfficherLigne(n, '_', '_');
        // content
        for (int lg = 0; lg < n; lg++) {
            System.out.print("|");
            for (int col = 0; col < n; col++) {
                boolean montrerReine = sol[col] == n - 1 - lg;
                String contenuCase = montrerReine ? "o" : " ";
                System.out.print(contenuCase + "|");
            }
            System.out.println();
        }
        reinesAfficherLigne(n, '¯', '¯');
    }

    private static void reinesAfficherLigne(int n, char delim, char filling) {
        System.out.print(delim);
        for (int i = 0; i < 2 * n - 1; i++) {
            System.out.print(filling);
        }
        System.out.println(delim);
    }
}
