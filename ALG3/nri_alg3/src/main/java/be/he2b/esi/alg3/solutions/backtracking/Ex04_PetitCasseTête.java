/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Proposition de résolution du Petit Casse-Tête Sans garantie.
 *
 * @author nri
 */
public class Ex04_PetitCasseTête {

    final static boolean[][] GRAPHE
            = {
                {false, true, true, true, true, false, false, false},
                {true, false, false, true, true, true, false, false},
                {true, false, false, true, false, false, true, false},
                {true, true, true, false, true, false, true, true},
                {true, true, false, true, false, true, true, true},
                {false, true, false, false, true, false, false, true},
                {false, false, true, true, true, false, false, true},
                {false, false, false, true, true, true, true, false}
            };
    final static int SOLSIZE = GRAPHE.length;
    final static int[] VALEURS = {1, 2, 3, 4, 5, 6, 7, 8};

    public static void main(String[] args) {
        afficherSolutionsCasseTête();
    }

    private static void afficherCasseTête(int[] s) {
        // la méthode ne respecte pas GRAPHE
        final String NEWL = "\n";
        final String EMPT = " ";
        final String MTSP = " ";
        final String SEPT = "|";
        System.out.println("" // preserve alignment in source code after ALT+SHIFT+F
                + MTSP + EMPT + SEPT + s[0] + SEPT + s[1] + SEPT + EMPT + MTSP + NEWL
                + SEPT + s[2] + SEPT + s[3] + SEPT + s[4] + SEPT + s[5] + SEPT + NEWL
                + MTSP + EMPT + SEPT + s[6] + SEPT + s[7] + SEPT + EMPT + MTSP + NEWL
        );
    }

    private static void afficherSolutionsCasseTête() {
        List<int[]> solutions = new ArrayList<>();
        int[] result = new int[SOLSIZE];
        backtrack(result, 0, solutions);
        for (int[] solution : solutions) {
            afficherCasseTête(solution);
        }
    }

    private static void backtrack(int[] result, int sommet, List<int[]> solutions) {
        for (int val : VALEURS) {
            result[sommet] = val;
            if (estAcceptable(result, sommet)) {
                if (sommet == result.length - 1) {
                    solutions.add(Arrays.copyOf(result, result.length));
                } else {
                    backtrack(result, sommet + 1, solutions);
                }
            }
        }
    }

    private static boolean estAcceptable(int[] result, int curSommet) {
        boolean ok = true;
        for (int sommet = 0; ok && sommet < curSommet; sommet++) {
            ok = !voisin(sommet, curSommet) || pasConsecutif(result[sommet], result[curSommet]);
        }
        return ok && valUniques(result, curSommet);
    }

    private static boolean voisin(int sommet, int curSommet) {
        return GRAPHE[sommet][curSommet];
    }

    private static boolean pasConsecutif(int val1, int val2) {
        return Math.abs(val1 - val2) != 1;
    }

    private static boolean valUniques(int[] result, int curSommet) {
        boolean ok = true;
        for (int sommet = 0; ok && sommet < curSommet; sommet++) {
            ok = result[curSommet] != result[sommet];
        }

        return ok;
    }

}
