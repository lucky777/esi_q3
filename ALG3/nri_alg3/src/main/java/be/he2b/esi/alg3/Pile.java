/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 * @param <T> type des éléments de la pile
 */
public interface Pile<T> {

    public void empiler(T val);

    public T dépiler();

    public T sommet();

    public boolean estVide();

}
