/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.recursivite;

import java.util.Arrays;

/**
 *
 * @author nri
 */
public class Recursivite {

    static int nbExecutions = 0;
    static Integer[] fibonacciMemoire = new Integer[50];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        testerFibo();
//        testerTableauSym();
        testerDivisionEntiere();
    }

    private static void testerFibo() {
        System.out.println(" i : fibo (nb d'execution)");
        for (int i = 0; i < 15; i++) {
            nbExecutions = 0;
            System.out.printf("%2d : %4d (%d)\n", i, fibonacci(i), nbExecutions);
        }
        System.out.println(Arrays.toString(fibonacciMemoire));
    }

    public static int fibonacci(int n) {
        nbExecutions++;
//        return n < 2 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
        if (fibonacciMemoire[n] != null) {
            return fibonacciMemoire[n];
        } else if (n < 2) {
            return 1;
        } else {
            int val = fibonacci(n - 1) + fibonacci(n - 2);
            fibonacciMemoire[n] = val;
            return val;
        }
    }

    public static boolean tabSymRec(Object[] tab, int début, int fin) {
        System.out.printf("test %d et %d : %s et %s\n", début, fin, tab[début], tab[fin]);
        return (début >= fin
                || (tab[début].equals(tab[fin]) && tabSymRec(tab, début + 1, fin - 1)));
    }

    private static void testerTableauSym() {
        Object[] arr = {5, 1, 8, 4, 1, 5};
        System.out.println(tabSymRec(arr, 0, arr.length - 1));
    }

    private static void testerDivisionEntiere() {
        int dividende = 275,
                diviseur = 13,
                resultat = divisionEntiereRecursif(dividende, diviseur);
        System.out.printf("division de %d par %d : %d\n", dividende, diviseur, resultat);
    }

    private static int divisionEntiereRecursif(int dividende, int diviseur) {
        return dividende < diviseur
                ? 0
                : 1 + divisionEntiereRecursif(dividende - diviseur, diviseur);
    }

    private static int factorielle(int i) {
        return (i > 0) ? i * factorielle(i - 1) : 1;
    }

    private static void hanoïRec(int n, char dép, char arr, char inter) {
        if (n > 0) {
            hanoïRec(n - 1, dép, inter, arr);
            System.out.println("Déplacer le plateau " + n + " de " + dép + " vers " + arr);
            hanoïRec(n - 1, inter, arr, dép);
        }
    }
}
