/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 * @param <T> le type des éléments de la pile
 */
public class PileListe<T> implements Pile<T> {

    private ListeChainee<T> lc;
    
    public PileListe() {
        lc = new ListeChainee<>();
    }

    @Override
    public void empiler(T val) {
        lc.insérerTête(val);
    }

    @Override
    public T dépiler() {
        T val = this.sommet();
        lc.supprimerTête();
        return val;
    }

    @Override
    public T sommet() {
        if (this.estVide()) {
            throw new NullPointerException("La pile est vide"); // todo: vérifier la bonne exception
        }
        return lc.getPremier().getValeur();
    }

    @Override
    public boolean estVide() {
        return lc.estVide();
    }

}
