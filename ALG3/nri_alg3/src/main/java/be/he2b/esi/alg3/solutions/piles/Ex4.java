/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.piles;

import be.he2b.esi.alg3.Pile;
import be.he2b.esi.alg3.PileListe;
import java.util.regex.Pattern;

/**
 *
 * @author nri
 */
public class Ex4 {

    public static void main(String[] args) {
//        String[] tab = {"122", "3", "*"};
        String[] tab = {"4", "3", "3", "*", "+", "2", "+"};
        System.out.println(RPN(tab));
    }

    // from https://stackoverflow.com/a/16078719 -- alternative to Guava or Apache Commons
    private static final Pattern DOUBLE_PATTERN = Pattern.compile(
            "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
            + "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
            + "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
            + "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");

    public static boolean isFloat(String s) {
        return DOUBLE_PATTERN.matcher(s).matches();
    }

    static Double RPN(String[] tab) {
        Pile<Double> pile = new PileListe<>();
        for (String item : tab) {
            if (isFloat(item)) {
                pile.empiler(Double.parseDouble(item));
            } else {
                double nb1 = 0;
                double nb2 = 0;
                try {
                    nb2 = pile.dépiler();
                    nb1 = pile.dépiler();
                } catch (Exception e) {
                    throw new AssertionError("Tableau mal formé: trop d'opérateurs ?");
                }
                double res = switch (item.charAt(0)) {
                    case '*' ->
                        nb1 * nb2;
                    case '/' ->
                        nb1 / nb2;
                    case '-' ->
                        nb1 - nb2;
                    case '+' ->
                        nb1 + nb2;
                    default ->
                        throw new AssertionError("Tableau mal formé: opérateur inconnu: " + item);
                };
                pile.empiler(res);
            }
        }
        final Double resultat = pile.dépiler();
        if (!pile.estVide()) {
            throw new AssertionError("Tableau  mal formé: manque d'opérateurs ?");
        }
        return resultat;
    }
}
