package be.he2b.esi.nri.alg3.examen202001;

import be.he2b.esi.alg3.ElementListe;
import be.he2b.esi.alg3.ListeChainee;

/**
 *
 * @author nri
 */
public class Structures {

    public static ListeChainee extrait(String valeur, ListeChainee<String> l) {
        ListeChainee<String> result = new ListeChainee<>();
        ElementListe<String> courant = l.getPremier();
        while (courant != null && courant.getSuivant() != null) {
            if (valeur.equals(courant.getSuivant().getValeur())) {
                l.supprimerAprès(courant);
                result.insérerTête(valeur);
            } else {
                courant = courant.getSuivant();
            }
        }
        if (l.getPremier() != null && valeur.equals(l.getPremier().getValeur())) {
            l.supprimerTête();
            result.insérerTête(valeur);
        }
        return result;
    }

    public static void fusionInverseLC(ListeChainee<Integer> l1, ListeChainee<Integer> l2) {
        ElementListe<Integer> el1 = l1.getPremier();
        ElementListe<Integer> el2 = l2.getPremier();
        ElementListe<Integer> result = null;
        while (el1 != null && el2 != null) {
            ElementListe<Integer> current;
            if (el1.getValeur() < el2.getValeur()) {
                current = el1;
                el1 = el1.getSuivant();
            } else {
                current = el2;
                el2 = el2.getSuivant();
            }
            current.setSuivant(result);
            result = current;
        }
        // now at most one of el1 or el2 is non-null.
        if (el1 == null) {
            el1 = el2;
        }
        // Move each element to the front of /result/.
        while (el1 != null) {
            ElementListe<Integer> current = el1;
            el1 = el1.getSuivant();
            current.setSuivant(result);
            result = current;
        }
        l1.setPremier(result);
        l2.setPremier(null);
    }

}
