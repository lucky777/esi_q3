package be.he2b.esi.alg3.solutions.backtracking;

/**
 *
 * @author nri
 */
public class Ex05_Fresque {

    public static void main(String[] args) {
        int n;
        try {
            n = Integer.parseInt(args[0]);
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
            n = 10;
        }
        fresque(n);
    }

    public static void fresque(int n) {
        Couleur[] tab = new Couleur[n];
        if (bt(tab, 0)) {
            afficherFresque(tab);
        } else {
            System.out.println("Aucune fresque n'a pu être construite");
        }
    }

    private static void afficherFresque(Couleur[] tab) {
        for (Couleur couleur : tab) {
            String ch = switch (couleur) {
                case BLANC ->
                    "□";
                case GRIS ->
                    "▦";
                case NOIR ->
                    "■";
                default ->
                    throw new AssertionError();
            };
            System.out.print(ch);
        }
        System.out.println();
    }

    private static boolean bt(Couleur[] fresque, int nbCases) {
        for (Couleur couleur : Couleur.values()) {
            fresque[nbCases] = couleur;
            if (estAcceptable(fresque, nbCases)) {
                if (nbCases == fresque.length - 1 || bt(fresque, nbCases + 1)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean estAcceptable(Couleur[] fresque, int pos) {
        for (int d = 1; 2 * d <= pos; d++) {
            if (fresque[pos] == fresque[pos - d]
                    && fresque[pos - d] == fresque[pos - 2 * d]) {
                return false;
            }
        }
        return true;
    }
}

enum Couleur {
    NOIR, GRIS, BLANC;
}
