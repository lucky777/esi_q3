/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.arbres;

import be.he2b.esi.alg3.Arbre;
import be.he2b.esi.alg3.File;
import be.he2b.esi.alg3.FileList;
import be.he2b.esi.alg3.Noeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouveauNoeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouvelArbre;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class Exercice02 {

    public static void main(String[] args) {
        Arbre<Integer> a
                = nouvelArbre(5,
                        nouveauNoeud(4,
                                nouveauNoeud(2),
                                nouveauNoeud(5)),
                        nouveauNoeud(2),
                        nouveauNoeud(10));

        System.out.println(nbFeuilles_largeur(a));
        System.out.println(nbFeuilles_profondeur(a));
    }

    static <T> int nbFeuilles_largeur(Arbre<T> a) {
        File<Noeud<T>> file = new FileList<>();
        if (a.getRacine() != null) {
            file.enfiler(a.getRacine());
        }
        int cpt = 0;
        while (!file.estVide()) {
            Noeud<T> nc = file.défiler();
            if (nc.getNbFils() == 0) {
                cpt++;
            }
            for (int i = 0; i < nc.getNbFils(); i++) {
                file.enfiler(nc.getFils(i));
            }
        }
        return cpt;
    }

    static <T> int nbFeuilles_profondeur(Arbre<T> a) {
        return nbFeuilles_profondeurRec(a.getRacine());
    }

    /**
     * Retourner le nombre de feuilles du sous-arbre partant du noeud donné.
     *
     * @param <T>
     * @param noeud
     * @return
     */
    private static <T> int nbFeuilles_profondeurRec(Noeud<T> noeud) {
        if (noeud == null) {
            return 0;
        }
        if (noeud.getNbFils() == 0) {
            return 1;
        }
        int cpt = 0;
        for (int i = 0; i < noeud.getNbFils(); i++) {
            cpt += nbFeuilles_profondeurRec(noeud.getFils(i));
        }
        return cpt;
    }

}
