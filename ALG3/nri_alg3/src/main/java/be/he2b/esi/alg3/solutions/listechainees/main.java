/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.listechainees;

import be.he2b.esi.alg3.ElementListe;
import be.he2b.esi.alg3.ListeChainee;
import java.util.List;

/**
 *
 * @author nri
 */
public class main {

    public static void main(String[] args) {
        System.out.println("Hello World !");
        ListeChainee<Integer> mylist = ListeChainees.make(List.of(42, 35, 78, 42, 12));
        System.out.println("mylist = " + mylist.toString());
        System.out.println("Taille de la liste (version récursive) : " + ListeChainees.tailleRec(mylist));
        ListeChainee<Integer> l2 = new ListeChainee<>();
        ListeChainee<Integer> l3 = new ListeChainee<>();
        setsil(mylist, l2, l3);
        System.out.println("l2 = " + l2.toString());
        System.out.println("l3 = " + l3.toString());
    }

    /**
     *
     * @param l liste de départ
     * @param l2 liste (a priori vide) qui récupère les éléments d'ordre impair
     * @param l3 liste (a priori vide) qui recupère les éléments d'ordre pair
     */
    public static void setsil(ListeChainee<Integer> l, ListeChainee<Integer> l2, ListeChainee<Integer> l3) {
        ElementListe<Integer> elt, lastElt2, lastElt3;
        elt = l.getPremier();
        lastElt2 = null;
        lastElt3 = null;
        while (elt != null) {
            lastElt2 = insertAfterAndGetNew(l2, lastElt2, elt.getValeur());
            elt = elt.getSuivant();
            if (elt != null) {
                lastElt3 = insertAfterAndGetNew(l3, lastElt3, elt.getValeur());
                elt = elt.getSuivant();
            }
        }
    }

    static ElementListe<Integer> insertAfterAndGetNew(ListeChainee<Integer> l, ElementListe<Integer> elt, Integer val) {
        // insère une valeur après un élément donné dans l, et
        // renvoie l'élément nouvellement créé
        // si l'élément donné est null, on insère en tête
        ElementListe<Integer> nouveau;
        if (elt == null) {
            l.insérerTête(val);
            nouveau = l.getPremier();
        } else {
            l.insérerAprès(elt, val);
            nouveau = elt.getSuivant();
        }
        return nouveau;
    }

}
