/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

import java.util.Arrays;

/**
 * Proposition de résolution du Petit Casse-Tête Sans garantie.
 *
 * @author nri
 */
public class Ex04_PetitCasseTêteSansGraphe {

    public static void main(String[] args) {
        afficherSolutionsCasseTête();
    }

    private static void afficherSolutionsCasseTête() {
        backtrack(new int[3][4], 0, 1);
    }

    private static void afficherCasseTête(int[][] s) {
        System.out.println(Arrays.deepToString(s));
    }

    private static void backtrack(int[][] sol, int lg, int col) {

        for (int val = 1; val < 9; val++) {
            sol[lg][col] = val;

            if (estAcceptable(sol, lg, col)) {
                if (lg == 2 && col == 2) {
                    afficherCasseTête(sol);
                } else {
                    int saveLg = lg, saveCol = col;
                    if (lg == 0 && col == 2) {
                        lg++;
                        col = 0;
                    } else if (lg == 1 && col == 3) {
                        lg++;
                        col = 1;
                    } else {
                        col++;
                    }
                    backtrack(sol, lg, col);
                    lg = saveLg;
                    col = saveCol;
                }
            }
        }
    }

    private static boolean estAcceptable(int[][] sol, int curLg, int curCol) {
        for (int lg = 0; lg <= curLg; lg++) {
            int maxCol = (lg == curLg ? curCol : sol[lg].length);
            for (int col = 0; col < maxCol; col++) {
                if (estDansTableau(lg, col)) {
                    int minimalDifference;
                    if (estVoisin(curLg, curCol, lg, col)) {
                        minimalDifference = 2;
                    } else {
                        minimalDifference = 1;
                    }
                    if (Math.abs(sol[lg][col] - sol[curLg][curCol]) < minimalDifference) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static boolean estVoisin(int curLg, int curCol, int lg, int col) {
        return Math.abs(curLg - lg) <= 1 && Math.abs(curCol - col) <= 1 && (lg != curLg || col != curCol);
    }

    private static boolean estDansTableau(int lg, int col) {
        return !((lg == 0 || lg == 2) && (col == 0 || col == 3));
    }

}
