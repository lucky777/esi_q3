/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.arbres;

import be.he2b.esi.alg3.Arbre;
import be.he2b.esi.alg3.File;
import be.he2b.esi.alg3.FileList;
import be.he2b.esi.alg3.Noeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouveauNoeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouvelArbre;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class Exercice01 {

    public static void main(String[] args) {
        Arbre<Integer> a
                = nouvelArbre(5,
                        nouveauNoeud(4,
                                nouveauNoeud(2),
                                nouveauNoeud(5)),
                        nouveauNoeud(2),
                        nouveauNoeud(10));

        System.out.println(nbNoeuds_largeur(a));
        System.out.println(nbNoeuds_profondeur(a));
    }

    static <T> int nbNoeuds_largeur(Arbre<T> a) {
        File<Noeud<T>> file = new FileList<>();
        if (a.getRacine() != null) {
            file.enfiler(a.getRacine());
        }
        int cpt = 0;
        while (!file.estVide()) {
            Noeud<T> nc = file.défiler();
            cpt++;
            for (int i = 0; i < nc.getNbFils(); i++) {
                file.enfiler(nc.getFils(i));
            }
        }
        return cpt;
    }

    static <T> int nbNoeuds_profondeur(Arbre<T> a) {
        return nbNoeuds_profondeurRec(a.getRacine());
    }

    private static <T> int nbNoeuds_profondeurRec(Noeud<T> noeud) {
        if (noeud == null) {
            return 0;
        }
        int cpt = 1;
        for (int i = 0; i < noeud.getNbFils(); i++) {
            cpt += nbNoeuds_profondeurRec(noeud.getFils(i));
        }
        return cpt;
    }

}
