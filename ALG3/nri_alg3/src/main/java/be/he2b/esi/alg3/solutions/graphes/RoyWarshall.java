/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.graphes;

import java.util.Arrays;

/**
 *
 * @author nri
 */
public class RoyWarshall {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean[][] m = {
            {false, true,  false, true,  false},
            {false, false, true,  false, false},
            {true,  false, false, false, false},
            {false, false, false, false, true},
            {true,  false, false, false, false}
        };
        System.out.println("Situation de départ:");
        Graphes.printAccessibilityMatrix(m);
        RoyWarshall(m, true);

    }


    public static boolean[][] RoyWarshall(boolean[][] adjMatrix, boolean print) {
        boolean[][] accessMatrix = Graphes.cloneArray(adjMatrix);
        int n = adjMatrix.length;
        for (int k = 0; k < n; k++) {
            RoyWarshallUpdateFor(adjMatrix, accessMatrix, k);
            if (print) {
                System.out.printf("Situation à l'étape %d:\n", k + 1); // k+1 car on numérote àpd 0
                Graphes.printAccessibilityMatrix(accessMatrix);
            }
        }
        return accessMatrix;
    }

    /**
     * Etape k de l'algo de RoyWarshall
     *
     * @param adjMatrix Matrice d'adjacence
     * @param accessMatrix Matrice d'accessibilité pour les étapes 0 à k - 1
     * @param k Numéro de l'étape (compté àpd 0)
     */
    public static void RoyWarshallUpdateFor(boolean[][] adjMatrix, boolean[][] accessMatrix, int k) {
        int n = accessMatrix.length;
        for (int i = 0; i < n; i++) {
            // Original:
            // for (int j = 0; j < n; j++) {
            //     accessMatrix[i][j] = accessMatrix[i][j] || (accessMatrix[i][k] && accessMatrix[k][j]);
            // }
            // "Optimized":
            if (accessMatrix[i][k]) {
                for (int j = 0; j < n; j++) {
                    accessMatrix[i][j] = accessMatrix[i][j] || accessMatrix[k][j];
                }
            }

        }
    }

}
