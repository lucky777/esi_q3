/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

import java.util.Objects;

/**
 *
 * @author nri
 * @param <T>
 */
public class ElementListe<T> {
    private T valeur;
    private ElementListe<T> suivant;

    public T getValeur() {
        return valeur;
    }

    public void setValeur(T valeur) {
        this.valeur = valeur;
    }

    public ElementListe<T> getSuivant() {
        return suivant;
    }
    
    public void setSuivant(ElementListe<T> suivant) {
        this.suivant = suivant;
    }

    public ElementListe(T val, ElementListe<T> next) {
        this.valeur = val;
        this.suivant = next;
    }

    @Override
    public String toString() {
        return valeur.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.valeur);
        hash = 37 * hash + Objects.hashCode(this.suivant);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElementListe<?> other = (ElementListe<?>) obj;
        if (!Objects.equals(this.valeur, other.valeur)) {
            return false;
        }
        if (!Objects.equals(this.suivant, other.suivant)) {
            return false;
        }
        return true;
    }


    
}
