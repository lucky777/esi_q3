package be.he2b.esi.alg3.solutions.graphes;

import be.he2b.esi.alg3.File;
import be.he2b.esi.alg3.FileList;
import java.util.List;

public class Ex15Contagion {

    static void contagion(List<Integer>[] graphe, Integer depart, String[] nomNoeuds) {
        boolean[] sommetsVisités = new boolean[graphe.length];
        // faux = prêt, true = traité
        File<Integer> file = new FileList<>();
        file.enfiler(depart);
        sommetsVisités[depart] = true;
        while (!file.estVide()) {
            Integer sommet = file.défiler();
            System.out.println("On traite le sommet: " + nomNoeuds[sommet]);
            List<Integer> voisins = graphe[sommet];
            for (Integer voisin : voisins) {
                if (!sommetsVisités[voisin]) {
                    sommetsVisités[voisin] = true;
                    file.enfiler(voisin);
                }
            }
        }
    }

    public static void main(String[] args) {
        List<Integer>[] graphe
                = new List[]{
                    List.of(5),
                    List.of(0, 2),
                    List.of(),
                    List.of(1, 4),
                    List.of(1),
                    List.of(1, 3)
                };
        contagion(graphe, 5, new String[]{"A", "B", "C", "D", "E", "F"});
    }
}
