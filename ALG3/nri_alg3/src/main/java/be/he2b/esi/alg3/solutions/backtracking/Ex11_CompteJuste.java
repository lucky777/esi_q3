/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author nri
 */
public class Ex11_CompteJuste {

    public static void main(String[] args) {
        afficherCompteJuste(new int[]{10, 20, 50, 50, 5, 20, 2, 10, 10, 2}, 123);
        afficherCompteJuste(new int[]{10, 20, 50, 50, 5, 20, 2, 10, 10, 1, 2}, 123);

    }

    private static void afficherCompteJuste(int[] portemonnaie, int valeurCible) {
        System.out.printf("Pour faire %d avec les pièces %s%n", valeurCible, Arrays.toString(portemonnaie));
        final ArrayList<Integer> solution = new ArrayList<>();
        boolean trouvé = backtracking(portemonnaie, valeurCible, 0, 0, solution);
        if (trouvé) {
            System.out.println("On a utilisé les pièces : " + solution);
        } else {
            System.out.println("On n'y arrive pas!");
        }
    }

    /**
     *
     * @param portemonnaie
     * @param somme
     * @param nbIndicesTentés nombre de pièces/billets déjà tentées (àpd début
     * du portemonnaie)
     * @return
     */
    private static boolean backtracking(int[] portemonnaie, int somme, int nbIndicesTentés, int sommeActuelle, List<Integer> solutionFinale) {
        if (nbIndicesTentés >= portemonnaie.length) {
            return false;
        }
        boolean trouvé = false;
        final int valATenter = portemonnaie[nbIndicesTentés];
        if (sommeActuelle + valATenter == somme) {
            trouvé = true;
        } else if (sommeActuelle + valATenter < somme) {
            trouvé = backtracking(portemonnaie, somme, nbIndicesTentés + 1, sommeActuelle + valATenter, solutionFinale);
        }
        if (trouvé) {
            solutionFinale.add(portemonnaie[nbIndicesTentés]);
        }
        return trouvé || backtracking(portemonnaie, somme, nbIndicesTentés + 1, sommeActuelle, solutionFinale);
    }

}
