package be.he2b.esi.alg3.solutions.graphes;

public class Ex06 {

    public static void main(String[] args) {
        Graphes.printAccessibilityMatrix(ex6(8));
    }

    static boolean[][] ex6(int n) {
        boolean[][] M = new boolean[n][n];
        for (int i = 0; i < n; i++) {
            M[i][(i + 1) % n] = true;
            M[i][(i + 2) % n] = true;
            M[i][(i - 1 + n) % n] = true;
            M[i][(i - 2 + n) % n] = true;
        }
        return M;
    }
}
