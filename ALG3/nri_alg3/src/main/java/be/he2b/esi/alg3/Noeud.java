/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class Noeud<T> {

    T valeur;
    List<Noeud<T>> listeFils;

    public Noeud(T valeur) {
        this.valeur = valeur;
        this.listeFils = new ArrayList<>();
    }

    public T getValeur() {
        return valeur;
    }

    public int getNbFils() {
        return listeFils.size();
    }

    public void setValeur(T valeur) {
        this.valeur = valeur;
    }

    public Noeud<T> getFils(int i) {
        return listeFils.get(i);
    }

    public void setFils(int i, Noeud<T> noeud) {
        listeFils.set(i, noeud);
    }

    public void ajouterFils(Noeud<T> noeud) {
        listeFils.add(noeud);
    }

    public void supprimerFils(int i) {
        listeFils.remove(i);
    }

}
