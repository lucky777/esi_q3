/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

/**
 *
 * @author nri
 */
public class Ex02_NoConsecutiveRepeatedStrings {
    public static void main(String[] args) {
        System.out.println(noConsecutiveRepeatedStrings(100));
    }
    
    static String noConsecutiveRepeatedStrings(int n) {
        return backtracking(new StringBuilder(n), n);
    }

    static String backtracking(StringBuilder motcourant, int goal) {
        final char[] letters = {'a', 'b', 'c'};
        boolean fini = false;
        String result = null;
        int i = 0;
        while (!fini && i < letters.length) {
            motcourant.append(letters[i]);
            if (estAcceptable(motcourant)) {
                if (motcourant.length() == goal) {
                    result = motcourant.toString();
                    fini = true;
                } else {
                    result = backtracking(motcourant, goal);
                    fini = result != null;
                }
            }
            if (!fini) {
                motcourant.deleteCharAt(motcourant.length() - 1);
            }
            i++;
        }
        return result;
    }

    private static boolean estAcceptable(StringBuilder motcourant) {
        int n = motcourant.length();
        int longueur = 1;
        boolean looksOK = true;
        while (looksOK && longueur <= n / 2) {
            CharSequence seq1 = motcourant.subSequence(n - longueur, n);
            CharSequence seq2 = motcourant.subSequence(n - 2 * longueur, n - longueur);
            if (seq1.equals(seq2)) {
                looksOK = false;
            } else {
                longueur++;
            }
        }
        return looksOK;
    }

}
