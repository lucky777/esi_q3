package be.he2b.esi.alg3.solutions.graphes;

import be.he2b.esi.alg3.ListeChainee;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ex08 {

    public static void main(String[] args) {
        boolean[][] m = {
            {false, true, false, true, false},
            {false, false, true, false, false},
            {true, false, false, false, false},
            {false, false, false, false, true},
            {true, false, false, false, false}
        };
        System.out.println(Arrays.toString(adjacenceVersTableauDeListe(m)));
        System.out.println(Arrays.toString(adjacenceVersTableauDeListeChainée(m)));
    }

    static List<Integer>[] adjacenceVersTableauDeListe(boolean[][] M) {
        final int n = M.length;
        List<Integer>[] l = (List<Integer>[]) new List[n];
        for (int i = 0; i < n; i++) {
            l[i] = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                if (M[i][j]) {
                    l[i].add(j);
                }
            }
        }
        return l;
    }

    static ListeChainee<Integer>[] adjacenceVersTableauDeListeChainée(boolean[][] M) {
        final int n = M.length;
        ListeChainee<Integer>[] l = (ListeChainee<Integer>[]) new ListeChainee[n];
        for (int i = 0; i < n; i++) {
            l[i] = new ListeChainee<>();
            for (int j = 0; j < n; j++) {
                if (M[i][j]) {
                    l[i].insérerTête(j);
                }
            }
        }
        return l;
    }
}
