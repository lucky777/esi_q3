package be.he2b.esi.alg3.solutions.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Étant donné une matrice d'entiers, trouve un chemin de poids minimal parmi
 * les entrées de la matrice.
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class PoidsMinimal {

    public static void main(String[] args) {
        Jeu jeu;
        
        jeu = new Jeu(new int[][]{
            {1, 1, 3},
            {3, 2, 6},
            {1, 1, 2}
        }, new Case(1, 1), new Case(2, 2));
        System.out.println(jeu.parcoursMinimum());
        
        jeu = new Jeu(new int[][]{
            {1, 3, 2, 2, 4, 7, 6},
            {8, 9, 4, 8, 3, 3, 3},
            {6, 1, 2, 6, 10, 8, 5},
            {8, 2, 7, 5, 7, 9, 3},
            {5, 2, 1, 2, 6, 5, 2},
            {4, 3, 4, 3, 2, 3, 2},
            {2, 2, 5, 6, 7, 2, 1}
        });
        System.out.println(jeu.parcoursMinimum());
        
    }
}

/**
 * Enum classique comprenant les directions cardinales.
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
enum Direction {
    GAUCHE(0, -1),
    DROITE(0, 1),
    HAUT(-1, 0),
    BAS(1, 0);

    /**
     * Détermine une direction pour se rapprocher du but à partir d'une position
     * de départ donnée.
     *
     * Itérer les appels et se déplacer chaque fois dans la direction donnée
     * permet ultimement d'atteindre le but.
     *
     * @param from départ
     * @param to but
     * @return une direction
     */
    public static Direction direction(Case from, Case to) {
        int dHorizontal = to.colonne - from.colonne;
        if (dHorizontal < 0) {
            return GAUCHE;
        }
        if (dHorizontal > 0) {
            return DROITE;
        }
        int dy = to.ligne - from.ligne;
        if (dy < 0) {
            return HAUT;
        }
        if (dy > 0) {
            return BAS;
        }
        throw new IllegalArgumentException("Les cases sont égales: " + from + " " + to);
    }

    private final int deltaCol;

    /**
     * Get the value of deltaCol
     *
     * @return the value of deltaCol
     */
    public int getDeltaCol() {
        return deltaCol;
    }

    private final int deltaLg;

    /**
     * Get the value of deltaLg
     *
     * @return the value of deltaLg
     */
    public int getDeltaLg() {
        return deltaLg;
    }

    private Direction(int deltaLg, int deltaCol) {
        this.deltaLg = deltaLg;
        this.deltaCol = deltaCol;
    }

}

/**
 * Une position (ligne, colonne)
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
class Case {

    final int ligne;
    final int colonne;

    public Case(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    /**
     * Indique la case dans la direction donnée.
     *
     * @param dir une direction
     * @return une case à la nouvelle position
     */
    public Case next(Direction dir) {
        return new Case(ligne + dir.getDeltaLg(), colonne + dir.getDeltaCol());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.ligne;
        hash = 89 * hash + this.colonne;
        return hash;
    }

    /**
     * Deux cases sont égales si elles ont la même position.
     *
     * @param obj un objet
     * @return vrai si les objets sont deux cases égales
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Case other = (Case) obj;
        if (this.ligne != other.ligne) {
            return false;
        }
        if (this.colonne != other.colonne) {
            return false;
        }
        return true;
    }

    /**
     * Une case s'affiche au format (lg, col)
     *
     * @return une chaîne représentant la case
     */
    @Override
    public String toString() {
        return "(" + ligne + ", " + colonne + ')';
    }

}

/**
 * Cette classe représente le tableau de jeu, avec son point de départ et son
 * point d'arrivée.
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
class Jeu {

    final private int[][] tab;
    final private Case caseDepart;
    final private Case caseArrivée;

    /**
     * Constructeur par défaut
     *
     * Le point de départ est en haut à gauche (0,0) Le point d'arrivée est en
     * bas à droite.
     *
     * @param tab le tableau de jeu
     */
    public Jeu(int[][] tab) {
        this(tab,
                new Case(0, 0),
                new Case(tab.length - 1,
                        tab[tab.length - 1].length - 1));
    }

    /**
     * Construit un jeu avec tous les paramètres spécifiés.
     *
     * @param tab le tableau
     * @param caseDepart la case de départ
     * @param caseArrivée la case d'arrivée
     */
    public Jeu(int[][] tab, Case caseDepart, Case caseArrivée) {
        this.tab = tab;
        this.caseDepart = caseDepart;
        this.caseArrivée = caseArrivée;
    }

    int getValeur(Case c) {
        return tab[c.ligne][c.colonne];
    }

    /**
     * Détermine un parcours (non-optimal) du plateau entre les cases de départ
     * et d'arrivée.
     *
     * @return un parcours
     */
    Solution parcoursTrivial() {
        Solution sol = new Solution(this);
        for (Case courante = caseDepart; !courante.equals(caseArrivée);) {
            Direction dir = Direction.direction(courante, caseArrivée);
            sol.goForward(dir);
            courante = courante.next(dir);
        }
        return sol;
    }

    public Case getCaseArrivée() {
        return caseArrivée;
    }

    public Case getCaseDepart() {
        return caseDepart;
    }

    /**
     * Détermine une case est dans le plateau de jeu
     *
     * @param uneCase une case
     * @return vrai si la case est dans le plateau, faux sinon
     */
    boolean contient(Case uneCase) {
        return uneCase.ligne >= 0
                && uneCase.colonne >= 0
                && uneCase.ligne < this.tab.length
                && uneCase.colonne < this.tab[uneCase.ligne].length;
    }

    @Override
    public String toString() {
        return "Jeu{" + "tab=" + Arrays.deepToString(tab) + '}';
    }

    /**
     * Détermine le chemin de poids minimal
     *
     * @param jeu le plateau de jeu
     * @return le chemin de poids minimal
     */
    public Solution parcoursMinimum() {
        Solution parcoursOptimal = this.parcoursTrivial();
        Solution parcours = new Solution(this);
        parcoursMinBacktracking(parcours, parcoursOptimal);
        return parcoursOptimal;
    }

    private void parcoursMinBacktracking(Solution parcours, Solution parcoursOptimal) {
        for (Direction dir : Direction.values()) {
            if (parcours.estAcceptable(dir, parcoursOptimal)) {
                parcours.goForward(dir);
                if (parcours.estFini()) {
                    parcoursOptimal.copier(parcours);
                } else {
                    parcoursMinBacktracking(parcours, parcoursOptimal);
                }
                parcours.goBack();
            }
        }
    }
}

/**
 * Une solution au jeu est fournie par une liste de cases.
 *
 * La première est au point de départ, la dernière au point d'arrivée.
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
class Solution {

    private final Jeu jeu;
    private final List<Case> cases;
    private int somme;

    public Solution(Jeu jeu) {
        this.cases = new ArrayList<>();
        this.jeu = jeu;
        this.somme = 0;
        this.ajouterCase(jeu.getCaseDepart());
    }

    /**
     * La somme associée à la solution courante.
     *
     * @return
     */
    public int getSomme() {
        return somme;
    }

    /**
     * Ajoute une case à la solution courante.
     *
     * @param uneCase
     */
    private void ajouterCase(Case uneCase) {
        this.cases.add(uneCase);
        this.somme += jeu.getValeur(uneCase);
    }

    /**
     * Retourne une case en arrière.
     */
    public void goBack() {
        final int lastIndex = this.cases.size() - 1;
        Case derniereCase = this.cases.get(lastIndex);
        this.cases.remove(lastIndex);
        this.somme -= jeu.getValeur(derniereCase);
    }

    /**
     * Avance d'une case dans la direction donnée.
     *
     * @param dir la direction
     */
    void goForward(Direction dir) {
        Case next = derniereCase().next(dir);
        if (!jeu.contient(next)) {
            throw new IllegalArgumentException("La case n'est pas dans le jeu: " + next);
        }
        ajouterCase(next);
    }

    /**
     * La dernière case de la solution courante.
     *
     * @return la dernière case
     */
    private Case derniereCase() {
        return this.cases.get(this.cases.size() - 1);
    }

    @Override
    public String toString() {
        return "Solution{" + "cases=" + cases + " ; somme=" + this.somme + "}";
    }

    /**
     * Modifie la solution courante pour être égale à celle donnée
     *
     * @param parcours la solution à copier
     */
    void copier(Solution parcours) {
        this.effacerParcours();
        for (Case aCase : parcours.cases) {
            this.ajouterCase(aCase);
        }
    }

    private void effacerParcours() {
        this.cases.clear();
        this.somme = 0;
    }

    /**
     * Vérifie si la solution est complète.
     *
     * @return vrai si la solution est complète
     */
    boolean estFini() {
        return this.derniereCase().equals(this.jeu.getCaseArrivée());
    }

    /**
     * Vérifie si la case dans la direction donnée est acceptable par rapport à
     * la solution optimale
     *
     * Il faut que la case :
     * <ul>
     * <li> soit dans la plateau</li>
     * <li> ne soit pas dans la solution courante</li>
     * <li> ne fasse pas dépasser la meilleure somme</li>
     * </ul>
     *
     * @param dir
     * @param parcoursOptimal
     * @return
     */
    boolean estAcceptable(Direction dir, Solution parcoursOptimal) {
        Case nouvelleCase = derniereCase().next(dir);
        return jeu.contient(nouvelleCase)
                && !this.contient(nouvelleCase)
                && this.getSomme() + jeu.getValeur(nouvelleCase) < parcoursOptimal.getSomme();
    }

    /**
     * Vérifie si la solution courante contient la case donnée.
     *
     * @param uneCase
     * @return
     */
    private boolean contient(Case uneCase) {
        return cases.contains(uneCase);
    }

    public List<Case> asList() {
        return cases;
    }

}
