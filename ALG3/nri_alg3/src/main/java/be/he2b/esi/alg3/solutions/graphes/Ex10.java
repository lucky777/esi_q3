package be.he2b.esi.alg3.solutions.graphes;

public class Ex10 {

    public static void main(String[] args) {
        boolean[][] mSym = new boolean[][]{
            {false, true, false, false, true, false},
            {true, false, true, false, false, false},
            {false, true, false, true, false, true},
            {false, false, true, false, true, true},
            {true, false, false, true, false, true},
            {false, false, true, true, true, false}
        };

        boolean[][] mTri = new boolean[][]{
            {false, true, false, false, true, false},
            {false, false, true, false, false, false},
            {false, false, false, true, false, true},
            {false, false, false, false, true, true},
            {false, false, false, false, false, true},
            {false, false, false, false, false, false}
        };
        System.out.println(degMaximalVSymetrique(mSym));
        System.out.println(degMaximalVTriangulaireSup(mTri));
    }

    static int degMaximalVSymetrique(boolean[][] M) {
        int degMax = -1;
        int sommetMax = -1;
        for (int sommet = 0; sommet < M.length; sommet++) {
            int degCourant = degréSymétrique(M, sommet);
            if (degCourant > degMax) {
                degMax = degCourant;
                sommetMax = sommet;
            }

        }
        return sommetMax;
    }

    private static int degréSymétrique(boolean[][] M, int sommet) {
        int deg = 0;
        for (int col = 0; col < M.length; col++) {
            if (M[sommet][col]) {
                deg++;
            }
        }
        return deg;
    }

    static int degMaximalVTriangulaireSup(boolean[][] M) {
        int sommetMax = -1;
        int degMax = -1;
        for (int sommet = 0; sommet < M.length; sommet++) {
            int degCourant = degréTriangulaireSup(M, sommet);
            if (degCourant > degMax) {
                degMax = degCourant;
                sommetMax = sommet;
            }
        }
        return sommetMax;
    }

    private static int degréTriangulaireSup(boolean[][] M, int sommet) {
        int deg = 0;
        int lg = 0, col = sommet;
        for (int i = 0; i < M.length; i++) {
            if (M[lg][col]) {
                deg++;
            }
            if (lg < sommet) { // on parcourt la colonne jusqu'à la diagonale puis on achève la ligne
                lg++;
            } else {
                col++;
            }
        }
        return deg;
    }
}
