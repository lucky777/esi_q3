/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 * @param <T> Type des éléments de la pile
 */
public class PileTab<T> implements Pile<T> {

    private final T[] tab;
    private int indSommet;
    private final int tailleMax;

    public PileTab(int taille) {
        tab = (T[])(new Object[taille]);
        tailleMax = taille;
        indSommet = -1;
    }

    @Override
    public void empiler(T val) {
        if (indSommet + 1 < tailleMax) {
            indSommet++;
            tab[indSommet] = val;
        } else {
            throw new ArrayIndexOutOfBoundsException("taille de la pile dépassée");
        }
    }

    @Override
    public T dépiler() {
        T val = sommet();
        indSommet--;
        return val;
    }

    @Override
    public T sommet() {
        if (estVide()) {
            throw new NullPointerException("La pile est vide");
        }
        return tab[indSommet];
    }

    @Override
    public boolean estVide() {
        return indSommet == -1;
    }

}
