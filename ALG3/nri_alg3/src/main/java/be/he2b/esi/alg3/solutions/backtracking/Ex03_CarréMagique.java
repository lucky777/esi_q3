/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author nri
 */
public class Ex03_CarréMagique {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testerCarréMagique();
    }

    private static void testerCarréMagique() {
        for (int[][] sol : carréMagique(3)) {
            carréMagiqueAfficherSol(sol);
            System.out.println();
        }
    }

    private static void carréMagiqueAfficherSol(int[][] carre) {
        //System.out.println(Arrays.deepToString(carre));
        printArray(carre);
    }

    public static void printArray(int[][] m) {
        System.out.println(Arrays.deepToString(m).replace("], ", "]\n "));
    }

    public static boolean carréMagiqueEstAcceptable(int[][] carré, int lg, int col) {
        int n = carré.length;
        int sommeMagique = (n * (n * n + 1)) / 2;
        boolean ok = true;
        if (lg == n - 1) {
            ok = ok && carréMagiqueSommeCol(carré, col) == sommeMagique;
            if (col == 0) {
                ok = ok && carréMagiqueSommeDiagA(carré) == sommeMagique;
            }
            if (col == n - 1) {
                ok = ok && carréMagiqueSommeDiagD(carré) == sommeMagique;
            }
        }
        if (col == n - 1) {
            ok = ok && carréMagiqueSommeLg(carré, lg) == sommeMagique;
        }
        return ok && carréMagiqueValUniques(carré, lg, col);
    }

    public static List<int[][]> carréMagique(int n) {
        List<int[][]> sols = new ArrayList<>();
        carréMagiqueBacktracking(sols, new int[n][n], 0, 0);
        return sols;
    }

    private static void carréMagiqueBacktracking(List<int[][]> sols, int[][] sol, int lg, int col) {
        int n = sol.length;
        for (int val = 1; val <= n * n; val++) {
            sol[lg][col] = val;
            if (carréMagiqueEstAcceptable(sol, lg, col)) {
                if (lg == n - 1 && col == n - 1) {
                    sols.add(cloneArray(sol));
                } else {
                    if (col == n - 1) {
                        carréMagiqueBacktracking(sols, sol, lg + 1, 0);
                    } else {
                        carréMagiqueBacktracking(sols, sol, lg, col + 1);
                    }
                }
            }
        }
    }

    private static boolean carréMagiqueValUniques(int[][] carré, int lg, int col) {
        boolean ok = true;
        int n = carré.length;
        int i = 0;
        while (ok && i <= lg) {
            int j = 0;
            int colMax = (i < lg) ? n : col;
            while (ok && j < colMax) {
                ok = carré[i][j] != carré[lg][col];
                j++;
            }
            i++;
        }
        return ok;
    }

    private static int carréMagiqueSommeCol(int[][] carré, int col) {
        int somme = 0;
        for (int[] carré1 : carré) {
            somme += carré1[col];
        }
        return somme;
    }

    private static int carréMagiqueSommeDiagA(int[][] carré) {
        int somme = 0;
        for (int col = 0; col < carré.length; col++) {
            somme += carré[carré.length - 1 - col][col];
        }
        return somme;
    }

    private static int carréMagiqueSommeDiagD(int[][] carré) {
        int somme = 0;
        for (int col = 0; col < carré.length; col++) {
            somme += carré[col][col];
        }
        return somme;
    }

    private static int carréMagiqueSommeLg(int[][] carré, int lg) {
        int somme = 0;
        for (int col = 0; col < carré.length; col++) {
            somme += carré[lg][col];
        }
        return somme;
    }

    /**
     * Clones the provided array
     *
     * @param src
     * @return a new clone of the provided array
     */
    public static int[][] cloneArray(int[][] src) {
        int length = src.length;
        int[][] target = new int[length][src[0].length];
        for (int i = 0; i < length; i++) {
            System.arraycopy(src[i], 0, target[i], 0, src[i].length);
        }
        return target;
    }
}
