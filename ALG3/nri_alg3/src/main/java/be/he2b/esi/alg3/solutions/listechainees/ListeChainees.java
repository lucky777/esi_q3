/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.listechainees;

import be.he2b.esi.alg3.*;
import java.util.Collection;

/**
 *
 * @author nri
 */
public class ListeChainees {

    /**
     * Ajoute les éléments d'une collection à une liste chaînée.
     *
     * @param <T>
     * @param l
     * @param coll
     */
    public static <T> void populate(ListeChainee<T> l, Collection<T> coll) {
        ElementListe<T> last = getLastElement(l);
        for (T elt : coll) {
            if (last == null) {
                l.insérerTête(elt);
                last = l.getPremier();
            } else {
                l.insérerAprès(last, elt);
                last = last.getSuivant();
            }
        }
    }

    public static <T> ListeChainee<T> make(Collection<T> coll) {
        ListeChainee<T> l = new ListeChainee<>();
        populate(l, coll);
        return l;
    }

    public static <T> int taille(ListeChainee<T> l) { // itératif
        int taille = 0;
        for (ElementListe elt = l.getPremier(); elt != null; elt = elt.getSuivant()) {
            taille++;
        }
        return taille;
    }

    public static <T> int tailleRec(ListeChainee<T> l) { // façade
        return tailleRec(l.getPremier());
    }

    private static int tailleRec(ElementListe cur) { // récursif
        return (cur == null) ? 0 : 1 + tailleRec(cur.getSuivant());
    }

    /**
     * Move an ElementListe elsewhere.
     *
     * The <code>ElementListe</code> that follows {@code srcPrevious} is moved
     * after {@code targetPrevious}.
     *
     * If {@code srcPrevious} is null, the first <code>ElementListe</code> of
     * {@code srcList} is moved.
     *
     * If {@code targetPrevious} is null, the <code>ElementListe</code> is moved
     * to the beginning of {@code targetList}.
     *
     * @param <T> Generic type.
     * @param srcList List where the element currently lies.
     * @param srcPrevious The preceding element. Its "next" Element will be
     * moved. must be non-null.
     * @param targetList Target list, where the element is going.
     * @param targetPrevious Insert after this element.
     * @throws IllegalArgumentException If the element following
     * {@code srcPrevious} is null.
     */
    static <T> void moveElementAfter(
            ListeChainee<T> srcList, ElementListe<T> srcPrevious,
            ListeChainee<T> targetList, ElementListe<T> targetPrevious) {
        final ElementListe<T> movingElement = nextEL(srcList, srcPrevious);
        if (movingElement == null) {
            throw new IllegalArgumentException("L'élément " + srcPrevious + " n'a pas de suivant. Rien à bouger !");
        }
        final ElementListe<T> srcNext = movingElement.getSuivant();
        if (srcPrevious == null) {
            srcList.setPremier(srcNext);
        } else {
            srcPrevious.setSuivant(srcNext);
        }
        final ElementListe<T> toNext = nextEL(targetList, targetPrevious);
        movingElement.setSuivant(toNext);
        if (targetPrevious == null) {
            targetList.setPremier(movingElement);
        } else {
            targetPrevious.setSuivant(movingElement);
        }

    }

    private static <T> ElementListe<T> nextEL(ListeChainee<T> l, ElementListe<T> prec) {
        return (prec == null) ? l.getPremier() : prec.getSuivant();
    }

    private static <T> ElementListe<T> getLastElement(ListeChainee<T> l) {
        ElementListe<T> last = l.getPremier();
        if (last != null) {
            while (last.getSuivant() != null) {
                last = last.getSuivant();
            }
        }
        return last;
    }
}
