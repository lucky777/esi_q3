/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

import java.util.Objects;

/**
 *
 * @author nri
 * @param <T> Type des éléments de la liste.
 */
public class ListeBD<T> {

    private ElementListeBD<T> premier;
    private ElementListeBD<T> dernier;

    public ListeBD() {
        this.premier = null;
        this.dernier = null;
    }

    /**
     * Get the value of dernier
     *
     * @return the value of dernier
     */
    public ElementListeBD<T> getDernier() {
        return this.dernier;
    }

    /**
     * Set the value of dernier.
     *
     * For the sake of self-consistency, this also sets the value of premier
     * accordingly.
     *
     * @param elt new value of dernier
     */
    public void setDernier(ElementListeBD<T> elt) {
        this.dernier = elt;
        while (elt != null && elt.getPrecedent() != null) {
            elt = elt.getPrecedent();
        }
        this.premier = elt;

    }

    public ElementListeBD<T> getPremier() {
        return this.premier;
    }

    /**
     * sets the value of premier.
     *
     * For the sake of self-consistency, this also sets the value of dernier
     * accordingly.
     *
     * @param elt
     */
    public void setPremier(ElementListeBD<T> elt) {
        this.premier = elt;
        while (elt != null && elt.getSuivant() != null) {
            elt = elt.getSuivant();
        }
        this.dernier = elt;
    }

    public void insérerTête(T val) {
        if (estVide()) {
            this.premier = this.dernier = new ElementListeBD<>(val, null, null);
        } else {
            ElementListeBD<T> newElt = new ElementListeBD<>(val, this.premier, null);
            this.premier.setPrecedent(newElt);
            this.premier = newElt;
        }
    }

    public void insérerFin(T val) {
        if (estVide()) {
            this.premier = this.dernier = new ElementListeBD<>(val, null, null);
        } else {
            ElementListeBD<T> newElt = new ElementListeBD<>(val, null, this.dernier);
            this.dernier.setSuivant(newElt);
            this.dernier = newElt;
        }
    }

    public void insérerAprès(ElementListeBD<T> before, T val) {
        if (before == null) {
            throw new IllegalArgumentException();
        }
        final ElementListeBD<T> after = before.getSuivant();
        final ElementListeBD<T> newElt = new ElementListeBD<>(val, after, before);
        before.setSuivant(newElt);
        if (after != null) {
            before.setPrecedent(newElt);
        } else {
            this.dernier = newElt;
        }
    }

    public void insérerAvant(ElementListeBD<T> after, T val) {
        if (after == null) {
            throw new IllegalArgumentException();
        }
        final ElementListeBD<T> before = after.getPrecedent();
        final ElementListeBD<T> newElt = new ElementListeBD<>(val, after, before);
        after.setPrecedent(newElt);
        if (before != null) {
            before.setSuivant(newElt);
        } else {
            this.premier = newElt;
        }
    }

    public void supprimerTête() {
        if (this.premier == null) {
            throw new NullPointerException();
        }
        this.premier = this.premier.getSuivant();
        if (this.premier == null) {
            this.dernier = null;
        }
    }

    public void supprimerFin() {
        if (this.dernier == null) {
            throw new NullPointerException();
        }
        this.dernier = this.dernier.getPrecedent();
        if (this.dernier == null) {
            this.premier = null;
        }
    }

    public void supprimerAprès(ElementListeBD<T> elt) {
        if (elt == null || elt.getSuivant() == null) {
            throw new IllegalArgumentException();
        }
        final ElementListeBD<T> newNext = elt.getSuivant().getSuivant();
        elt.setSuivant(newNext);
        if (newNext == null) {
            this.dernier = elt;
        }
    }

    public void supprimerAvant(ElementListeBD<T> elt) {
        if (elt == null || elt.getPrecedent() == null) {
            throw new IllegalArgumentException();
        }
        final ElementListeBD<T> newPrevious = elt.getPrecedent().getPrecedent();
        elt.setPrecedent(newPrevious);
        if (newPrevious == null) {
            this.dernier = elt;
        }
    }

    public boolean estVide() {
        return this.premier == null;
    }

    // Cette méthode n'est pas dans le cours d'ALG3,
    // elle sert seulement pour afficher la liste  à l'écran.
    @Override
    public String toString() {
        ElementListeBD cur = this.premier;
        StringBuilder result = new StringBuilder();
        result.append("(");
        if (cur != null) {
            result.append(cur.toString());
            cur = cur.getSuivant();
        }
        while (cur != null) {
            result.append(" <-> ").append(cur.toString());
            cur = cur.getSuivant();
        }
        result.append(")");
        return result.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.premier);
        hash = 83 * hash + Objects.hashCode(this.dernier);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListeBD<?> other = (ListeBD<?>) obj;
        if (!Objects.equals(this.premier, other.premier)) {
            return false;
        }
        if (!Objects.equals(this.dernier, other.dernier)) {
            return false;
        }
        return true;
    }

}
