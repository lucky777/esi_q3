/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.recursivite;

/**
 *
 * @author nri
 */
public class Exercice11_Taches {

    static int nbAppels;

    public static void main(String[] args) {
        
    }

    public static boolean estDansMêmeTâche(Couleur[][] tab, int i1, int j1, int i2, int j2) {
        boolean[][] visités = new boolean[tab.length][tab[0].length];
        nbAppels = 0;
        final boolean reponse = estDMTRec(tab, i1, j1, i2, j2, visités);
        System.out.printf("Nombre d'appels: %d\n", nbAppels);
        return reponse;
    }

    private static boolean estDMTRec(Couleur[][] tab, int x, int y, int xD, int yD, boolean[][] visités) {
        nbAppels++;
        if (!estDansTableau(tab, x, y)
                || !tab[x][y].equals(tab[xD][yD])
                || visités[x][y]) {
            return false;
        }
        if (xD == x && yD == y) {
            return true;
        }
        visités[x][y] = true;
        return estDMTRec(tab, x + 1, y, xD, yD, visités)
                || estDMTRec(tab, x - 1, y, xD, yD, visités)
                || estDMTRec(tab, x, y - 1, xD, yD, visités)
                || estDMTRec(tab, x, y + 1, xD, yD, visités);
    }

    private static boolean estDansTableau(Object[][] tab, int x, int y) {
        return estDansTableau(tab, x) && estDansTableau(tab[x], y);
    }

    private static boolean estDansTableau(Object[] tab, int x) {
        return 0 <= x && x < tab.length;
    }
}
