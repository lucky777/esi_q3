/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.arbres;

import be.he2b.esi.alg3.Arbre;
import be.he2b.esi.alg3.Noeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouveauNoeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouvelArbre;

/**
 *
 * @author nri
 */
public class Exercice07 {

    public static void main(String[] args) {
        Arbre<Integer> a
                = nouvelArbre(null,
                        nouveauNoeud(null,
                                nouveauNoeud(2),
                                nouveauNoeud(5)),
                        nouveauNoeud(2),
                        nouveauNoeud(10));
        System.out.println(additionRec(a.getRacine()));
    }

    static int additionRec(Noeud<Integer> n) {
        if (n.getNbFils() == 0) {
            return n.getValeur();
        }
        
        int som = 0;
        for (int i = 0; i < n.getNbFils(); i++) {
            Noeud<Integer> nc = n.getFils(i);
            som += additionRec(nc);
        }
        n.setValeur(som);
        return som;
    }

}
