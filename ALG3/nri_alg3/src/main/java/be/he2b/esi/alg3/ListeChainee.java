/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

import java.util.Objects;

/**
 *
 * @author nri
 * @param <T> Type des éléments de la liste.
 */
public class ListeChainee<T> {

    private ElementListe<T> premier;

    public ElementListe<T> getPremier() {
        return premier;
    }

    public void setPremier(ElementListe<T> elt) {
        this.premier = elt;
    }

    public ListeChainee() {
        this.premier = null;
    }

    public void insérerTête(T val) {
        this.premier = new ElementListe<>(val, this.premier);
    }

    public void insérerAprès(ElementListe<T> elt, T val) {
        if (elt == null) {
            throw new IllegalArgumentException();
        }
        elt.setSuivant(new ElementListe<>(val, elt.getSuivant()));
    }

    public void supprimerTête() {
        if (this.premier == null) {
            throw new NullPointerException();
        }
        this.premier = this.premier.getSuivant();
    }

    public void supprimerAprès(ElementListe<T> elt) {
        if (elt == null || elt.getSuivant() == null) {
            throw new IllegalArgumentException();
        }
        elt.setSuivant(elt.getSuivant().getSuivant());
    }

    public boolean estVide() {
        return this.premier == null;
    }

    // Cette méthode n'est pas dans le cours d'ALG3,
    // elle sert seulement pour afficher la liste  à l'écran.
    @Override
    public String toString() {
        ElementListe cur = this.premier;
        StringBuilder result = new StringBuilder();
        result.append("(");
        if (cur != null) {
            result.append(cur.toString());
            cur = cur.getSuivant();
        }
        while (cur != null) {
            result.append(", ").append(cur.toString());
            cur = cur.getSuivant();
        }
        result.append(")");
        return result.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.premier);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListeChainee<?> other = (ListeChainee<?>) obj;
        if (!Objects.equals(this.premier, other.premier)) {
            return false;
        }
        return true;
    }
    
    

}
