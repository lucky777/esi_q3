package be.he2b.esi.alg3.solutions.arbres;

import be.he2b.esi.alg3.Arbre;
import be.he2b.esi.alg3.Noeud;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class Arbres {

    @SafeVarargs
    public static <T> Arbre<T> nouvelArbre(T valeur, Noeud<T>... enfants) {
        Arbre<T> arbre = new Arbre<>();
        arbre.setRacine(nouveauNoeud(valeur, enfants));
        return arbre;
    }

    @SafeVarargs
    public static <T> Noeud<T> nouveauNoeud(T valeur, Noeud<T>... enfants) {
        Noeud<T> noeud = new Noeud<>(valeur);
        for (Noeud<T> enfant : enfants) {
            noeud.ajouterFils(enfant);
        }
        return noeud;
    }

}
