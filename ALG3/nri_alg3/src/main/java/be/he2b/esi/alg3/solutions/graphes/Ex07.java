package be.he2b.esi.alg3.solutions.graphes;

public class Ex07 {

    static void supprimeArêtesValeursPaires(boolean[][] M, int[] valNoeuds) {
        for (int lg = 0; lg < M.length; lg++) {
            if (valNoeuds[lg] % 2 == 0) { // on parcourt la ligne seulement si la valeur est paire, sinon ça ne sert à rien.
                for (int col = 0; col < M[lg].length; col++) {
                    if (M[lg][col] && valNoeuds[col] % 2 == 0) {
                        M[lg][col] = false;
                    }
                }
            }
        }
    }
}
