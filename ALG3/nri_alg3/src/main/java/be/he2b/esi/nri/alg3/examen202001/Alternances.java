package be.he2b.esi.nri.alg3.examen202001;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nri
 */
public class Alternances {

    public static void main(String[] args) {

        afficherAlternances(new int[]{1, 5, 9}, new int[]{7, 8, 11});
    }

    public static void afficherAlternances(int[] A, int[] B) {
        afficherAlternances_backtracking(A, B, 0, 0, new ArrayList<>());
    }

    public static void afficherAlternances_backtracking(int[] A, int[] B, int iA, int iB, List<Integer> liste) {
        for (int i = iA; i < A.length; i++) {
            liste.add(A[i]);
            if (afficherAlternances_estOk(liste)) {
                for (int j = iB; j < B.length; j++) {
                    liste.add(B[j]);
                    if (afficherAlternances_estOk(liste)) {
                        System.out.println(liste.toString());
                        afficherAlternances_backtracking(A, B, i + 1, j + 1, liste);
                    }
                    liste.remove(liste.size() - 1);
                }
            }
            liste.remove(liste.size() - 1);
        }
    }

    public static boolean afficherAlternances_estOk(List<Integer> liste) {
        int n = liste.size();
        return n < 2 || liste.get(n - 2) < liste.get(n - 1);
    }
}
