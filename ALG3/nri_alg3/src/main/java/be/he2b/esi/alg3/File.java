/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 * @param <T> type des éléments de la file
 */
public interface File<T> {

    public void enfiler(T val);

    public T défiler();

    public T tête();

    public boolean estVide();

}
