package be.he2b.esi.alg3;

import java.util.Objects;

/**
 *
 * @author nri
 * @param <T>
 */
public class ElementListeBD<T> {

    private T valeur;
    private ElementListeBD<T> suivant;

    private ElementListeBD<T> precedent;

    public ElementListeBD(T val, ElementListeBD<T> next, ElementListeBD<T> prev) {
        this.valeur = val;
        this.suivant = next;
        this.precedent = prev;
    }

    /**
     * Get the value of precedent
     *
     * @return the value of precedent
     */
    public ElementListeBD<T> getPrecedent() {
        return precedent;
    }

    /**
     * Set the value of precedent
     *
     * @param precedent new value of precedent
     */
    public void setPrecedent(ElementListeBD<T> precedent) {
        this.precedent = precedent;
    }

    public T getValeur() {
        return valeur;
    }

    public void setValeur(T valeur) {
        this.valeur = valeur;
    }

    public ElementListeBD<T> getSuivant() {
        return suivant;
    }

    public void setSuivant(ElementListeBD<T> suivant) {
        this.suivant = suivant;
    }

    @Override
    public String toString() {
        return valeur.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.valeur);
        hash = 23 * hash + Objects.hashCode(this.suivant);
        hash = 23 * hash + Objects.hashCode(this.precedent);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElementListeBD<?> other = (ElementListeBD<?>) obj;
        if (!Objects.equals(this.valeur, other.valeur)) {
            return false;
        }
        if (!Objects.equals(this.suivant, other.suivant)) {
            return false;
        }
        if (!Objects.equals(this.precedent, other.precedent)) {
            return false;
        }
        return true;
    }

    
}
