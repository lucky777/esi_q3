package be.he2b.esi.alg3.solutions.graphes;

import be.he2b.esi.alg3.ElementListe;
import be.he2b.esi.alg3.ListeChainee;
import be.he2b.esi.alg3.solutions.listechainees.ListeChainees;
import java.util.List;

public class Ex09 {

    public static void main(String[] args) {
        List<Integer>[] t = (List<Integer>[]) new List[]{
            List.of(1, 3),
            List.of(2),
            List.of(0),
            List.of(4),
            List.of(0)
        };
        Graphes.printAccessibilityMatrix(listeVersMatrice(t));

        ListeChainee<Integer>[] t2 = (ListeChainee<Integer>[]) new ListeChainee[]{
            ListeChainees.make(List.of(1, 3)),
            ListeChainees.make(List.of(2)),
            ListeChainees.make(List.of(0)),
            ListeChainees.make(List.of(4)),
            ListeChainees.make(List.of(0))
        };
        Graphes.printAccessibilityMatrix(listechaineeVersMatrice(t2));
    }

    static boolean[][] listeVersMatrice(List<Integer>[] t) {
        int n = t.length;
        boolean[][] M = new boolean[n][n];
        for (int noeud = 0; noeud < t.length; noeud++) {
            List<Integer> voisins = t[noeud];
            for (Integer voisin : voisins) {
                M[noeud][voisin] = true;
            }
        }
        return M;
    }

    static boolean[][] listechaineeVersMatrice(ListeChainee<Integer>[] t) {
        int n = t.length;
        boolean[][] M = new boolean[n][n];
        for (int noeud = 0; noeud < t.length; noeud++) {
            ElementListe<Integer> voisin = t[noeud].getPremier();
            while (voisin != null) {
                M[noeud][voisin.getValeur()] = true;
                voisin = voisin.getSuivant();
            }
        }
        return M;
    }

}
