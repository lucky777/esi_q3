/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

/**
 *
 * @author nri
 */
public class FileTab<T> implements File<T> {

    private int premier;
    private int dernier;
    private final T[] tab;

    public FileTab(int tailleMax) {
        if (tailleMax < 1) {
            throw new IllegalArgumentException("Il faut pouvoir mettre quelque chose au moins !");
        }
        this.tab = (T[])(new Object[tailleMax + 1]);
        this.premier = 0;
        this.dernier = tab.length - 1;
    }

    @Override
    public boolean estVide() {
        return next(this.dernier) == this.premier;
    }

    public boolean estPleine() {
        return next(next(this.dernier)) == this.premier;
    }

    private int next(int dernier) {
        return (dernier + 1) % tab.length;
    }

    @Override
    public T tête() {
        if (estVide()) {
            throw new IllegalStateException("La File est vide");
        }
        return tab[premier];
    }

    @Override
    public T défiler() {
        T val = tête();
        tab[premier] = null;
        premier = next(premier);
        return val;
    }

    @Override
    public void enfiler(T val) {
        if (estPleine()) {
            throw new IllegalStateException("La File est pleine");
        }
        dernier = next(dernier);
        tab[dernier] = val;
    }

}
