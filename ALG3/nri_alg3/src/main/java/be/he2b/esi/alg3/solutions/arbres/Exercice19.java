/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.arbres;

import be.he2b.esi.alg3.Arbre;
import be.he2b.esi.alg3.Noeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouveauNoeud;
import static be.he2b.esi.alg3.solutions.arbres.Arbres.nouvelArbre;

/**
 *
 * @author nri
 */
public class Exercice19 {

    public static void main(String[] args) {
        Arbre<String> a
                = nouvelArbre("Meuse",
                        nouveauNoeud("Semois",
                                nouveauNoeud("Vierre"),
                                nouveauNoeud("Rulles")),
                        nouveauNoeud("Lesse",
                                nouveauNoeud("Lomme")),
                        nouveauNoeud("Bocq"),
                        nouveauNoeud("Sambre",
                                nouveauNoeud("Omeau"),
                                nouveauNoeud("Eau d'Heure")),
                        nouveauNoeud("Ourthe",
                                nouveauNoeud("Amblève",
                                        nouveauNoeud("Warche"),
                                        nouveauNoeud("Ninglinspo")),
                                nouveauNoeud("Somme")));
        afficherChemin(a, "Somme");
        afficherChemin(a, "Eau d'Heure");
        afficherChemin(a, "Senne");
    }

    private static void afficherChemin(Arbre<String> a, String valeur) {
        System.out.println("On cherche " + valeur + " :");
        if (!afficherChemin(a.getRacine(), valeur)) {
            System.out.println("Valeur non-trouvée !");
        }
    }

    private static boolean afficherChemin(Noeud<String> n, String valeur) {
        boolean trouvé = n.getValeur().equals(valeur);
        for (int i = 0; i < n.getNbFils(); i++) {
            Noeud<String> nc = n.getFils(i);
            // ici on short-circuit car si la valeur est déjà trouvée, inutile de chercher plus loin.
            trouvé = trouvé || afficherChemin(nc, valeur);
        }
        if (trouvé) {
            System.out.println(n.getValeur());
        }
        return trouvé;
    }

}
