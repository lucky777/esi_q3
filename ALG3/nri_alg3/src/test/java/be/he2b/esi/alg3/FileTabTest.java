/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author nri
 */
public class FileTabTest {
    
    public FileTabTest() {
    }
    
    @Test
    public void testFileAvecUnePlace() {
        FileTab<Integer> instance = new FileTab<>(1);
        instance.enfiler(5);
        assertTrue(instance.estPleine());
        assertFalse(instance.estVide());
    }

    @Test
    public void testEstVide() {
        FileTab instance = new FileTab<Integer>(5);
        boolean expResult = true;
        boolean result = instance.estVide();
        assertEquals(expResult, result);
    }

    @Test
    public void testEstPleine_false() {
        FileTab instance = new FileTab<Integer>(5);
        boolean expResult = false;
        boolean result = instance.estPleine();
        assertEquals(expResult, result);
    }

    @Test
    public void testEstPleine_true() {
        FileTab instance = new FileTab<Integer>(5);
        instance.enfiler(2);
        instance.enfiler(2);
        instance.enfiler(2);
        instance.enfiler(2);
        instance.enfiler(2);
        boolean expResult = true;
        boolean result = instance.estPleine();
        assertEquals(expResult, result);
    }

    @Test
    public void testTête() {
        FileTab instance = new FileTab<Integer>(5);
        instance.enfiler(5);
        instance.enfiler(4);
        Object expResult = 5;
        Object result = instance.tête();
        assertEquals(expResult, result);
    }

    @Test
    public void testDéfiler() {
        FileTab instance = new FileTab<Integer>(5);
        instance.enfiler(3);
        Object expResult = 3;
        Object result = instance.défiler();
        assertEquals(expResult, result);
        assertTrue(instance.estVide());
    }

}
