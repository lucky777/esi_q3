package be.he2b.esi.alg3.solutions.listechainees;

import be.he2b.esi.alg3.ListeChainee;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author nri
 */
public class ListeChaineesTest {

    @Test
    public void testTaille_tailleNulle() {
        int expResult = 0;
        int result = ListeChainees.taille(new ListeChainee<>());
        assertEquals(expResult, result);
    }

    @Test
    public void testTailleRec_tailleNulle() {
        int expResult = 0;
        int result = ListeChainees.tailleRec(new ListeChainee<>());
        assertEquals(expResult, result);
    }

    @Test
    public void testTaille_tailleNonNulle() {
        int expResult = 3;
        int result = ListeChainees.taille(ListeChainees.make(List.of(5, 1, 0)));
        assertEquals(expResult, result);
    }

    @Test
    public void testTailleRec_tailleNonNulle() {
        int expResult = 3;
        int result = ListeChainees.taille(ListeChainees.make(List.of(5, 1, 0)));
        assertEquals(expResult, result);
    }

    @Test
    public void testMoveElementAfter_HeadToHead() {
        ListeChainee<Integer> src = ListeChainees.make(List.of(2, 4, 15));
        ListeChainee<Integer> target = ListeChainees.make(List.of(1, 2, 10));
        ListeChainees.moveElementAfter(src, null, target, null);
        assertEquals(ListeChainees.make(List.of(4, 15)), src);
        assertEquals(ListeChainees.make(List.of(2, 1, 2, 10)), target);
    }

    @Test
    public void testMoveElementAfter_HeadToEnd() {
        ListeChainee<Integer> src = ListeChainees.make(List.of(2, 4, 15));
        ListeChainee<Integer> target = ListeChainees.make(List.of(1, 2, 10));
        ListeChainees.moveElementAfter(src, null, target, target.getPremier().getSuivant().getSuivant());
        assertEquals(ListeChainees.make(List.of(4, 15)), src);
        assertEquals(ListeChainees.make(List.of(1, 2, 10, 2)), target);
    }

    @Test
    public void testMoveElementAfter_EndToHead() {
        ListeChainee<Integer> src = ListeChainees.make(List.of(2, 4, 15));
        ListeChainee<Integer> target = ListeChainees.make(List.of(1, 2, 10));
        ListeChainees.moveElementAfter(src, src.getPremier().getSuivant(), target, null);
        assertEquals(ListeChainees.make(List.of(2, 4)), src);
        assertEquals(ListeChainees.make(List.of(15, 1, 2, 10)), target);
    }

    @Test
    public void testMoveElementAfter_MiddleToMiddle() {
        ListeChainee<Integer> src = ListeChainees.make(List.of(2, 4, 15));
        ListeChainee<Integer> target = ListeChainees.make(List.of(1, 2, 10));
        ListeChainees.moveElementAfter(src, src.getPremier(), target, target.getPremier());
        assertEquals(ListeChainees.make(List.of(2, 15)), src);
        assertEquals(ListeChainees.make(List.of(1, 4, 2, 10)), target);
    }

}
