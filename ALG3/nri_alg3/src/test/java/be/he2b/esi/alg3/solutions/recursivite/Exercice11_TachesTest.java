/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.he2b.esi.alg3.solutions.recursivite;

import static be.he2b.esi.alg3.solutions.recursivite.Couleur.B;
import static be.he2b.esi.alg3.solutions.recursivite.Couleur.G;
import static be.he2b.esi.alg3.solutions.recursivite.Couleur.R;
import static be.he2b.esi.alg3.solutions.recursivite.Couleur.Y;
import static be.he2b.esi.alg3.solutions.recursivite.Exercice11_Taches.estDansMêmeTâche;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author nri
 */
public class Exercice11_TachesTest {

    public Exercice11_TachesTest() {
    }

    @Test
    public void testEstDansMêmeTâche() {
        Couleur[][] tab = {
            //0,1, 2, 3, 4, 5, 6, 7, 8, 9,10,11
            {Y, R, R, G, G, G, G, G, B, B, B, B},//0
            {Y, Y, R, R, G, G, G, G, Y, B, B, B},//1
            {Y, Y, Y, R, G, G, G, G, Y, B, B, B},//2
            {Y, Y, Y, Y, R, R, Y, Y, Y, Y, G, G},//3
            {G, G, G, B, R, R, B, B, B, G, G, G},//4
            {G, G, B, B, R, R, R, B, B, G, G, G},//5
            {G, G, B, B, B, R, R, B, R, R, R, G},//6
            {G, G, G, G, B, G, G, R, R, R, R, G},//7
            {B, B, G, G, G, G, R, R, R, B, B, B},//8
            {B, B, G, Y, Y, G, R, R, B, B, B, B},//9
            {B, B, B, Y, Y, G, G, R, B, B, B, B},//10
            {B, B, B, B, Y, Y, Y, R, R, R, R, R} //11
        };
        assertFalse(estDansMêmeTâche(tab, 0, 0, 3, 6));
        assertTrue(estDansMêmeTâche(tab, 1, 8, 3, 6));
        assertTrue(estDansMêmeTâche(tab, 7, 10, 11, 11));
    }

}
