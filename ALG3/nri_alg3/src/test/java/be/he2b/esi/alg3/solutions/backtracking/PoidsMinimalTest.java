package be.he2b.esi.alg3.solutions.backtracking;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Nicolas Richard <nrichard@he2b.be>
 */
public class PoidsMinimalTest {

    @Test
    public void testParcoursMinimum_NonRectiligne() {
        System.out.println("parcoursMinimum");
        Jeu jeu = new Jeu(new int[][]{
            {1, 1, 3},
            {3, 2, 6},
            {1, 1, 2}
        });
        List<Case> expResult = List.of(
                new Case(0, 0),
                new Case(0, 1),
                new Case(1, 1),
                new Case(2, 1),
                new Case(2, 2)
        );
        List<Case> result = jeu.parcoursMinimum().asList();
        assertEquals(expResult, result);
    }

    @Test
    public void testParcoursMinimum_DroitePuisBas() {
        System.out.println("parcoursMinimum");
        Jeu jeu = new Jeu(new int[][]{
            {1, 1, 3},
            {3, 5, 0},
            {1, 1, 2}
        });
        List<Case> expResult = List.of(
                new Case(0, 0),
                new Case(0, 1),
                new Case(0, 2),
                new Case(1, 2),
                new Case(2, 2)
        );
        List<Case> result = jeu.parcoursMinimum().asList();
        assertEquals(expResult, result);
    }

    @Test
    public void testParcoursMinimum_BasPuisDroite() {
        System.out.println("parcoursMinimum");
        Jeu jeu = new Jeu(new int[][]{
            {1, 3, 3},
            {3, 2, 6},
            {1, 1, 2}
        });
        List<Case> expResult = List.of(
                new Case(0, 0),
                new Case(1, 0),
                new Case(2, 0),
                new Case(2, 1),
                new Case(2, 2)
        );
        List<Case> result = jeu.parcoursMinimum().asList();
        assertEquals(expResult, result);
    }

    @Test
    public void testParcoursMinimum_ObstacleAEviter() {
        System.out.println("parcoursMinimum");
        Jeu jeu = new Jeu(new int[][]{
            {1,   1,  1},
            {10, 10,  1},
            {1,   1,  1},
            {1,  10, 10},
            {1,   1,  1}
        });
        List<Case> expResult = List.of(
                new Case(0, 0),
                new Case(0, 1),
                new Case(0, 2),
                new Case(1, 2),
                new Case(2, 2),
                new Case(2, 1),
                new Case(2, 0),
                new Case(3, 0),
                new Case(4, 0),
                new Case(4, 1),
                new Case(4, 2)
        );
        List<Case> result = jeu.parcoursMinimum().asList();
        assertEquals(expResult, result);
    }

}
