//Exercice 5
//Page 101

public class Fresque_Tricolore {
    public static void main(String[] args) {
        
    }
    
    public static boolean bt(int[] fresque, int nb_cases) {
        for (int i=1; i<3; i++) { //nb_cases = nb of colors already placed
            fresque[nb_cases] = i;
            if(isOk(fresque, nb_cases)) {
                nb_cases++;
                if(nb_cases == fresque.length || bt(fresque, nb_cases)) {
                    return true;
                }
                nb_cases--;
            }
        }
        //No valid solution for fresque
        return false;
    }
    
    public static void facade(int n) {
        int[] fresque = new int[n];
        if(bt(fresque, 0)) {
            show_fresque(fresque);
        } else {
            System.out.println("No solution for this fresque!");
        }
    }

    public static boolean isOk(int[] fresque, int pos) {
        for(int i=1; i<=pos/2; i++) {
            if(fresque[pos] == fresque[pos-i] && fresque[pos-i] == fresque[pos-i*2]) {
                return false;
            }
        }
        return true;
    }
}
