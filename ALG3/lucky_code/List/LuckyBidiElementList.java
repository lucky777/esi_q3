public class LuckyBidiElementList<T> {

    private T value;
    private LuckyBidiElementList<T> previous;
    private LuckyBidiElementList<T> next;
    // First call
    public LuckyBidiElementList(T value) {
        this.value = value;
        this.previous = null;
        this.next = null;
    }

    // Next calls
    public LuckyBidiElementList(T value, LuckyBidiElementList<T> previous, LuckyBidiElementList<T> next) {
        this.value = value;
        this.previous = previous;
        this.next = next;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public LuckyBidiElementList<T> getPrevious() {
        return this.previous;
    }

    public void setPrevious(LuckyBidiElementList<T> element) {
        this.previous = element;
    }

    public LuckyBidiElementList<T> getNext() {
        return this.next;
    }

    public void setNext(LuckyBidiElementList<T> element) {
        this.next = element;
    }
}