import java.util.List;
import java.util.LinkedList;

public class LuckyStack<T> {

    private List<T> list;

    public LuckyStack() {
        this.list = new LinkedList<>();
    }

    public void push(T value) {
        this.list.add(value);
    }

    public T pop() {
        if (this.list.isEmpty()) {
            System.out.println("error: LuckyStack is empty!");
            return null;
        }
        return this.list.remove(this.list.size() - 1);
    }

    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    public T top() {
        if (this.list.isEmpty()) {
            System.out.println("error: LuckyStack is empty!");
            return null;
        }

        return this.list.get(this.list.size() - 1);
    }
}