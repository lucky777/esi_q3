import java.util.List;
import java.util.LinkedList;

public class LuckyQueue<T> {

    private List<T> list;

    public LuckyQueue() {
        this.list = new LinkedList<>();
    }

    public void enQueue(T value) {
        this.list.add(value);
    }

    public T deQueue() {
        if (this.isEmpty()) {
            System.out.println("error: LuckyQueue is empty!");
            return null;
        }
        return this.list.remove(0);
    }

    public T head() {
            if (this.isEmpty()) {
                System.out.println("error: LuckyQueue is empty!");
                return null;
            }
            return this.list.get(0);
    }

    public boolean isEmpty() {
        return this.list.isEmpty();
        //return this.list.size() == 0;       Good too
    }
}