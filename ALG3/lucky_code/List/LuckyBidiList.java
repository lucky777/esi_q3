public class LuckyBidiList<T> {
    private LuckyBidiElementList<T> first;
    private LuckyBidiElementList<T> last;

    public LuckyBidiList() {
        this.first = null;
        this.last = null;
    }

    public LuckyBidiElementList<T> getFirst() {
        return this.first;
    }

    public void setFirst(LuckyBidiElementList<T> element) {
        if(this.first.getNext() != null) {
            this.first.getNext().setPrevious(element);
        }
        this.first = element;
    }

    public LuckyBidiElementList<T> getLast() {
        return this.last;
    }

    public void setLast(LuckyBidiElementList<T> element) {
        if(this.last.getPrevious() != null) {
            this.last.getPrevious().setNext(element);
        }
        this.last = element;
    }

    public boolean isEmpty() {
        return this.first == null;
    }

    public void insertHead(T value) {
        if (this.first == null) {
            this.first = new LuckyBidiElementList<T>(value);
            this.last = this.first;
        } else {
            this.first = new LuckyBidiElementList<T>(value, null, this.first);
            this.first.getNext().setPrevious(this.first);
        }
    }

    public void insertEnd(T value) {
        if (this.last == null) {
            this.last = new LuckyBidiElementList<T>(value);
            this.first = this.last;
        } else {
            this.last = new LuckyBidiElementList<T>(value, this.last.getPrevious(), null);
            this.last.getPrevious().setNext(this.first);
        }
    }

    public void insertAfter(LuckyBidiElementList<T> element, T value) {
        LuckyBidiElementList<T> tmp = new LuckyBidiElementList<>(value, element, element.getNext());
        if(element.getNext() != null) {
            element.getNext().setPrevious(tmp);
        } else {
            this.last = tmp;
        }
        element.setNext(tmp);
    }

    public void insertBefore(LuckyBidiElementList<T> element, T value) {
        LuckyBidiElementList<T> tmp = new LuckyBidiElementList<>(value, element.getPrevious(), element);
        if(element.getPrevious() != null) {
            element.getPrevious().setNext(tmp);
        } else {
            this.first = tmp;
        }
        element.setPrevious(tmp);
    }

    public void deleteHead() {
        if(this.first == null) {
            System.out.println("error: List is empty, nothing to delete");
        } else {
            if (this.first.getNext() != null) {        //On vérifie s'il y a un élément avant le dernier
                this.first.getNext().setPrevious(null);
                this.first = this.first.getNext();
            } else {    //S'il n'y a pas d'élément avant le dernier, alors il est premier aussi
                this.first = null;
                this.last = null;
            }
        }
    }

    public void deleteEnd() {
        if(this.last == null) {
            System.out.println("error: List is empty, nothing to delete");
        } else {
            if (this.last.getPrevious() != null) {     //On vérifie s'il y a un élément avant le dernier
                this.last.getPrevious().setNext(null);
                this.last = this.last.getPrevious();
            } else {    //S'il n'y a pas d'élément avant le dernier, alors il est premier aussi
                this.first = null;
                this.last = null;
            }
        }
    }

    public void delete(LuckyBidiElementList<T> element) {
        if(element == this.first && element == this.last) {    //On vérifie si l'élément est le seul
            this.first = null;
            this.last = null;
        }
        else if(element.getPrevious() == null) {    //On vérifie si l'élément est le premier
            element.getNext().setPrevious(null);
            this.first = element.getNext();
        }
        else if(element.getNext() == null) {    //On vérifie si l'élément est le dernier
            element.getPrevious().setNext(null);
            this.last = element.getPrevious();
        } else {    //L'élément n'est ni le seul, ni premier, ni dernier
            element.getPrevious().setNext(element.getNext());
            element.getNext().setPrevious(element.getPrevious());
        }
    }
}