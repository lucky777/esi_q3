public class LuckyElementList<T> {

    private T value;
    private LuckyElementList<T> next;

    //First call
    public LuckyElementList(T value) {
        this.value = value;
        this.next = null;
    }

    // Next calls
    public LuckyElementList(T value, LuckyElementList<T> LuckyElementList) {
        this.value = value;
        this.next = LuckyElementList;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public LuckyElementList<T> getNext() {
        return this.next;
    }

    public void setNext(LuckyElementList<T> element) {
        this.next = element;
    }
}
