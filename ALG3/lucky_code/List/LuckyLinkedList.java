public class LuckyLinkedList<T> {
    
    private LuckyElementList<T> first;

    public LuckyLinkedList() {
        this.first = null;
    }

    public LuckyElementList<T> getFirst() {
        return this.first;
    }

    public void setFirst(LuckyElementList<T> element) {
        this.first = element;
    }

    public boolean isEmpty() {
        return this.first == null;
    }

    public void insertHead(T value) {
        this.first = new LuckyElementList<T>(value, this.first);
    }

    public void deleteHead() {
        if (this.first == null) {
            System.out.println("error: LuckyLinkedList is empty");
        } else {
            this.first = this.first.getNext();
        }
    }

    public void insertAfter(LuckyElementList<T> element, T value) {
        if (element == null) {
            System.out.println("error: given element is null");
        } else {
            element.setNext(new LuckyElementList<T>(value, element.getNext()));
        }
    }

    public void deleteAfter(LuckyElementList<T> element) {
        if (element.getNext() == null) {
            System.out.println("error: LuckyLinkedList is empty");
        } else {
            element.setNext(element.getNext().getNext());
        }
    }

    public int getLength() {
        int cpt = 0;
        LuckyElementList<T> current = this.first;
        while(current != null) {
            cpt++;
            current = current.getNext();
        }
        return cpt;
    }
}