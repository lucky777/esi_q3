public class Eight_Queens {

    private static int cpt = 0;

    public static void main(String[] args) {
        solve();
    }

    public static int[] solve() {
        int[] queens = new int[8];
        int nb_queens = 0;

        for(int i=0; i<8; i++) {
            queens[i]= 8;
        }

        if (backtracking(queens, nb_queens)) {
            return queens;
        } else {
            return null;
        }
    }

    public static boolean backtracking(int[] queens, int nb_queens) {

        boolean found = false;
        int ln = 0;

        while(!found && ln < 8) { //!found && ln < 8 ... to show only first solution
            queens[nb_queens] = ln;
            if (is_good(queens, nb_queens)) {
                if (nb_queens == 7) {
                    found = true;  //Show only first solution
                    System.out.println("Solution " + (++cpt) + ":");
                    show(queens, nb_queens); //Show solutions
                } else {
                    found = backtracking(queens, nb_queens+1);
                }
            }
            ln++;
        }

        return found;
    }

    public static boolean is_good(int[] queens, int pos) {

        for(int i=0; i<pos; i++) {
            if (queens[i] == queens[pos]) {
                return false; //Line
            }

            if (Math.abs(queens[pos] - queens[i]) == pos - i) {
                return false; //Diagonal
            }
        }
        return true; //is_good
    }

    public static void show(int[] queens, int nb) {
        for(int i=0; i<8; i++) {
            System.out.println("---------------------------------");
            for(int j=0; j<8; j++) {
                System.out.print("| ");
                System.out.print((queens[j]==i) && j<=nb ? "X " : "  ");
            }
            System.out.println("|");
        }
        System.out.println("\n#################################\n");
    }
}