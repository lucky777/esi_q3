public class RoyWarshall {
    public static void main(String[] args) {
        boolean[][] tab = {{false, true, false, true, false},
                           {false, false, true, false, false},
                           {true, false, false, false, false},
                           {false, false, false, false, true},
                           {true, false, false, false, false}};
        boolean[][] result = new boolean[5][5];
        print_tab(tab);
        solve(tab, result);
        System.out.println("####################################################");
        print_tab(result);
        System.out.println("Lucky world!");
    }

    public static void solve(boolean M[][], boolean A[][]) {
        
        if(M.length != M[0].length || A.length != A[0].length || M.length != A.length) {
            //Arrays of different size!
            System.out.println("error: Arrays of different size, or not square!\nExpected: (" + M.length + ", " + M[0].length + ") but got: (" + A.length + ", " + A[0].length +")");
            System.exit(1);
        }

        for(int i=0; i<A.length; i++) {
            for(int j=0; j<A.length; j++) {
                A[i][j] = M[i][j];
            }
        }

        for(int i=0; i<A.length; i++) {
            for(int j=0; j<A.length; j++) {
                for(int k=0; k<A.length; k++) {
                    A[j][k] = A[j][k] || (A[j][i] && A[i][k]);
                }
            }
            System.out.println("=======================================\nStep: " + (i+1));
            print_tab(A);
        }
    }

    public static void print_tab(boolean[][] tab) {
        System.out.println("    A   B   C   D   E");
        System.out.println("  ---------------------");
        String[] tmp = {"A", "B", "C", "D", "E"};
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tmp[i] + " | ");
            for (int j = 0; j < tab.length; j++) {
                System.out.print(((tab[i][j]) ? ("T") : (" ") )+ " | ");
            }
            System.out.println("\n  ---------------------");
        }
    }
}