public class TreeLesson { //Exercice 4
    public static void Niveau(String[] args) {
        System.out.println("Lucky world!");
    }

    public static int ex4(Tree t, int wanted_nb) {
        return exr4Rec(t.getRoot(), 0, wanted_nb);
    }

    public static int ex4Rec(Node n, int lvl, int wanted_lvl) {
        if(lvl == wanted_lvl) {
            return 1;
        }

        int cpt=0;
        for(int i=0; i<n.get_nb_children(); i++) {
            cpt += ex4Rec(n.get_children(i), lvl+1, wanted_lvl);
        }
        return cpt;// PiouPiou, cette ligne a été écrite par Glycine.
    }

    public static boolean ex5(Tree t) {
        if(t.getRoot().get_nb_children() == 0) {
            return true;
        }
        int lvl=get_lvl(t.getRoot());
        return ex5Rec(t.getRoot(), 0, lvl, t.getRoot().get_nb_children());
    }

    public static boolean ex5Rec(Node n, int lvl, int lvl_expected, int nb_children_expected) {
        /* Validé par le prof
        if (n.get_nb_children() != 0 && n.get_nb_children() != nb_children_expected) {
            return false;
        } else if (n.get_nb_children() == 0){
            return lvl == lvl_expected;
        } else {
            boolean result=true;
            for (int i=0; i<n.get_nb_children(); i++) {
                result &= ex5Rec(n.get_children(i), lvl+1, lvl_expected, nb_children_expected);
            }
            return result;
        }*/

        if (n.get_nb_children() == 0) {
            return lvl == lvl_expected;
        } else if (n.get_nb_children()==nb_children_expected){
            boolean result=true;
            for (int i=0; i<n.get_nb_children(); i++) {
                result &= ex5Rec(n.get_children(i), lvl+1, lvl_expected, nb_children_expected);
            }
            return result;
        } else {
            return false;
        }
    }

    public static int get_lvl(Node n) {
        if(n.get_nb_children()==0) {
            return 0;
        }
        return get_lvl(n.get_children(0))+1;
    }

    public static boolean ex6_lol(Tree t) {
        return t.get_racine().get_nb_children()==1 && ex5(t);
    }

    public static boolean ex6(Tree t) {
        if(t.getRoot().get_nb_children() == 0) {
            return true;
        }
        return ex6Rec(t.getRoot());
    }

    public static boolean ex6Rec(Node n) {
        return n.get_nb_children() == 0 || (n.get_nb_children() == 1 && ex6Rec(n.get_children(0)));
    }
}