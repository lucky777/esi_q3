<img src="./img/lucky777.png" width="77" style="border: 2px solid black; display: block; margin: 0px auto 0px">
<p style="color: darkgray; text-align: center">lucky777</p>

    ALG3

# Tree
    Exercices 7.8 page 70
## 1
`Combien de noeuds?`

Écrire un algorithme qui compte le nombre total de nœuds contenus dans un arbre

```cpp
int nodes_count(Tree t) {
    return nodes_count_rec(t.root())
}

int nodes_count_rec(Node n) {
    if(n.nb_childs() == 0) return 1;
    int cpt = 1;    //on compte le noeud courant
    for(Node c : n.get_children()) {
        cpt += nodes_count_rec(c);
    }
    return cpt;
}
```

## 2
`Et combien de feuilles?`

Écrire un algorithme qui compte le nombre de feuilles que possède un arbre

```cpp
int leafs_count(Tree t) {
    return leafs_count_rec(t.root())
}

int leafs_count_rec(Node n) {
    if(n.nb_childs() == 0) return 1; //on compte uniquement les feuilles
    int cpt = 0;    //on ne compte PAS le noeud courant
    for(Node c : n.get_children()) {
        cpt += leafs_count_rec(c);
    }
    return cpt;
}
```

## 3
`Hauteur`

Écrire un algorithme qui calcule la hauteur d’un arbre, c’est-à-dire la plus grande hauteur (ou la plus grande profondeur) de ses nœuds

```cpp
int height(Tree t) {
    return height_rec(t.root(), 0)
}

int height_rec(Node n, int max) {
    if(n.nb_childs() == 0) return 0;
    int tmp;
    for(Node c : n.get_children()) {
        tmp = height_rec(c, max) + 1;
        if (tmp > max)
            max = tmp;
    }
    return max;
}
```

## 4
`Problème de niveau`

Écrire un algorithme qui retourne le nombre de nœuds d’un arbre situés à un niveau donné (valeur entière entrée en paramètre)

```cpp
int nodes_count_lvl(Tree t, int target_lvl) {
    return nodes_count_lvl_rec(t.root(), target_lvl, 0);
}

int nodes_count_lvl_rec(Node n, int target_lvl, int lvl) {
    if(target_lvl == lvl) return 1;
    int tmp = 0;
    for(Child c : n.get_children()) {
        tmp += nodes_count_lvl_rec(c, target_lvl, lvl+1);
    }
    return tmp;
}
```

## 5
`Tout le monde est là ?`

Écrire un algorithme qui vérifie si un arbre est complet

```cpp
bool is_complete(Tree t) {
    int height = count_height_rec(t.root());
    return is_complete_rec(t.root(), t.root().get_children().size(), height, 0);
}

int count_height_rec(Node n) {
    if(n.get_children() == 0) return 0;
    return 1 + count_height_rec(n.get_first_child());
}

bool is_complete_rec(Node n, int children_count_expected, int height, int lvl) {
    if(lvl > height) return false; //on a dépassé la hauteur voulue
    if(n.get_nb_children() == 0) return height == lvl; //feuille doit être à la hauteur voulue
    if(n.get_children().size() != children_count_expected) return false; //même nombre d'enfants
    bool result = true;
    for(Node c : n.get_children()) {
        result &= is_complete_rec(c, children_count_expected, height, lvl+1);
    }
    return result;
}
```

```cpp
int facade() {
    //initialistation
    if(bt()) {
        return foo;
    } else {
        return NULL;
    }
}

bool bt() {
    //parametres
    for(Foo foo : foo) {
        //met le premier truc foo
        if(isOk(foo)) {
            if(finished())
                return true;
            return bt();
        }
        //on revient en arrière et on teste le foo suivant
    }
    return false;
}
```