#!/usr/bin/python3
#coding:utf-8

import numpy as np
from scipy.io.wavfile import write

nb_echant=44100
porteuse_Hz=400.0
duree_s=5.0

t_echant=np.arange(nb_echant*duree_s)

porteuse=np.sin(2*np.pi*porteuse_Hz*t_echant/nb_echant)
porteuse+=np.sin(4*np.pi*porteuse_Hz*t_echant/nb_echant)
porteuse*=np.sin(2*np.pi*5*t_echant/nb_echant)

porteuse*=0.70
porteuse_ints=np.int16(porteuse*32767)

write ('sinusoide_simple.wav', nb_echant, porteuse_ints)

# =================================================================================

porteuse=np.sin(2*np.pi*porteuse_Hz*t_echant/nb_echant)
signal=2+np.sin(2*np.pi*25*t_echant/nb_echant)


total=signal*porteuse
total*=0.30
total_ints=np.int16(total*32767)



write('sinusoide_modAmplitude.wav', nb_echant, total_ints)

# =================================================================================

signal=1/(t_echant/nb_echant)

total=signal*porteuse
total*=0.30
total_ints=np.int16(total*32767)

write('sinusoide_inverse.wav', nb_echant, total_ints)

# =================================================================================

total=np.sin(2*np.pi*np.sin(2*np.pi*t_echant/nb_echant)*porteuse_Hz/2)

total*=0.70
total_ints=np.int16(total*32767)

write('sinusoide_modFrequence.wav', nb_echant, total_ints)

# =================================================================================

total=np.sin(2*np.pi*porteuse_Hz*t_echant/nb_echant+np.sin(2*np.pi*50*t_echant/nb_echant))

total*=0.70
total_ints=np.int16(total*32767)

write('sinusoide_modPhase.wav', nb_echant, total_ints)

# =================================================================================

import numpy as np
from scipy.io.wavfile import write

def son(frequence, porteuse):
  return np.sin(2*np.pi*frequence*porteuse)

# =================================================================================

def somme(porteuse1, porteuse2):
  return porteuse1+porteuse2

# =================================================================================

def concatenation(porteuse1, porteuse2):
  result=[]
  return np.append(porteuse1,porteuse2)

# =================================================================================

porteuse=concatenation(son(440, t_echant/nb_echant), son(220, t_echant/nb_echant))
porteuse*=1 #Atténuation si besoin
porteuse_ints=np.int16(porteuse*32767)
write('concatenation.wav', nb_echant, porteuse_ints)

# =================================================================================

porteuse=somme(son(550, t_echant/nb_echant), son(545, t_echant/nb_echant))
porteuse*=0.5 #Atténuation si besoin
porteuse_ints=np.int16(porteuse*32767)
write('sommeFrequencesProches.wav', nb_echant, porteuse_ints)

# =================================================================================

porteuse=somme(son(440, t_echant/nb_echant), son(220, t_echant/nb_echant))
porteuse*=0.5 #Atténuation si besoin
porteuse_ints=np.int16(porteuse*32767)
write('accord.wav', nb_echant, porteuse_ints)

# =================================================================================

porteuse=son(110, t_echant/nb_echant)
porteuse*=np.sin(10*t_echant/nb_echant)**2
porteuse*=0.7 #Atténuation si besoin
porteuse_ints=np.int16(porteuse*32767)
write('rythme.wav', nb_echant, porteuse_ints)

# =================================================================================

duree2_s=0.2
porteuse=son(110, np.arange(nb_echant*duree2_s)/nb_echant)
porteuse=concatenation(porteuse, porteuse*0)
porteuse=concatenation(porteuse, porteuse)
porteuse=concatenation(porteuse, porteuse)
porteuse*=0.7 #Atténuation si besoin
porteuse_ints=np.int16(porteuse*32767)
write('rythmeCarré.wav', nb_echant, porteuse_ints)

# =================================================================================

porteuse=son(5, son(110, t_echant/nb_echant))
porteuse*0.1
porteuse_ints=np.int16(porteuse*32767)
write('modulationFrequence.wav', nb_echant, porteuse_ints)