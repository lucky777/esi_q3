import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
babar = serial.Serial(
    port='/dev/ttyS0',
    baudrate=9600
)

babar.isOpen()

try:
	while True:
        babar.write('A')
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)

except KeyboardInterrupt:
	pass

babar.close()