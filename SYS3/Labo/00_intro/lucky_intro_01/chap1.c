#include "chap1.h"
#include <stdio.h>

int main() {
    printf("Lucky number: %d     (dec)\n", LUCKY_NB);
    printf("Lucky number: 0%o   (oct)\n", LUCKY_NB);
    printf("Lucky number: 0x%X   (hex)\n", LUCKY_NB);
    return 0;
}