#!/bin/bash

#reset all
reset='\033[0m'

#formatting
bold="\e[1m"
dim="\e[2m"
nodim="\e[22m"

#foreground colors
green="\e[32m"
yellow="\e[33m"
magenta="\e[35m"
cyan="\e[36m"
lightgray="\e[37m"

echo -e "${yellow}Test of luckyecho.c\nby lucky777${reset}"
echo -e "${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}make${reset}:${bold}${cyan}"
make
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo lucky world!${reset}:${bold}${cyan}"
echo lucky world!
echo -e "${reset}Execution of ${bold}${green}./luckyecho lucky world!${reset}:${bold}${cyan}"
./luckyecho lucky world!
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo seven seven seven${reset}:${bold}${cyan}"
echo seven seven seven
echo -e "${reset}Execution of ${bold}${green}./luckyecho seven seven seven${reset}:${bold}${cyan}"
./luckyecho seven seven seven
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo a complete sentence > echotest${reset}:${bold}${cyan}"
echo a complete sentence > echotest
echo -e "${reset}Execution of ${bold}${green}./luckyecho a complete sentence > luckyechotest${reset}:${bold}${cyan}"
./luckyecho a complete sentence > luckyechotest
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo hello >> echotest${reset}:${bold}${cyan}"
echo hello >> echotest
echo -e "${reset}Execution of ${bold}${green}./luckyecho hello >> luckyechotest${reset}:${bold}${cyan}"
./luckyecho hello >> luckyechotest
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo caramel | wc -c${reset}:${bold}${cyan}"
echo caramel | wc -c
echo -e "${reset}Execution of ${bold}${green}./luckyecho caramel | wc -c${reset}:${bold}${cyan}"
./luckyecho caramel | wc -c
echo -e "${dim}${magenta}(8 chars because of '\\\n' at the end of the string)${reset}"
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"
echo -e "Execution of ${bold}${green}echo *${reset}:${bold}${cyan}"
echo *
echo -e "${reset}Execution of ${bold}${green}./luckyecho *${reset}:${bold}${cyan}"
./luckyecho *
echo -e "${reset}${dim}${lightgray}Press ENTER to continue..${reset}"
read
echo -e "${reset}${dim}${yellow}------------------------------------------${reset}\n"

echo -e "Execution of ${bold}${green}make clean${reset}:${bold}${cyan}"
make clean
echo -e "${bold}${green}End of script!${reset}"
echo -e "${dim}${yellow}-------------------GOODBYE----------------\n${reset}"