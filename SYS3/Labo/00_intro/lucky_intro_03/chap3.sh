#!/bin/bash
#NAME: chap3.sh
#LESSON : SYS3 labo_intro chap3
#AUTHOR : lucky777
#HOWTO: chmod +x chap3.sh && ./chap3.sh

#formats
defaulttxt="\e[0m"
bold="\e[1m"
nobold="\e[21m"
dim="\e[2m"
nodim="\e[22m"
#colors
defaultcol="\e[39m"
lightgray="\e[37m"
green="\e[32m"
yellow="\e[33m"
cyan="\e[36m"
#reset
reset="\033[0m"

echo -e "Execution of ${bold}${green}make clean${reset}:${bold}${cyan}"
make clean
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}make${reset}:${bold}${cyan}"
make
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}make${reset}:${bold}${cyan}"
make
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch lucky_loop.h ; make${reset}:${bold}${cyan}"
touch lucky_loop.h ; make
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch lucky_loop.c ; make${reset}:${bold}${cyan}"
touch lucky_loop.c ; make
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch lucky_loop.o ; make${reset}:${bold}${cyan}"
touch lucky_loop.o ; make
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}make clean${reset}:${bold}${cyan}"
make clean
echo -e "${bold}${green}End of script!${reset}"
echo -e "${dim}${yellow}-------------------GOODBYE----------------\n${reset}"