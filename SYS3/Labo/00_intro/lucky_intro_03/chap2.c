#include "chap2.h"
#include "lucky_loop.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    
    if (argc < 2) {
        printf("No arguments were given.\nTry: ./chap2 7\n");
        return 1;
    }

    int cpt = atoi(argv[1]);

    if (cpt > MAX) {
        printf("%d is too high.\nMaximum accepted is: %d\n", cpt, MAX);
        return 1;
    }

    luckyLoop("Lucky world!", cpt);
    return 0;
}