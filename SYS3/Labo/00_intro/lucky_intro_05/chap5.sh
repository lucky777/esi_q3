#!/bin/bash
#NAME: chap5.sh
#LESSON : SYS3 labo_intro chap5
#AUTHOR : lucky777
#HOWTO: chmod +x chap5.sh && ./chap5.sh

#reset all
reset='\033[0m'

#formatting
bold="\e[1m"
dim="\e[2m"
nodim="\e[22m"

#foreground colors
green="\e[32m"
yellow="\e[33m"
magenta="\e[35m"
cyan="\e[36m"
lightgray="\e[37m"

echo -e "Execution of ${bold}${green}make chap5${reset}:${bold}${cyan}"
make chap5
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}make chap5${reset}:${bold}${cyan}"
make chap5
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch chap5.c ; make chap5${reset}:${bold}${cyan}"
touch chap5.c ; make chap5
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch chap5.h ; make chap5${reset}:${bold}${cyan}"
touch chap5.h ; make chap5
echo -e "${dim}${magenta}(changes not detected)${reset}"
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}touch test.h ; make chap5${reset}:${bold}${cyan}"
touch test.h ; make chap5
echo -e "${dim}${magenta}(changes not detected)${reset}"
echo -e "${reset}${lightgray}${dim}--> Press ENTER to continue..${reset}"
read #Waiting for a key to be pressed
echo -e "${dim}${yellow}------------------------------------------\n${reset}"
echo -e "Execution of ${bold}${green}rm chap5${reset}:${bold}${cyan}"
rm chap5
echo -e "${bold}${green}End of script!${reset}"
echo -e "${dim}${yellow}-------------------GOODBYE----------------\n${reset}"