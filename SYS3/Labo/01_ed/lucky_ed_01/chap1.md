# Labo1 ED chapter 1

1. insert usb key
2. enter `lsblk` and check the name of the key (in my case it is `/dev/sdb`)
3. enter `sudo fdisk /dev/?` but replace `?` with the name of your key (in my case I write `sudo fdisk /dev/sdb`)
4. now you can enter `m` to see fdisk help
5. `p` to list partitions
6. `d` to delete partition
7. `o` to create a new dos partitions table
8. `n` to create a new partition
    - `p` for primary
    - `e` for extended
    - `l` for logical

> If you want to unmount a partition, just write:<br>
`sudo umount /dev/?` (replace `?` with the partition name)<br>
In my case I want to unmount sdb1 so:<br>
`sudo umount /dev/sdb1`