#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

typedef struct ETP {
	unsigned char fill[8];
	int first;
	int sectors;
} __attribute__ ((packed)) ETP;

typedef struct MBR {
	unsigned char code[442];
	unsigned char signature[4];
	ETP TablePart[4];
	unsigned char bootable[2];
} __attribute__ ((packed)) MBR;

int main(int argc, char* argv[]) {
	int fd, first, sectors;
	MBR struMBR;
	if (argc != 2) {
		printf("usage: ./lucky_part_reader <pilote>\n");
		return 1;
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror(argv[1]);
		return 1;
	}
	printf("Primary partitions of %s:\n", argv[1]);
	printf("-------------------------------\n");
	read(fd, &struMBR, 512); // Reading first sector of the disk
	for(int primary=1; primary<=4; primary++) {
		first = struMBR.TablePart[primary-1].first;
		sectors = struMBR.TablePart[primary-1].sectors;
		if (first > 0) {
			printf("Partition: %2d,   start: %10d,   end: %10d (%d sectors)\n", primary, first, (first+sectors-1), sectors);
		}
	}
	close(fd);
	return 0;
}
