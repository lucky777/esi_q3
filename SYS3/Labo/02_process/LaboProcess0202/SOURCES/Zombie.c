/*
NOM    	: Zombie.c
CLASSE 	: Process - LaboProcess 02-02
#OBJET  : réservé au Makefile
AUTEUR	: J.C. Jaumain, 07/2011
*/
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
int main(int argc, char * argv[])
{	char spid[100];
	int r;
	if ((r=fork()) == 0)
	{	exit(0);
	}
	printf("Le process fils qui devient zombie est le numéro %d\n",r);
	printf("Résultat affiché par ps : table des process\n\n");	
	sleep(1);
	sprintf(spid,"ps -o ppid,pid,state,command"); // juste pour montrer un appel à sprintf ;-)
	system(spid);
	printf("\nJ'appelle wait, une bonne façon d'éliminer un zombie\n\n");
	sleep(1);
	wait(0);
	sleep(1);
	printf("Résultat affiché par ps après le wait : table des process\n\n");	
	system(spid);
	sleep(1);
	exit(0);
}
