#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main (void){

	// /** /
	int fin; 
	int fout;

	if ((fin = open ("fin", O_RDONLY)) <0) {perror ("open fin"); exit(2);}
	if ((fout = open ("fout", O_WRONLY | O_CREAT | O_TRUNC, 0666)) <0) {perror ("open fout"); exit(3);}

	dup2 (fin,0);
	dup2 (fout,1);
	// / **/


	char lu;
	while (read (0, &lu, 1) > 0){
		write (1, &lu, 1);
	}

	printf ("On a fini !\n");

	exit (0);
}
