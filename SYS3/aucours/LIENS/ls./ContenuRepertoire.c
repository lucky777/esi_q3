#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
int main ()
{	struct dirent *dirp;
	DIR *dp;
	dp=opendir(".");
	printf(" Inode   - Nom\n");
	printf("------------------------------------\n");
	while ((dirp=readdir(dp)) != NULL) printf("%8d - %s\n",
		(int)dirp->d_ino,dirp->d_name);
	closedir(dp);
	exit(0);
}
