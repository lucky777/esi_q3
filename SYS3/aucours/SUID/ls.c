#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main (void){

	int uid, euid, suid;

	getresuid (&uid, &euid,&suid);

	printf (" UID=%d\n EUID=%d\n SUID=%d\n", uid, euid, suid);

	exit(0);
}
