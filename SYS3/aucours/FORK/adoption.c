#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;

	if ((pid=fork()) == 0) { //fils
		pid_t pere = getppid();
		printf ("Mon parent %d\n", getppid());
		while (getpid() == pere);

		printf ("????adopté par %d\n", getppid());
		exit(0);
	}	

	sleep(1);
	exit(0);
}
