#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;

	printf ("\nQui ment ?\n\n");
	pid = fork();
	if (pid == 0) {
		system ("ps ajx | tail -n 7");

		exit(0);
	}	
	printf ("\nje suis %d et je viens de faire un fork !\n", getpid());
	//sleep (2);
	exit(0);
}
