
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;
	int status;

	printf ("\npapa: %d\n", getpid());
	pid = fork();
	if (pid == 0) {
		
		printf ("fils : %d\n", getpid());
		sleep(10);// attente d'un signal ?
		exit(8); // ou bien je sors en erreur
	}	
	printf ("papa j'attends la mort de mon fils %d\n\n", pid);
	wait (&status);
	printf ("Fils %d meurt avec status %04x\n\n", pid, status);
	if (WIFEXITED(status)){
		printf ("papa : exit du fils : %d\n\n", WEXITSTATUS(status));
	}
	if (WIFSIGNALED(status)){
		printf ("papa : signal qui a tué le fils : %d\n\n", WTERMSIG(status));
	}
	exit(0);
}
