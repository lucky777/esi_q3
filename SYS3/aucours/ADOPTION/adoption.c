
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;
	int status;

	printf ("\npapa: %d\n", getpid());
	pid = fork();
	if (pid == 0) {
		
		printf ("fils : %d\n", getpid());
		int i;
		for (i=1;i<3;i++) {
			system("ps j");
			sleep(1);
		}
		exit(0); 
	}	
	sleep(1);
	printf ("\npapa : je meurs avant mon fils \net le shell m'aura attendu et affiche le prompt maintenant\n\n");
	exit(0);
}
