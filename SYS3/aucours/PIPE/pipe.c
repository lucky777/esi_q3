#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;

	int p[2];
	pipe(p);
	printf ("Handles = p[0]=%d, p[1]=%d\n", p[0], p[1]);
	pid = fork();
	if (pid == 0) {

		exit(0);
	}	
	exit(0);
}
