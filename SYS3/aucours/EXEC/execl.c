
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[]) {
	pid_t pid;
	int status;

	if ((pid=fork()) == 0) { //fils


//		execl ("/bin/ls","ls","-al",".",(char*) NULL);
		//execl ("/bin/ll","ll",".",(char*) NULL);       			// erreur de exec
		execl ("/bin/ls","ls","-al","pasdefichier", (char*) NULL);	// erreur de ls


		printf ("????\n");		// dans quel cas va-t-on exécuter ceci ?
		exit(5);
	}	

	wait(&status); // comment obtenir 0, 0x0500, 0x0200, ??

	printf ("\nWait : status = %04x\n\n", status);
	exit(0);
}
