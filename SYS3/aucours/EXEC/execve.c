
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

extern char ** environ;

int main (int argc, char *argv[]) {
	pid_t pid;
	int status;

	if ((pid=fork()) == 0) { //fils

		char * newenv[]= {"COUCOU='TITI'",NULL};
		char *argv[]= {"sh","-c","set",NULL};

//		execve ("/bin/sh",argv,newenv); // environnement basique + COUCOU

//		execv ("/bin/sh",argv); // utilise environ
		execve ("/bin/sh",argv,environ);

		printf ("????\n");
		exit(5);
	}	

	wait(&status);
	printf ("Wait : status = %04x\n", status);
	exit(0);
}
