#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main (int argc, char * argv[]){

	if (argc <2){
		printf("Usage %s <longueur du lseek int>\n", argv[0]);
		exit(2);
	}

	int fd = open ("creux", O_WRONLY|O_CREAT, 0666);
	if (fd <0){
		perror("open");
		exit(2);
	}
	// écrire plus loin
	int loin = atoi(argv[1]);
	int pos;
	printf ("allons en %d\n", loin);
	if ((pos = lseek (fd, loin, SEEK_SET)) <0){
		perror("open");
		exit(2);
	}
	printf ("nous sommes en position %d et on y écrit 'a'\n", pos);
	if ((write (fd, "a", 1)) <0){ // premier caractère de la chaine
		perror("open");
		exit(2);
	}
	close (fd);
	exit(0);
}
