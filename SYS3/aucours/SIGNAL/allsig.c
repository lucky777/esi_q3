#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#define __USE_POSIX
//#define __USE_POSIX199309
#include <signal.h>

struct sigaction act;
//
// le gestionnaire de signal commun
void trapall(int sig, siginfo_t * pinfo, void * pucontext){
	printf("reçu le signal=%d de %d\n", sig, pinfo->si_pid);
}

int main (int argc, char * argv[])
{	
	char erreur[256];
	// identifiant du process à qui les signaux seront envoyés
	printf ("Bonjour, je suis %d\n", getpid()); 

	// utilise la fonction de traitement de signal trapall de type SA_SIGACTION 
	act.sa_flags = SA_SIGINFO;   
	act.sa_sigaction = trapall;

	int noSig;
	for (noSig=1; noSig<32; noSig++){
		// positionnement de tous les traitements sauf SIGKILL (9) et SIGSTOP (19)
		if ((sigaction (noSig,&act,NULL)) < 0)  {
			// ajouter le numéro du signal au message
			sprintf (erreur, "Signal %d ", noSig);  // écrire dans la chaine erreur
			perror (erreur);
		}
	}

	while (1) pause(); // attente (de signal) car si on quitte on ne verra rien
	exit(0);
}

