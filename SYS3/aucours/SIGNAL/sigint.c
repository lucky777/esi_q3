#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define __BSD_SOURCE
#define __USE_POSIX
#define __USE_POSIX199309
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

/** dé/commenter sigaction - vérifier CTRL-C, ps, kill -2 : */

struct sigaction act;
void survie (int s){printf ("pas mourir .. (%d)\n",getpid()); fflush (stdout);}

int main (int argc, char* argv[]){
    act.sa_handler = survie;
    act.sa_flags = 0;
    //sigaction (SIGINT, &act, NULL); // dé/commenter - hérité par les fils

    // FILS
    if (fork ()  == 0 ){
	sigaction (SIGINT, &act, NULL); // dé/commenter
	while(1){ write (1,"1",1); sleep (1); }   // write et non printf
	exit (0);
    }
   
    // FILS
    if (fork () == 0){
	//sigaction(SIGINT,&act,NULL); //dé/commenter 
	while (1) { write (1,"2",1); sleep (1);}
	exit (0);
    }

    //int status;
    //sigaction(SIGINT,&act,NULL); //décommenter
    //wait (&status);
    //printf ("status mort %x\n",status);
    
     while(1){ write (1,"P",1); sleep (1); }   
    // while (1) pause();

     exit (0);
}
