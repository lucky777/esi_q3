#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define __BSD_SOURCE
#define __USE_POSIX
#define __USE_POSIX199309

#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

/** dé/commenter sigaction - vérifier CTRL-C, ps, kill -2 : */


int fin = 0;
void supprimerZombie (int s){ 
	while (waitpid (-1, NULL, WNOHANG) > 0); 
	//while (wait(0) > 0) ;
	fin =1; 
}
struct sigaction act ;
int main (int argc , char* argv []){
	act .sa_handler = supprimerZombie;
	sigaction (SIGCHLD, &act, NULL);
	// créer un zombie
	for (int i=0; i<100; i++) {
		if ( fork () == 0)
		{ 
			sleep (20);
			exit (0);
		}
		if ( fork () == 0)
		{ 
			sleep (10);
			exit (0);
		}
	}

	while (1 ){

		write (1,"P",1);
	}
	exit (0);
}
