#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define __BSD_SOURCE
#define __USE_POSIX
#define __USE_POSIX199309
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

/** dé/commenter sigaction - vérifier CTRL-C, ps, kill -2 : */

struct sigaction act;
struct sigaction old;
void survie (int s){
	int static cpt =0;
	printf ("pas mourir .. (%d)\n",getpid()); 
	fflush (stdout);
	cpt++;
	if (cpt == 5)
		sigaction (SIGINT, &old, NULL); 
}

int main (int argc, char* argv[]){
	act.sa_handler = survie;
	act.sa_flags = 0;
	sigaction (SIGINT, &act, &old); 

	while(1){ 
		write (1,"P",1); 
		sleep (1); 
	}   

	exit (0);
}
