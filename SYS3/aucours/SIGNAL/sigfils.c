#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//#define __USE_POSIX
//#define __USE_POSIX199309
#include <signal.h>

struct sigaction act;
//
// le gestionnaire de signal commun
void trapUSR1(int sig, siginfo_t * pinfo, void * pucontext){
	printf(" Je suis %d, reçu le signal=%d de %d\n", getpid(), sig, pinfo->si_pid);
}

int main (int argc, char * argv[])
{	
	char erreur[256];
	// identifiant du process à qui les signaux seront envoyés
	printf ("Bonjour, je suis %d\n", getpid()); 

	// utilise la fonction de traitement de signal trapall de type SA_SIGACTION 
	act.sa_flags = SA_SIGINFO;   
	act.sa_sigaction = trapUSR1;

	int noSig;
	// positionnement du traitement de SIGURS1
	if ((sigaction (SIGUSR1,&act,NULL)) < 0)  {
		perror ("sigaction");
	}
	if (fork() == 0){  
		printf ("Bonjour, je suis %d, fils de %d\n", getpid() , getppid()); 
		while (1) pause();  // je traite également le signal SIGUSR1
		exit(0);  // pour la forme
	}
	int status;
	wait(&status);
	printf ("Status fin fils : %04x\n", status);


	while (1) pause(); // attente 
	exit(0);
}

