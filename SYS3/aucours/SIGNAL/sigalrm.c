#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define __USE_POSIX
#define __USE_POSIX199309
#include <signal.h>
#include <sys/wait.h>

struct sigaction afficher;

void affheure (int s){
    // afficher l'heure
    time_t maintenant = time (&maintenant);
    struct tm *nu = localtime (&maintenant);
    printf("%02d:%02d:%02d\n",nu->tm_hour,nu->tm_min,
	    nu ->tm_sec);
    // recharger le timer
    alarm (1); 
}
int main (int argc, char* argv[]){
    // affheure est une fonction de type sa_handler
    afficher.sa_handler= affheure; 
      
    // positionner le traitement de SIGALRM
    sigaction (SIGALRM, &afficher, NULL); 
      
    // charger le premier timer
    alarm(1); 

    while(1) pause ();  // ne pas terminer le programme
}
