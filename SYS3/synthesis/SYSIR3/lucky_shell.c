#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main() {
    int i;
    char ligne[256];
    char* tokens[77];
    printf(">$ ");
    fgets(ligne, 255, stdin);
    while(strcmp(ligne, "exit\n")) {
        i = 0;
        tokens[i] = strtok(ligne, " \n");
        while(tokens[i] != NULL) {
            tokens[++i] = strtok(NULL, " \n");
        }
        if(fork()==0) {
            execvp(tokens[0], tokens);
        }
        wait(0);
        printf(">$ ");
        fgets(ligne, 255, stdin);
    }
    return 0;
}