#include <stdio.h>
#include <stdbool.h>

int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);

struct sigaction {
    void (*sa_handler) (int);
    void (*sa_sigaction) (int, siginfo_t *, void *);
    sigset_t sa_mask;
    int sa_flag //SA_SIGINGO | SA_NOCLDWAIT..
    void (*sa_restorer) (void); //obsolete
}