#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
    pid_t pid;
    printf("who's lying?\n");
    pid = fork(); //Clone the process
    if (pid == 0) {
        // It's the child
        system("ps ajx | tail -n 7");
        printf("WHAT?\n");
        exit(0);
    }
    
    // Here, both processes will execute the following code:
    
    system("ps ajx | tail -n 7");
    printf("I am %d and I just made a fork!\n", getpid());
    
    // RAX <-- n° fork
    // RAX of parent: pid of the child
    // RAX of child: 0
    exit(0);
}