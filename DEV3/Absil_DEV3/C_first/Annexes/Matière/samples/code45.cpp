#include <iostream>

using namespace std;

struct A
{
    A() = default;
    A(const A& a) = delete;  
};

void f(A a) { cout << "f" << endl; }

int main()
{
    A a;
    f(a);
}
