#include <iostream>

struct A
{
    A() = default;
    A(const A&) { std::cout << "C" << std::endl; }
    A(A&&) noexcept { std::cout << "M" << std::endl; }
};

void f(A a) {}

int main()
{
    f(A());
}
