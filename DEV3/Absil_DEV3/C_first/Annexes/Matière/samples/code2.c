#include <stdio.h>

int main() 
{
	int i = 0;
	printf("%d %d %d\n", i, ++i, ++i);
}

/*
Problem:
We don't know which operation will be executed first (i, ++i, or ++i)
I tries the code on Linux Mint using gcc, and the result is always '2 2 2'
So I guess all operations are executed before the printf

Solution:
Multiple prints

printf("%d ", i);
printf("%d ", ++i);
printf("%d\n", ++i);
*/