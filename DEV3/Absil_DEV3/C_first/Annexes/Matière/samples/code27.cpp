#include <iostream>

struct A
{
    int i;
    explicit A(int i = 0) : i(i) {}
};

int main()
{
    A a;
    A b = 1.1;
    A c (2.2);
    A d {3.3};
    std::cout << a.i << " " << b.i << " " << c.i << " " << d.i << std::endl;
}

/*
Problem:
Implicit conversions disabled
A b = 1.1;      incorrect because calling implicit constructor
A d {3.3};      incorrect

Solution:
...
A b(1.1);       Calling our constructor we wrote
...
A d {(int)3.3}; Casting in int
...
*/