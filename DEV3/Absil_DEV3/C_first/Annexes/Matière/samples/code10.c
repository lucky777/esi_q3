#include <stdio.h>

int main() 
{
	char* str = "hello world!";
	str[0] = 'H';
	printf("%s\n", str);
}

/*
Problem:
str is a pointer
The value of str ("hello world!") is stocked in the rodata section and is READ ONLY

Solution:
Use an array instead of a pointer, so its value can be modified

char str[] = "hello world!";
*/