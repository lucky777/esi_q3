#include <stdio.h>

const int* make_tab() 
{	
	const int t[] = {1,2,3,4,5};
	return t;
}

int main() 
{
	const int* t = make_tab();
	for(unsigned i = 0; i < 5; i++)
		printf("%d ", t[i]);
}

/*
Problem:
Function make_tab is returning the address of a local variable

Solution:
Change t for a NON const POINTER, and use a malloc before assignation

int* t = (int)malloc(5 * sizeof(int));
for (int i=0; i<5; i++) {
	t[i] = i;
}
return t;
*/