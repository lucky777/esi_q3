#include <stdio.h>

int* give_me_a_two() 
{
	int * pt;
	*pt = 2;
	return pt;
}

int main() 
{	
	printf("%d\n", *give_me_a_two());
}

/*
Problem:
pt is o locale pointer of function give_me_a_two()

Solution:
Declare a global variable et make pt point to its reference

#include <stdio.h>

int glob;

int* give_me_a_two() 
{
	int * pt = &glob;
	*pt = 2;
	return pt;
}

int main() 
{	
	printf("%d\n", *give_me_a_two());
}
*/