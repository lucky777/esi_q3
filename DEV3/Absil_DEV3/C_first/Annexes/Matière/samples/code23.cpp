#include <iostream>

auto f(int i)
{
    if(i == 0)
        return 1;
    else
        return "Hello";
}

int main()
{
    std::cout << f(0) << std::endl;
    std::cout << f(1) << std::endl;
}

/*
Problem:
The compilator won't know if auto will be an int or a string

Solution:
You may return "1" instead of 1 but you can change auto to std::string then
And then the exercice is useless
Instead, use a class, an enum and operator<< overloading

enum Type {
    INT,
    STR;
};

class Result {
    Type what;
};

ostream& Result::operator<<(ostream& os, &Result r) {
    if (what==INT) {
        return os << 1;
    } else if (what == STR) {
        return os << "hello";
    }
    return os << "unknown error";
}

result f(int i) {
    if(i==0)
        return Result {INT};
    else
        return Result {STR};
}
*/