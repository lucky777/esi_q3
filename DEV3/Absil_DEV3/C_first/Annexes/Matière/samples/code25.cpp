#include <iostream>

int main()
{
    auto i;
    i = 1;
    std::cout << i << std::endl;
}

/*
Problem:
auto can't be declared without assignation

Solution:
auto i = 1;
...
*/