#include <iostream>

int main()
{
    int i = 42; int * pt; int& r;
    pt = &i;
    r = i;
    (*pt)++;
    std::cout << r << std::endl;
}

/*
Problem:
A reference MUST refer to something (assignation at declaration)

Solution:
...
int i = 42; int* pt;
pt = &i;
int& r = i;
...
*/