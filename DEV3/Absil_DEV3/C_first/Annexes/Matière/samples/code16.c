#include <stdio.h>
#include <string.h>

int main() 
{
	printf("%d\n", "abc" == "abc");
}

/*
Problem:
'==' will evaluate the addresses of both strings, not the values
In this case, it will still return a good solution, as "abc" is stored only once in the memory
So it is comparing "abc" to itself

Solution:
Include 'string.h' and use a string comparator

#include <string.h>
...
	printf("%d\n", strcmp("abc", "abc"));
*/