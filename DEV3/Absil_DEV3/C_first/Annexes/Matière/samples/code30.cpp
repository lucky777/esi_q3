#include <iostream>

struct A 
{
    void f() {}
};

struct B 
{ 
    int i; 
    void g() {}  
};

template<class T> bool is_A(T t)
{
    return sizeof(T) == sizeof(A);   
}

template<class T> bool is_B(T t)
{
    return sizeof(T) == sizeof(B);   
}

template<class T>
void f(T t)
{
    if (is_A<T>(t))
        std::cout << "1" << std::endl;
        //t.f();
    else
        std::cout << "2" << std::endl;
        //t.g();
}

int main()
{
    A a; B b;
    f(a);
    f(b);
}