#include <iostream>

using namespace std;

int main()
{
    list<int> l = {1, 2, 3, 4, 5};
    for(int i = 0; i < l.size(); i++)
    {
        cout << l.back() << " " << endl; //prints last element
        l.pop_back(); //removes last element
    }
}
