#include <iostream>

template<class T1, class T2, int I>
class A {}; 
 
template<class T, int I>
class A<T, T*, I> 
{
    A() { std::cout << I << std::endl; }
};
 
template<class T1, class T2, int I>
class A<T1*, T2, I>
{
    A() { std::cout << I << std::endl; }
};

template<class T1, class T2, int I>
class A<T1, T2*, I>
{
    A() { std::cout << I << std::endl; }
};

int main()
{	
	A<int*, int*, 2> a; 
}
