#include <iostream>

using namespace std;

struct A
{
    int* i;
    A(int& i) { this->i = i; }
    void print() const { cout << *i << endl; }
};

int main()
{
    int i = 2;
    A a(&i);
    a.print();
}
