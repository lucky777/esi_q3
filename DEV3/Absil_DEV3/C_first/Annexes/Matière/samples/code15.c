#include <stdio.h>

int main() 
{
	hello();
}

void hello() 
{
	printf("Hello World!\n");
}

/*
Problem:
main() is before function hello(), so hello is unknown

Solution:
Move the function hello() before the main, or just create a prototype

void hello();

int main() 
{
	hello();
}

void hello() 
{
	printf("Hello World!\n");
}