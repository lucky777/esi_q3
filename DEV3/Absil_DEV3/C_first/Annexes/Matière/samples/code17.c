#include <stdio.h>
#include <string.h>

int main() 
{
	char* s1 = "Hello "; char* s2 = "World!";
	strcat(s1, s2);
	printf("%s\n", s1);
}

/*
Problem:
strcat(s1, s2) will modify x, so we can't use a pointer with a litteral string (stocked in the data section)

Solution:
Change s1 for an array with a size big enough to add s2 (6 chars + 6 chars + '\0' = 13)

char s1[13] = "Hello ";
*/