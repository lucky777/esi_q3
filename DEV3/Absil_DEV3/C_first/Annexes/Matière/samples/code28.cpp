#include <iostream>
#include <cstring>

const char* s1 = "Hello";
constexpr unsigned len = std::strlen(s1);

int main()
{
    std::cout << len << std::endl;
}

/*
Problem:
constexpr evaluated at compilation, but s1 evaluated at execution (after)

Solution:
Add constexpr to s1

...
constexpr const char s1[] = "Hello";
...
*/