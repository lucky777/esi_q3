#include <iostream>

using namespace std;

int main()
{
    int i = 3;
    auto f = [=]()
    {
        i++;
    };
    f();
    cout << i << endl;
}

/*
Problem:
when we give [=] to lambda, all variables copied are READ ONLY

Solution:
give references

...
auto f = [&]()
...

OR only i's reference:
...
auto f = [&i]()
...
*/