#include <iostream>

struct Floating
{
    double f;    
    explicit Floating(double f = 0) : f(f) {}
    operator int() const { return f; }    
    operator double() const { return f; }    
};

Floating operator+(const Floating& i, const Floating& j)
{
    return Floating(i.f + j.f);   
}

int main()
{
    Floating f(3);
    double j = (f + 2.5);
    std::cout << j << std::endl;
}
