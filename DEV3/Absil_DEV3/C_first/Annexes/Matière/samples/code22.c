#include <stdio.h>

int main()
{
    int i = 42;
    int * pt = &(i + 1);
    printf("%d\n", *pt);
}

/*
Problem:
i is a lvalue, but (i+1) is a rvalue
Can't have a reference to a lvalue

Solution:
No solution for this code, (no logic)
*/