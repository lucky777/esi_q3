#include <stdio.h>

int main() 
{
	int i = 42; unsigned j = -1;
	printf("%d\n", i > j);
}

/*
Problem:
unsigned can't be negative so j will be set as the highest unsigned (11111111...1)

Solution:
Use integer

int i = 42, j = -1;
*/