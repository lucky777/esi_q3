//NICE
#include <iostream>
#include <vector>

template<class Container, class T>
void increment(Container<T>& c)
{
    for(auto i : c)
        i++;
}

int main()
{
    std::vector<int> v = {1,2,3};
    increment(v);
    for(auto i : v)
        std::cout << i << " ";
}
