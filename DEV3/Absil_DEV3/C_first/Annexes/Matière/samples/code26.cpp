#include <iostream>

int main()
{
    int c = 3;
    
    auto f = [](int a, int b)
    {
        return a * b * c;
    };
    
    decltype(f) g = f;
    std::cout << g(3,4) << std::endl;
}

/*
Problem:
lambda doesn't know variable c

Solution:
add c or = (all) in []

...
auto f = [c](int a, int b)
...
*/