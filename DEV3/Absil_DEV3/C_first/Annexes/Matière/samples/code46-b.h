#include "code46-a.h"

struct B
{
    int _i;
    A* _a;
    
    inline int i() const
    {
        return _i;   
    }
};