#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct A { int i; };

int main()
{
    vector<A> v = {{5},{4},{3},{2},{1}};
    sort(v.begin(), v.end());
    for_each(v.begin(), v.end(), [](A a){ cout << a.i << " ";});
}
