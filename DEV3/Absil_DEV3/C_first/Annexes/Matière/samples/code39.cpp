#include <iostream>

using namespace std;

void f(void* p) { cout << "v" << endl; }
//void f(char* c) { cout << "c" << endl; }
void f(int* p) { cout << "i" << endl; }
void f(double* p) { cout << "d" << endl; }

int main()
{
    char c = 'c';
    f(&c);
}

/*
Problem:
What do you want?
*/