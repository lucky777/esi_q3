#include <stdio.h>

int main() 
{
	for(unsigned i = 5; i >= 0; i--)
		printf("%d ", i);
}

/*
Problem:
an 'unsigned' can't be negative, so when i will be 0, i-- will be the highest unsigned (infinite loop)

Solution:
Use an int instead of unsigned
*/