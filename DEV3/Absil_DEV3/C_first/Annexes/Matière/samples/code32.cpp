#include <iostream>
#include <vector>

class Wrapper
{
    std::vector<int> v;
    public:
        Wrapper(const std::vector<int>& v) : v(v) {}
        int operator[](int i) { return v[i]; }
};

int main()
{
    Wrapper w ({1,2,3});
    w[0] = -1;
    std::cout << w[0] << std::endl;
}

/*
Problem:
operator[] return a copy of the int, so it is a rvalue and we can't change it

Solution:
Return a reference
...
int& operator[](int i) { return v[i]; }
...
*/