#include <iostream>
#include <map>

using namespace std;

void print(const map<char, int> &m)
{	
	for(auto e : m)
		cout << "( " << e.first << " , " << e.second << " )" << endl;
	cout << endl;
}

int main()
{
	map<char, int> m;
	print(m);

	m['C'] = 10; 
    m['S'] = m['D'];    
	print(m);    	
}
