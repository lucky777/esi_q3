#include <stdio.h>

int f(const char tab[])  
{
	printf("%zu\n", sizeof(tab) / sizeof(*tab));
}

int main()
{
	char bat[] = {1, 2, 3, 4, 5, 6, 7};
	printf("%zu ", sizeof(bat) / sizeof(*bat));
	f(bat);
}

/*
Problem:
sizeof() is expecting a pointer* as parameter, not a 'tab[]'

Solution:
int f(const char* tab) {...}
*/