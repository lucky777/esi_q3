#include <iostream>

struct A 
{
    virtual void print() { std::cout << "A" << std::endl; }
};

struct B : A 
{
    void print() { std::cout << "B" << std::endl; }
};

int main()
{
    A a = B();
    a.print();
}
