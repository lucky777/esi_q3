#include <iostream>

using namespace std;

void f(int i = 0, int j)
{
    cout << i << " " << j << endl;   
}

int main()
{
    f(10);
}

/*
Problem:
When we call f(10), i = 10 and not j

Solution:
...
f(0, 10);
...

or

...
void f(int j, int i = 0)
...
*/