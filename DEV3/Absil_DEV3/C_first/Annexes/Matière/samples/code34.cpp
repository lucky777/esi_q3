#include <iostream>
#include <memory>

using namespace std;

class B;

struct A
{
    shared_ptr<B> pt;
    ~A() { cout << "-A" << endl; };
};

struct B
{
    shared_ptr<A> pt;
    ~B() { cout << "-B" << endl; };
};

int main()
{
    A a = A();
    auto b = make_shared<B>();
    a->pt = b; b->pt = a;    
} //memory should be automatically deallocated here
