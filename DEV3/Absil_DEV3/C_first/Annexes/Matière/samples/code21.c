#include <stdio.h>
#include <string.h>

int main()
{
    char* s1 = "Hello";
    char* s2 = " World";
    char s12[strlen(s1) + strlen(s2)];
    strcpy(s12, s1);
    strcat(s12, s2);
    printf("%s\n", s12);    
}

/*
Problem:
String must be zero ended so we need to add 1 case to s12

Solution:
...
    char s12[strlen(s1) + strlen(s2) + 1];
    ...