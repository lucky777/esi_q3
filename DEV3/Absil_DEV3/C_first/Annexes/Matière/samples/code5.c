#include <stdio.h> 

int main()  
{ 
	//vous ne pouvez pas modifier la ligne ci-dessous
	int i = 5; int j = 2; double d = 1.25;
	printf("%f\n", (i / j + d);
}

/*
Problem:
i and j are integers, so i/j will be troncated and coma will be lost

Solution:
Cast i and j in double

printf("%f\n", (double)i / (double)j + d);
*/