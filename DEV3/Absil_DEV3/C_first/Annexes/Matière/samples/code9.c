#include <stdio.h>
#include <stdlib.h>

void make_tab(unsigned* tab, unsigned size) 
{
	tab = (unsigned*)malloc(size * sizeof(unsigned));
	for(unsigned i = 0; i < size; i++)
		tab[i] = i;
}

int main() 
{
	unsigned * tab = NULL; 	
	make_tab(tab, 5); 	
	for(unsigned i = 0; i < 5; i++)
		printf("%d ", tab[i]);
}

/*
Problem:
tab is given in paramater as a copy

Solution:
Use pointer of pointer in function, and give reference of tab in parameter when calling it

void make_tab(unsigned** tab, unsigned size) 
{
	*tab = (unsigned*)malloc(size * sizeof(unsigned));
	for(unsigned i = 0; i < size; i++)
		(*tab)[i] = i;
}

int main() 
{
	unsigned * tab = NULL; 	
	make_tab(&tab, 5); 	
	for(unsigned i = 0; i < 5; i++)
		printf("%d ", tab[i]);
	printf("\n");
}
*/