#include <iostream>

struct A
{
    virtual A& operator=(const A& a) { std::cout << "=A" << std::endl; }  
};

struct B
{
    virtual B& operator=(const B& a) { std::cout << "=B" << std::endl; }  
};

int main()
{
    A * a = new A; B * b = new B;
    a = b;
}
