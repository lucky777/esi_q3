#include <iostream>
#include <concepts>

std::integral auto gcd(std::integral auto a, std::integral auto b)
{    
    return b == 0 ? a : gcd(b, a % b);
}

int main()
{
    std::cout << gcd(27, 15) << std::endl;
}