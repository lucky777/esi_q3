#include <stdio.h>

struct Token 
{
	int data;
	struct Token next;
};

typedef struct Token Token;

int main() 
{
	Token t1 {2}; Token t2 {3};
	t1.next = t2;
	printf("%d %d\n", t1.data, t2.data);
}

/*
Problem:
Token t1 {2}; invalid
And next must pointing to t2, and not getting a copy of it

Solution:
...
struct Token* next;
...
Token t1 = {2}; Token t2 = {3};
t1.next = &t2;
...
*/