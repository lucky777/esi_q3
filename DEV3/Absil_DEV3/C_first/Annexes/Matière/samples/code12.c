#include <stdio.h>

int main() 
{
	const char str[] = {'H', 'e', 'l', 'l', 'o'};
	printf("%s\n", str);
}

/*
Problem:
A string must be zero ended

Solution:
End with a \0

const char str[] = {'H', 'e', 'l', 'l', 'o', '\0'};
*/