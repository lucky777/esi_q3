#include <stdio.h>

void print(void* pt)
{
	printf("%d\n", (int)*pt);
}

int main()
{
	int i = 2;
	print(&i);
}

/*
Problem:
A void* pointer can't be dereferenced

Solution:
Create a temporary int pointer to print the value of the void pointer

...
int* tmp = pt;
printf("%d\n", *tmp);
...
*/