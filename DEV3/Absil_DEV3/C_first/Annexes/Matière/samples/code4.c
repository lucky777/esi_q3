#include <stdio.h>

void swap(int * i, int * j) 
{
	int * tmp = i;
	i = j; j = tmp;
}

int main() 
{
	int i = 2; int j = 3; swap(&i, &j);
	printf("%d %d\n", i, j);
}

/*
Problem:
The code is not swapping the values of i and j

Solution:
no need for tmp to be a pointer, and swap the values instead of using references

void swap(int* i, int* j) {
	int tmp = *i;
	*i = *j; *j = tmp;
}
*/