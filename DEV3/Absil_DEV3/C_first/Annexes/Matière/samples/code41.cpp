#include <iostream>

using namespace std;

struct A 
{
    int i;
    A(int i) { this->i = i; }
};

struct B
{
    A a;
    B(A a) { this->a = a; }
    void print() { cout << a.i << endl; }
};

int main()
{
    A a(2);
    B b(a);
    b.print();
}
