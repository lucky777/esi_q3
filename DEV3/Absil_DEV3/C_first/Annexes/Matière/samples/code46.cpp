#include <iostream>
#include "code46-a.h"
#include "code46-b.h"

int main()
{
    A a {1}; B b { 2, &a }; a._b = &b;
    std::cout << a.i() << " " << b.i() << std::endl;
}
