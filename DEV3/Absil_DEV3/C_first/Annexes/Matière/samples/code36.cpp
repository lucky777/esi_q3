#include <iostream>
#include <functional>

using namespace std;

struct A
{
    void print(int i) { cout << i << endl; }  
};

void apply(function<void (A&,int)> f, const A& a, int i)
{
    f(a, i);
}

int main()
{
    A a;
    function<void (A&,int)> f = &A::print;
    apply(f, a, 3);
}

/*
Problem:
method aply need a A& and not a const A&

Solution:
...
void apply(function<void (A&,int)> f, A& a, int i)
...
*/