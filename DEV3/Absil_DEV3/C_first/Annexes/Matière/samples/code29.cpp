#include <iostream>

template<int N>
struct A
{
    A() { std::cout << N << std::endl; } 
};

int main()
{
    int i = 2;
    A<i> a;
}

/*
Problem:
Template will be evaluated at compilation, but he doesn't know what is i

Solution:
...
constexpr int i = 2;
...
*/