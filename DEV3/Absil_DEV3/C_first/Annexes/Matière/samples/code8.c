#include <stdio.h>

int* give_me_a_one() 
{
	int * pt = 1;
	return pt;
}

int main() 
{	
	printf("%d\n", *give_me_a_one());
}

/*
Problem:
pt is a locale pointer of function give_me_a_one()

Solution:
Declare a globale variable and make pt point to its reference

#include <stdio.h>

int glob;

int* give_me_a_one() 
{
	int * pt = &glob;
	*pt = 1;
	return pt;
}

int main() 
{	
	printf("%d\n", *give_me_a_one());
}
*/