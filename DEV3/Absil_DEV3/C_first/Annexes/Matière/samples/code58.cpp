#include <iostream>

struct Integer
{
    int i;    
    Integer(int i = 0) : i(i) {}
    operator int() const { return i; }    
};

Integer operator+(const Integer& i, const Integer& j)
{
    return Integer(i.i + j.i);   
}

int main()
{
    Integer i = 3;
    int j = (i + 2.5);
    std::cout << j << std::endl;
}
