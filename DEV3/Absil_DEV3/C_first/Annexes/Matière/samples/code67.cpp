#include <iostream>

struct A
{
	int i;
	A(int i = 0) : i(i)  {}
};

struct B1 : A
{
	B1(int j = 0) : A(j) {}
};

struct B2 : A
{
	B2(int j = 0) : A(j) {}
};

struct C : B1, B2
{
	C(int j1 = 0, int j2 = 0) : B1(j1), B2(j2) {}
};

int main()
{
	C c (2, 4);	
	std::cout << c.i << std::endl;	
}
