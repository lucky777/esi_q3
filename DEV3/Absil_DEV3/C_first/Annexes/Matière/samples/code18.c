#include <stdio.h>

int main() 
{
	char str[5] = "Hello";
	printf("%s\n", str);
}

/*
Problem:
String must be zero ended '\0'

Solution:
'Hello' must be size 6 and not 5
'H', 'e', 'l', 'l', 'o', '\0'

char str[6] = "Hello";
*/