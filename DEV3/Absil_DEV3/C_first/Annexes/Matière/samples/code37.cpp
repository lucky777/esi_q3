#include <iostream>

using namespace std;

const int& f()
{
    const int i = 2;
    return i;
}

int main()
{
    cout << f() << endl;
}

/* Problem:
i is a locale variable, it will die at }

Solution:
Create a globale variable and make i a reference

...
const int x = 2;
...
const int& i = x;
...
*/