#include <iostream>
#include <memory>

using namespace std;

class Node
{
	int _data;
	shared_ptr<Node> _next;

	public:
		Node(int data, shared_ptr<Node> next = nullptr) : _data(data), _next(next) {}
		inline int data() const { return _data; }
		inline shared_ptr<Node> const next() const { return _next; }
		inline void setNext(shared_ptr<Node> next) { _next = next; }
};

class NodeIterator
{
	shared_ptr<Node> current;

	public:
		NodeIterator(shared_ptr<Node> current) : current(current) {}

		int operator *() { return current->data(); }

		NodeIterator& operator ++() //++ préfixé
		{
			current = current->next();
			return *this;
		}

		bool operator !=(const NodeIterator& it) const
		{
			return current != it.current;
		}
};

class LinkedList
{
	shared_ptr<Node> head;
	shared_ptr<Node> tail;
	
	public:
		LinkedList() : head(nullptr), tail(nullptr) {}    		

		void add(int data)
		{
			if(head == nullptr) //empty list
			{
				head = make_shared<Node>(data);
				tail = head;
			}
			else
			{
				auto last = make_shared<Node>(data);
				tail->setNext(last);
				tail = last;
			}
		}		

		NodeIterator begin() { return NodeIterator(head); }
		NodeIterator end() { return NodeIterator(nullptr); }
};

int main()
{
	LinkedList l;
	for(int i = 0; i < 10; i++)
		l.add(i);

    
	for(auto i : l)
		cout << i << endl;
    cout << endl;
    
    //same
    for(auto it = l.begin(); it != l.end(); ++it)
        cout << *it << endl;    
}
