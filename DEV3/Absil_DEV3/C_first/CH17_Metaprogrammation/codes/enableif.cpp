#include <iostream>

using namespace std;

template<class T>
struct A
{
    A(T) {}
    
    //template<std::enable_if_t<T,int> = false>    
    template<class U = T, std::enable_if_t<! std::is_same_v<U,int>, bool>>
    A(int) {}
};

int main()
{
    A<double> a(2.0); //ok
    A<int> b(1); //ko
}
