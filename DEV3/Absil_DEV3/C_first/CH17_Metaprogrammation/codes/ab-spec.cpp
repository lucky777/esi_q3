#include <iostream>

using namespace std;

struct A 
{ 
    A a() { return A(); } 
};

struct B 
{ 
    B b() { return B(); } 
};

struct AB : A, B
{
    AB a() { return AB(); }  
    AB b() { return AB(); }
};

template<class T>
class Wrapper
{
    T& t;
    public:
        Wrapper(T& t) = delete;
};

template<> class Wrapper<A>
{
    A& _a;
    public:
        Wrapper(A& a) : _a(a) {}
        A a() { return _a.a(); }
};

template<> class Wrapper<B>
{
    B& _b;
    public:
        Wrapper(B& b) : _b(b) {}
        B b() { return _b.b(); }
};

template<> class Wrapper<AB>
{
    AB& _ab;
    public:
        Wrapper(AB& ab) : _ab(ab) {}
        AB a() { return _ab.a(); }
        AB b() { return _ab.b(); }
};

struct AA 
{ 
    AA a() { return AA(); } 
};

int main()
{    
    int i = 2;    
    //Wrapper<int> w(i);
    //w.a(); w.b(); 
    
    A a; Wrapper<A> wa(a);
    wa.a(); //wa.b(); 
    
    B b; Wrapper<B> wb(b);
    /*wb.a(); */ wb.b();
        
    AB ab; Wrapper<AB> wab(ab);
    wab.a(); wab.b();
    
    AA aa; //Wrapper<AA> waa(aa);
    //waa.a(); waa.b();
}
