#include <iostream>
#include <iterator>
#include <vector>
#include <string>

template<class T>
concept Iterable = requires(T t)
{
    { t.begin() } -> std::forward_iterator;
    { t.end() } -> std::forward_iterator;
};

template<Iterable IT>
void print(const IT& it)
{
    for(const auto& a : it)
        std::cout << a << " ";
    std::cout << std::endl;
}

int main()
{
    std::vector<int> v = {0, 1, 2, 3, 4};
    print(v);
    print(std::string("Hello"));
}
