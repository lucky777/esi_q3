# Correction

## Code 1

Un `unsigned` ne sera jamais `négatif`. Si un unsigned est égal à `0` et qu'on lui soustrait `1`, il ne sera **pas** égal à `-1` mais à l'unsigned **le plus grand possible**. (Car tous les bits seront à `1`)

```c
//Solution
int main() {
    for(int i=5; i >= 5; i--) {
        ...
    }
}
```

## Code 2

En C, l'ordre d'évaluation des arguments n'est jamais défini.

```c
//Solution
int main() {
    int i = 0;
    printf("%d", i);
    i++;
    printf("%d", i);
    i++;
    printf("%d", i);
}
```

## Code 3

Lorsqu'on passe un tableau en paramètre d'une fonction, il est transormé en pointeur (sauf sizeof())

```c
//Solution
int f(const char tab[], unsigned size) {
    printf("%zu\n", sizeof(tab) / sizeof(*tab));
}

int main()
 {
     char bat[] = {1, 2, 3, 4, 5, 6, 7};
     printf("%zu", sizeof(bat)/sizeof(*bat));
     f(bat, (unsigned)sizeof(sizeof(bat)/sizeof(*bat));
 }
 ```

 ## Code 4

 Il faut échanger les valeurs et non les références.

```c
 void swap(int * i, int * j) {
     int tmp = *i;
     *i = *j;
     *j = tmp;
 }
 ```

 ## Code 5

 Il faut caster en double.

```c
 int i=5; int j=2; double d;
 d = (double)i/(double)j;
```

## Code 6

`t[]` est désalloué en fin de fonction car elle est locale à la fonction. Donc elle retournera une addresse vers un espace non alloué.

```c
//Solution à vérifier
const int t[] = (int*)malloc(5*sizeof(int));
```

## Code 7

Si on fait int * pt = 1;<br>
On définit la référence `1` et non pas la valeur.

```c
Pas de solution idéale ici.. Variable globale etc
```

## Code 8

En faisant `int * pt;`<br>
On alloue de la mémoire à un endroit aléatoire. Elle peut ne pas être acceptée par l'OS.

```c
MALLOC MA GUEULE
```

## Code 9

C'est une copie de tab qu'on envoie en paramètre. Il faut mettre un double pointeur.

```c
void make_tab(unsigned** tab, unsigned size)

make_tab(&tab, 5);
```

## Code 10

On travail avec un littéral ("hello world!") donc il est constant.

```c
jsais pas il va trop vite
```

## Code 11

int min; n'est jamais initialisé.

```c
//Solution
int min = MAX_INT;
//Ainsi min++ = 0;
```

## Code 12

N'est pas zéro terminé!
```c
//Solution
{'H', 'e', 'l', 'l', 'o', '\0'}
```

## Code 13

`unsigned` ne peut pas valoir `-1`.

```c
//Solution
int j; //Pas unsigned
```

## Code 14

On remplace MAX par '`5;`' et pas '`5`'.

```c
#define MAX 5
```

## Code 15

Fonction après le main, il faut prototyper.

C'est une erreur d'édition des liens et non de compilation.
```c
void hello();

int main() {}
```

## Code 16

On ne peut pas comparer deux chaînes avec ==.

```c
printf("%d\n", strcmp("abc", "abc");
```

## Code 17

Un littéral string est constant on ne peut pas le modifier.

```c
Il faut utiliser un buffer
```

## Code 18

N'est pas zéro terminé. (à vérifier)

## Code 19

Un Token a besoin de la taille d'un Token pour être défini. 

```c
struct Token {
    int data;
    struct Token* next;
}
```

## Code 20

Quand on essaie de déréfencer void* il n'est pas content.

Il faut caster en int* puis déférencer.