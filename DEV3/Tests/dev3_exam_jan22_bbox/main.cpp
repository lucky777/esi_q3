#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include "bbox.h"
#include "data.h"
#include "lucky_tools.h"

//QUESTION: Pourquoi le vecteur de pointeurs de BBox bug dans le for() à la création?

//Ce code ne fonctionne qu'avec ce data.cpp-ci (car les boîtes vont de -50 à 50)
//Contrairement au data.cpp original de l'examen ou elles allaient de -100 à 1920
//(Modification dans le but d'avoir un affichage correcte)

using namespace he2b;
using namespace std;

bool compare_area(const BBox* b1, const BBox* b2) {
    return b1->area() < b2->area();
}


bool compare_width_weight(const BBox* b1, const BBox* b2) {
    if(b1->width() == b2->width()) return b1->height() < b2->height();
    return b1->width() < b2->width();
}

BBox bbox_from_points(const std::vector<Point>& vec) {
    if(vec.size()==0) return BBox();
    int x1=vec[0].x, y1=vec[0].y, x2=vec[0].x, y2=vec[0].y;
    for(Point p : vec) {
        x1 = (p.x < x1) ? p.x : x1;
        x2 = (p.x > x2) ? p.x : x2;
        y1 = (p.y < y1) ? p.y : y1;
        y2 = (p.y > y2) ? p.y : y2;
    }
    return BBox(Point(x1, y1), Point(x2, y2));
}

int main()
{
    vector<pair<Point, Point>> rnd_pair_points;
    vector<BBox> boxes;
    vector<const BBox*> pointers;
    vector<Point> rnd_points;
    int cpt;
    Point pt {10, 20};
    BBox bbox1 {pt};
    BBox bbox2 {{-5, 0}, {5, 42}};

    clearscr();
    box("question 2");
    std::cout << "pt: " << pt.x << ", " << pt.y << std::endl
              << "bbox1: " << bbox1 << std::endl
              << "bbox2: " << bbox2 << std::endl
              << "intersection: " << (bbox1 & bbox2) << std::endl
              << "union: " << (bbox1 | bbox2) << std::endl
              << "pt in union: " << std::boolalpha
              << (bbox1 | bbox2).contains(pt) << std::endl;

    box("question 3");
    init_data();
    cpt = 0;
    rnd_pair_points = point_pairs(10);
    for(int i=0; i<10; i++) {
        boxes.push_back(BBox(rnd_pair_points[i].first, rnd_pair_points[i].second));
        cout << green << boxes[i] << def << endl;
        if(boxes[i].contains(pt)) {
            cpt++;
            log("pt inside new box!");
        }
    }

    box("question 4");
    box("pt is inside " + std::to_string(cpt) + " boxe(s)", green);
    enter();

    box("question 5");

    for(int i=0; i<10; i++) pointers.push_back(&boxes[i]);

    sort(pointers.begin(), pointers.end(), compare_area);
    box("sorted by area", magenta);
    for(const BBox* b : pointers) cout << magenta << *b << def << endl;
    enter();
    
    sort(pointers.begin(), pointers.end(), compare_width_weight);
    box("sorted by width and height", magenta);
    for(const BBox* b : pointers) cout << magenta << *b << def << endl;
    enter();
    
    box("question 6");
    rnd_points = points(10);
    cout << cyan << "10 points:" << def << endl;
    
    for(Point p : rnd_points) {
        cout << cyan << p.x << " | " << p.y << def << endl;
    }

    box("Box containing all 10 points", cyan);

    BBox fp = bbox_from_points(rnd_points);
    cout << cyan << fp << def << endl;
    enter();
    
    box("printing boxes", green);
    
    print_boxes(boxes);
    
    box("end", red);
    return 0;
}
