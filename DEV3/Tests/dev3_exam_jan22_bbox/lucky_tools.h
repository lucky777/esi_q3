#ifndef LUCKY_TOOLS_HPP
#define LUCKY_TOOLS_HPP

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 77
#define KEY_RIGHT 75
#define KEY_ESC 27
#define KEY_ENTER 10

#include <string>
#include <iostream>
#include <vector>

namespace Color {
    enum Code {
        FG_BLACK     = 30,
        FG_RED       = 31,
        FG_GREEN     = 32,
        FG_ORANGE    = 33,
        FG_BLUE      = 34,
        FG_MAGENTA   = 35,
        FG_CYAN      = 36,
        FG_LIGHTGRAY = 37,
        FG_DEFAULT   = 39,

        BG_RED       = 41,
        BG_GREEN     = 42,
        BG_BLUE      = 44,
        BG_DEFAULT   = 49
    };

    class Modifier {
        Code code;
    public:
        Modifier(Code pCode);
        void mod(Code pCode);
        Code get_code() const;
        std::string use();
    };
    std::ostream& operator<<(std::ostream&, const Modifier&);
}

namespace Frame_Box {
    class Fbox {
        std::string title;
        std::vector<std::string> txt;
    public:
        Fbox();
        bool set_title(std::string);
        bool add(std::string);
        bool print() const;
        bool print(Color::Modifier) const;
        bool clear();
    };
}

static Color::Modifier black(Color::FG_BLACK);
static Color::Modifier red(Color::FG_RED);
static Color::Modifier green(Color::FG_GREEN);
static Color::Modifier blue(Color::FG_BLUE);
static Color::Modifier orange(Color::FG_ORANGE);
static Color::Modifier magenta(Color::FG_MAGENTA);
static Color::Modifier cyan(Color::FG_CYAN);
static Color::Modifier lightgray(Color::FG_LIGHTGRAY);
static Color::Modifier def(Color::FG_DEFAULT);

static Color::Modifier red_bg(Color::BG_RED);
static Color::Modifier green_bg(Color::BG_GREEN);
static Color::Modifier blue_bg(Color::BG_BLUE);
static Color::Modifier def_bg(Color::BG_DEFAULT);

static Frame_Box::Fbox fbox;

size_t biggest_word(std::vector<std::string>);

bool box(std::string);
bool box(std::string, Color::Modifier);

void clearscr();
void zzz(int);
void error(const std::string);
void log(const std::string);
void enter();
bool ask_yn(const std::string);
int ask_int(int, int);
void bye();

#endif