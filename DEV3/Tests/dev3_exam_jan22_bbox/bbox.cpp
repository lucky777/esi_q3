#include <iostream>
#include "lucky_tools.h"
#include "bbox.h"

//QUESTION: pourquoi 'he2b::' pour les opérateurs hors classe BBox??

using namespace he2b;
using namespace std;

//constructors

BBox::BBox(int left, int bottom, int right, int top) : left_(left),
                                                       bottom_(bottom),
                                                       right_(right),
                                                       top_(top) {}

BBox::BBox() {
    BBox(0, 0, -1, -1);
}

BBox::BBox(const Point& point) : left_(point.x),
                                 bottom_(point.y),
                                 right_(point.x),
                                 top_(point.y) {}

BBox::BBox(const Point& corner1, const Point& corner2) {
    if(corner1.x < corner2.x) {
        this->left_ = corner1.x;
        this->right_ = corner2.x;
    } else {
        this->left_ = corner2.x;
        this->right_ = corner1.x;
    }

    if(corner1.y < corner2.y) {
        this->bottom_ = corner1.y;
        this->top_ = corner2.y;
    } else {
        this->bottom_ = corner2.y;
        this->top_ = corner1.y;
    }
}

//getters

Point BBox::bottomLeft() const {
    return Point(this->left_, this->bottom_);
}

Point BBox::topLeft() const {
    return Point(this->left_, this->top_);
}

Point BBox::bottomRight() const {
    return Point(this->right_, this->bottom_);
}

Point BBox::topRight() const {
    return Point(this->right_, this->top_);
}

//methods

int BBox::width() const {
    return this->right_-this->left_ + 1;
}

int BBox::height() const {
    return this->top_-this->bottom_ + 1;
}

int BBox::area() const {
    return this->width()*this->height();
}

bool BBox::empty() const {
    return this->area()==0;
}

bool containsCorner(const BBox& b1, const BBox& b2) {
    return b1.contains(b2.bottomLeft())
        || b1.contains(b2.topLeft())
        || b1.contains(b2.bottomRight())
        || b1.contains(b2.topRight());
}

bool crossOver(const BBox& b1, const BBox& b2) {
    return (b1.bottomLeft().x < b2.bottomLeft().x
         && b1.bottomRight().x > b2.bottomLeft().x
         && b1.bottomLeft().y < b2.bottomLeft().y
         && b1.topLeft().y > b2.bottomLeft().y);
}

bool BBox::contains(const Point& pt) const {
    return this->left_ <= pt.x && this->right_ >= pt.x &&
           this->bottom_ <= pt.y && this->top_ >= pt.y;
}

bool BBox::overlaps(const BBox& other) const {
    return (containsCorner(*this, other) || containsCorner(other, *this))
        || (crossOver(*this, other) || crossOver(*this, other));
}

BBox& BBox::operator &= (const BBox& rhs) {
    int x1, x2, y1, y2;
    if(!this->overlaps(rhs)) {
        this->left_=0;
        this->right_=-1;
        this->bottom_=0;
        this->top_=-1;
    } else {
        x1 = (this->left_ > rhs.bottomLeft().x) ? this->left_ : rhs.bottomLeft().x;
        x2 = (this->right_ < rhs.topRight().x) ? this->right_ : rhs.topRight().x;
        y1 = (this->bottom_ > rhs.bottomLeft().y) ? this->bottom_ : rhs.bottomLeft().y;
        y2 = (this->top_ < rhs.topRight().y) ? this->top_ : rhs.topRight().y;
        this->left_=x1;
        this->right_=x2;
        this->bottom_=y1;
        this->top_=y2;
    }
    return *this;
}

BBox& BBox::operator |= (const BBox& rhs) {
    int x1, x2, y1, y2;
    x1 = (this->left_ < rhs.bottomLeft().x) ? this->left_ : rhs.bottomLeft().x;
    x2 = (this->right_ > rhs.topRight().x) ? this->right_ : rhs.topRight().x;
    y1 = (this->bottom_ < rhs.bottomLeft().y) ? this->bottom_ : rhs.bottomLeft().y;
    y2 = (this->top_ > rhs.topRight().y) ? this->top_ : rhs.topRight().y;
    this->left_=x1;
    this->right_=x2;
    this->bottom_=y1;
    this->top_=y2;
    return *this;
}

BBox he2b::operator & (const BBox& lhs, const BBox& rhs) {
    BBox tmp(Point(lhs.bottomLeft().x, lhs.bottomLeft().y),
             Point(lhs.topRight().x, lhs.topRight().y));
    tmp &= rhs;
    return tmp;
}

BBox he2b::operator | (const BBox& lhs, const BBox& rhs) {
    BBox tmp(Point(lhs.bottomLeft().x, lhs.bottomLeft().y),
             Point(lhs.topRight().x, lhs.topRight().y));
    tmp |= rhs;
    return tmp;
}

bool he2b::operator < (const BBox& lhs, const BBox& rhs) {
    return (lhs.area() < rhs.area());
}

ostream& he2b::operator << (ostream& os, const BBox& bbox) {
    return os << "BBox(left: " << bbox.bottomLeft().x
              << ", bottom: " << bbox.bottomLeft().y
              << ", right: " << bbox.topRight().x
              << ", top: " << bbox.topRight().y
              << ", size: " << bbox.width() << "x" << bbox.height()
              << ", area: " << bbox.area() << ")";
}

bool cmp_point(const Point& p1, const Point& p2) {
    return p1.x == p2.x && p1.y == p2.y;
}

bool x_between(const Point& p, const Point& p1, const Point& p2) {
    if(p1.y != p2.y || p.y != p1.y) return false;
    if(p1.x < p2.x) {
        return p.x > p1.x && p.x < p2.x;
    } else {
        return p.x > p1.x && p.x < p2.x;
    }
}

bool y_between(const Point& p, const Point& p1, const Point& p2) {
    if(p1.x != p2.x || p.x != p1.x) return false;
    if(p1.y < p2.y) {
        return p.y > p1.y && p.y < p2.y;
    } else {
        return p.y > p1.y && p.y < p2.y;
    }
}

void he2b::print_boxes(const vector<BBox>& vec) {
    string c = " ";
    int cpt = 0;
    bool found;
    vector<Color::Modifier> cols;
    cols.push_back(red);
    cols.push_back(green);
    cols.push_back(blue);
    cols.push_back(orange);
    cols.push_back(magenta);
    cols.push_back(cyan);
    //si le point est un coin alors c = '└┘┌┐' ┬┴ ┤├
    //else si x_between alors c = '─'
    //else si Y-between alors c = '│'
    cout << orange.use()
         << "= = = = = = = = = = = = = = = = = = = = = = = = = ["
         << green.use()
         << "50"
         << orange.use()
         << "] = = = = = = = = = = = = = = = = = = = = = = = = =\n"
         << def.use();
    for(int y=50; y>=-50; y--) {
        for(int x=-50; x<=50; x++) {
            Point p(x, y);
            c = " ";
            cpt = 0;
            found = false;
            while(cpt < vec.size() && !found) {
                if(cmp_point(p, vec[cpt].bottomLeft())
                && cmp_point(p, vec[cpt].topRight())) {c = "+"; found = true;}
                else if(cmp_point(p, vec[cpt].bottomLeft())
                && cmp_point(p, vec[cpt].bottomRight())) {c = "┴"; found = true;}
                else if(cmp_point(p, vec[cpt].topLeft())
                && cmp_point(p, vec[cpt].topRight())) {c = "┬"; found = true;}
                else if(cmp_point(p, vec[cpt].bottomLeft())
                && cmp_point(p, vec[cpt].topLeft())) {c = "├"; found = true;}
                else if(cmp_point(p, vec[cpt].bottomRight())
                && cmp_point(p, vec[cpt].topRight())) {c = "┤"; found = true;}
                else if(cmp_point(p, vec[cpt].bottomLeft())) {c = "└"; found = true;}
                else if(cmp_point(p, vec[cpt].bottomRight())) {c = "┘"; found = true;}
                else if(cmp_point(p, vec[cpt].topLeft())) {c = "┌"; found = true;}
                else if(cmp_point(p, vec[cpt].topRight())) {c = "┐"; found = true;}
                else if(x_between(p, vec[cpt].bottomLeft(), vec[cpt].bottomRight())
                     || x_between(p, vec[cpt].topLeft(), vec[cpt].topRight())) {c = "─"; found = true;}
                else if(y_between(p, vec[cpt].bottomLeft(), vec[cpt].topLeft())
                     || y_between(p, vec[cpt].bottomRight(), vec[cpt].topRight())) {c = "│"; found = true;}
                cpt ++;
            }
            cout << cols[(cpt-1)%6] << c << def << flush;
        }
        zzz(14);
        cout << endl;;
    }
    cout << orange.use()
         << "= = = = = = = = = = = = = = = = = = = = = = = = ["
         << green.use()
         << "-50"
         << orange.use()
         << "] = = = = = = = = = = = = = = = = = = = = = = = = =\n"
         << def.use();
}