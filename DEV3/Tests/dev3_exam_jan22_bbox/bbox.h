/*!
 * \file bbox.h
 *
 * \brief Définition de la classe BBox.
 */
#ifndef BBOX_H
#define BBOX_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include "point.h"

/*!
 * \brief Espace de nommage de la [HE2B](https://www.he2b.be/).
 */
namespace he2b
{

/*!
 * \brief Classe représentant une Axis-Aligned Bounding Box (BBox).
 *
 * La _boîte englobante_, ou _Bounding Box_ (BBox) est un concept très utilisé
 * en dessin informatique en deux dimensions. Le concept est simple : c'est un
 * rectangle de dimension minimum qui englobe une « figure ». Dans le cadre de
 * cet énoncé, on se contentera d'englober des points. Notez que les côtés de
 * ces rectangles seront alignés avec les axes de coordonnées (il n'y a pas de
 * rotation). Techniquement, on parle de _Axis-Aligned Bounding Box_.
 *
 * Une boîte englobante est entièrement définie par son coin inférieur gauche et
 * son coin supérieur droit, deux points aux coordonnées entières (des
 * _pixels_). De là, on peut définir sa largeur, sa hauteur, son aire, etc.
 * Les points englobés sont ceux se trouvant dans le rectangle délimité par ses
 * deux coins, bordures comprises.
 *
 * Une boîte peut englober un seul pixel, qui est un carré de taille 1. Les
 * coins inférieur gauche et supérieur droit sont alors égaux.
 *
 * À noter qu'il existe des boîtes vides, définies comme ayant leur largeur et
 * leur hauteur nulle.
 *
 * Il n'est pas possible (si votre implémentation est correcte), avec cette
 * interface d'obtenir une boîte invalide, c'est-à-dire qui aurait sa largeur ou
 * hauteur négatives, ou dont seulement l'une des deux dimensions
 * (largeur/haute) serait nulle.
 */
class BBox
{
    /**@{*/
    /*!
     * \brief limites gauche, basse, droite, haute de la boîte.
     */
    int left_, bottom_, right_, top_;
    /**@}*/

    /*!
     * \brief Constructeur direct d'une BBox.
     *
     * À usage interne. Aucune vérification n'est faite sur la validité des
     * paramètres.
     * \param left limite gauche de la BBox.
     * \param bottom limite basse de la BBox.
     * \param right limite droite de la BBox.
     * \param top limite haute de la BBox.
     */
    BBox(int left, int bottom, int right, int top);
public:

    /*!
     * \brief Constructeur par défaut d'une BBox, vide.
     *
     * De manière interne, elle est délimitée par les points <tt>(0, 0)</tt> et
     * <tt>(-1, -1)</tt>, de telle sorte que sa hauteur et largeur soient
     * nulles.
     */
    BBox();

    /*!
     * \brief Construit une BBox englobant __un__ pixel.
     *
     * Les coins inférieur gauche et supérieur droit sont tous deux égaux au
     * pixel englobé.
     * \param point l'unique pixel englobé.
     */
    BBox(const Point& point);

    /*!
     * \brief Construit une BBox délimitée par les coins donnés.
     *
     * Les coins donnés en argument ne doivent pas nécessaire être les coins
     * inférieur gauche et supérieur droit. Il s'agit de deux points "opposés"
     * de la boîte. Les coordonnées correctes desx-terminal-emulator >& /dev/null & disown limites gauche, basse, droite
     * et haute sont déduites de ces deux points.
     *
     * Ce constructeur construit donc toujours une boîte non vide, quels que
     * soient les coins donnés.
     * \param corner1 coin quelconque du rectangle.
     * \param corner2 coin opposé au premier coin.
     */
    BBox(const Point& corner1, const Point& corner2);

    /*!
     * \brief Coin inférieur gauche de la boîte.
     * \return coin inférieur gauche.
     */
    Point bottomLeft() const;
    /*!
     * \brief Coin supérieur gauche de la boîte.
     * \return coin supérieur gauche.
     */
    Point topLeft() const;
    /*!
     * \brief Coin inférieur droit de la boîte.
     * \return coin inférieur droit.
     */
    Point bottomRight() const;
    /*!
     * \brief Coin supérieur droit de la boîte.
     * \return coin supérieur droit.
     */
    Point topRight() const;

    /*!
     * \brief Largeur de la boîte.
     *
     * Toujours positive. Nulle si et seulement si la boîte est vide.
     *
     * Un pixel est de largeur 1.
     * \return Largeur de la boîte.
     */
    int width() const;
    /*!
     * \brief Hauteur de la boîte.
     *
     * Toujours positive. Nulle si et seulement si la boîte est vide.
     *
     * Un pixel est de hauteur 1.
     * \return Hauteur de la boîte.
     */
    int height() const;

    /*!
     * \brief Aire de la boîte.
     *
     * Une boîte englobant un seul pixel possède une aire de 1 pixel².
     * \return Aire de la boîte, en pixel².
     */
    int area() const;

    /*!
     * \brief Indique si la boîte est vide.
     *
     * \return Vrai si la boîte est vide.
     */
    bool empty() const;

    /*!
     * \brief Indique si le point donné est contenu dans la boîte.
     * \param pt point à tester.
     * \return Vrai si le point est dans la boîte.
     */
    bool contains(const Point& pt) const;
    /*!
     * \brief Indique si la boîte donnée intersecte cette boîte.
     *
     * Les bordures comptent dans l'intersection. Ainsi, les boîtes
     * <code>BBox({0,0}, {10, 10})</code> et <code>BBox({10,0}, {20,10})</code>
     * sont en intersection (sur une largeur de 1 pixel :
     * <code>BBox({10, 0}, {10, 10})</code>).
     * \param other l'autre boîte avec laquelle tester l'intersection.
     * \return Vrai si les deux boites sont en intersection.
     */
    bool overlaps(const BBox& other) const;

    /*!
     * \brief [Opérateur d'assignation]
     * (https://en.cppreference.com/w/cpp/language/operator_assignment)
     * pour l'intersection.
     *
     * Après l'opération <code>bbox_a &= bbox_b</code>, la boîte
     * <code>bbox_a</code> représente le résultat de l'intersection entre
     * <code>bbox_a</code> et <code>bbox_b</code>.
     *
     * L'intersection entre deux boîtes est la plus grande boîte complètement
     * incluse aux deux autres.
     *
     * Notez que l'intersection peut être vide.
     *
     * \param rhs membre de droite de l'opérateur.
     * \return le membre de gauche.
     */
    BBox& operator &= (const BBox& rhs);
    /*!
     * \brief [Opérateur d'assignation]
     * (https://en.cppreference.com/w/cpp/language/operator_assignment)
     * pour l'union.
     *
     * Après l'opération <code>bbox_a |= bbox_b</code>, la boîte
     * <code>bbox_a</code> représente le résultat de l'union entre
     * <code>bbox_a</code> et <code>bbox_b</code>.
     *
     * L'union de deux boîtes est la plus petite boîte qui les contient toutes
     * les deux.
     *
     * \param rhs membre de droite de l'opérateur.
     * \return le membre de gauche.
     */
    BBox& operator |= (const BBox& rhs);
};

/*!
 * \brief Opérateur d'intersection entre deux boîtes.
 *
 * Cet opérateur est commutatif.
 *
 * cfr. BBox::operator&=().
 * \param lhs membre de gauche de l'opérateur.
 * \param rhs membre de droite de l'opérateur.
 * \return BBox résultant de l'intersection.
 */
BBox operator & (const BBox& lhs, const BBox& rhs);
/*!
 * \brief Opérateur d'union entre deux boîtes.
 *
 * Cet opérateur est commutatif.
 *
 * cfr. BBox::operator|=().
 * \param lhs membre de gauche de l'opérateur.
 * \param rhs membre de droite de l'opérateur.
 * \return BBox résultant de l'union.
 */
BBox operator | (const BBox& lhs, const BBox& rhs);

/*!
 * \brief Opérateur de comparaison stricte d'aire.
 * \param lhs membre de gauche de la comparaison.
 * \param rhs membre de droite de la comparaison.
 * \return Vrai si le membre de gauche possède une aire strictement plus
 * petite que l'aire du membre droite.
 */
bool operator <(const BBox& lhs, const BBox& rhs);

/*!
 * \brief Opérateur de sortie sur un stream.
 *
 * La sortie doit être de la forme, pour <code>BBox({22, 42}, {42, 83})</code> :
 *
 *     BBox(left: 22, bottom: 42, right: 42, top: 83, size: 21x42)
 *
 * \param os output stream dans lequel insérer les données.
 * \param bbox boîte à insérer.
 * \return l'output stream passé en entrée.
 */
std::ostream& operator<<(std::ostream& os, const BBox& bbox);
void print_boxes(const std::vector<BBox>& vec);

}
#endif // BBOX_H
