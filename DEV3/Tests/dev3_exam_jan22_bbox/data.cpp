#include "data.h"

#include <algorithm>
#ifdef _WIN32
#include <ctime>
#endif
#include <limits>
#include <random>
#include <utility>

namespace he2b
{

inline auto& urng()
{
#ifdef _WIN32
    static std::mt19937 u {};
    // https://stackoverflow.com/a/32731387
    // dans le lien précédent : Linux   <-> gcc
    //                       et Windows <-> msvc
#else
    static std::default_random_engine u {};
#endif
    return u;
}

inline int random_value(int min = std::numeric_limits<int>::min(),
                        int max = std::numeric_limits<int>::max())
{
    static std::uniform_int_distribution<int> d {};

    if (max < min) { std::swap(min, max); }

    return d(urng(), typename decltype(d)::param_type {min, max});
}

void init_data()
{
#ifdef _WIN32
    urng().seed(std::time(nullptr));
    // https://stackoverflow.com/a/18908041
#else
    static std::random_device rd {};
    urng().seed(rd());
#endif
}

std::vector<Point> points(std::size_t size)
{
    std::vector<Point> result(size);

    std::generate(std::begin(result), std::end(result), []() -> Point {
        return {random_value(-50, 50), random_value(-50, 50)};
    });

    return result;
}

std::vector<std::pair<Point, Point>> point_pairs(std::size_t size)
{
    std::vector<std::pair<Point, Point>> result(size);

    std::generate(std::begin(result), std::end(result),
    []() -> std::pair<Point, Point> {
        return {
            {random_value(-50, 50), random_value(-50, 50)},  //-100, 1920     -100, 1080
            {random_value(-50, 50), random_value(-50, 50)}   //-100, 1920     -100, 1080
        };
    });

    return result;
}

}
