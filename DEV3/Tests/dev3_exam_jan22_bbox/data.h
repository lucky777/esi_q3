/*!
 * \file data.h
 *
 * \brief Génération des données de points et boites.
 */
#ifndef DATA_H
#define DATA_H

#include <utility>
#include <vector>

#include "point.h"

/*!
 * \brief Espace de nommage de la [HE2B](https://www.he2b.be/).
 */
namespace he2b
{

/*!
 * \brief Initialisation du générateur de données.
 *
 * Appelez cette fonction _une fois_ avant d'utiliser les fonctions
 * points() et point_pairs().
 */
void init_data();

/*!
 * \brief Génère un vecteur de Point aléatoirement.
 * \param size nombre de points à générer.
 * \return Un vecteur de points aléatoires.
 */
std::vector<Point> points(std::size_t size);

/*!
 * \brief Génère des paires de points aléatoirement.
 * \param size nombre de paires à générer.
 * \return Un vecteur de paires de points aléatoires.
 */
std::vector<std::pair<Point, Point>> point_pairs(std::size_t size);

}

#endif // DATA_H
