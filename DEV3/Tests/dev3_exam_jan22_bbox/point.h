/*!
 * \file point.h
 *
 * \brief Définition de la structure de Point.
 */

#ifndef POINT_H
#define POINT_H

/*!
 * \brief Espace de nommage de la [HE2B](https://www.he2b.be/).
 */
namespace he2b
{

/*!
 * \brief Classe définissant un point 2D en pixels.
 */
struct Point {
    /*!
     * \brief Abscisse du point.
     */
    int x;
    /*!
     * \brief Ordonnée du point.
     */
    int y;

    /*!
     * \brief Construit un Point à partir de ses absisse et ordonnée.
     * \param x absisse (0 par défaut).
     * \param y ordonnée (0 par défaut).
     */
    Point(int x = 0, int y = 0) : x(x), y(y) {}
};

}

#endif // POINT_H
