#include <iostream>
#include <climits>
#include <thread>
#include <chrono>
#include "lucky_tools.h"

//Color::Modifier

Color::Modifier::Modifier(Code pCode) : code(pCode) {}

void Color::Modifier::mod(Code pCode) {
    this->code = pCode;
}

std::string Color::Modifier::use() {
    return "\033[" + std::to_string(this->code) + "m";
}

std::ostream& Color::operator<<(std::ostream& os, const Color::Modifier& mod) {
    return os << "\033[" << mod.get_code() << "m";
}

Color::Code Color::Modifier::get_code() const {
    return this->code;
}

//Fbox

Frame_Box::Fbox::Fbox() : title("") {}

bool Frame_Box::Fbox::set_title(std::string title) {
    this->title = title;
    return true;
}

bool Frame_Box::Fbox::add(std::string txt) {
    std::string tmp = "";

    if(txt.length() > 200) {
        error("fbox: max 200 chars");
        return false;
    }

    for(int i=0; i<txt.length(); i++) {
        if(txt[i] == '\n') {
            this->txt.push_back(tmp);
            tmp = "";        
        } else {
            tmp += txt[i];
        }
    }

    if(tmp != "") this->txt.push_back(tmp);
    return true;
}

bool Frame_Box::Fbox::print() const {
    return print(def);
}

bool Frame_Box::Fbox::print(Color::Modifier color) const {
    size_t big;
    int ms;

    if(this->txt.size() == 0) {
        error("fbox is empty, nothing to print");
        return false;
    }

    Color::Modifier col(Color::FG_DEFAULT);

    try {
        col.mod(color.get_code());
    } catch (...) {
        error("color not found!");
        col.mod(Color::FG_DEFAULT);
    }

    // START

    big = biggest_word(this->txt);
    ms=42;
    if(big<4) big=4;

    //TOP

    std::cout << col << "       ╒";
    for (int i=0; i<big+6; i++) {
        std::cout << "═";
    }
    std::cout << "╕" << std::endl;
    zzz(ms);

    //TITLE

    if(this->title.length() > 0) {
        std::cout << "       │";
        for (int i=0; i<(big/2) + 1; i++) {
            std::cout << " ";
        }
        std::cout << this->title;
        for (int i=0; i<(big/2)+1+(big%2); i++) {
            std::cout << " ";
        }
        std::cout << "│░" << std::endl;
        zzz(ms);
        //TITLE LINE
        std::cout << "       ├";
        for (int i=0; i<big+6; i++) {
            std::cout << "─";
        }
        std::cout << "┤░" << std::endl;
        zzz(ms);
    }

    //EMPTY

    std::cout << "       │";
    for (int i=0; i<big+6; i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //ENTRIES

    for(int i=0; i<this->txt.size(); i++) {
        std::cout << "       │   ";
        std::cout << this->txt[i];
        for(int j=0; j<big-this->txt[i].length()+3; j++) {
            std::cout << " ";
        }
        std::cout << "│░" << std::endl;
        zzz(ms);
    }

    //EMPTY

    std::cout << "       │";
    for (int i=0; i<big+6; i++) {
        std::cout << " ";
    }
    std::cout << "│░" << std::endl;
    zzz(ms);

    //BOTTOM
    std::cout << "       " << col << "└";
    for (int i=0; i<big+6; i++) {
        std::cout << "─";
    }
    std::cout << "┘░" << std::endl;
    std::cout << "         ";
    for (int i=0; i<big+6; i++) {
        std::cout << "░";
    }
    std::cout << "░" << std::endl << std::endl;
    zzz(ms);

    return true;
}

bool Frame_Box::Fbox::clear() {
    this->txt.clear();
    return true;
}

//Functions

size_t biggest_word(const std::vector<std::string> vec) {
    size_t result = 0;

    for(int i=0; i<vec.size(); i++) {
        if (vec[i].length() > result) result = vec[i].length();
    }

    return result;
}

bool box(std::string txt) {
    return box(txt, def);
}

bool box(std::string txt, Color::Modifier color) {

    Color::Modifier col(Color::FG_DEFAULT);

    try {
        col.mod(color.get_code());
    } catch (...) {
        error("color  not found!");
        col.mod(Color::FG_DEFAULT);
    }

    std::cout << std::endl << col << "              ┌";

    for (int i=0; i<(txt.length()+6); i++) {
        std::cout << "─";
    }

    std::cout << "┐" << std::endl;
    std::cout << "              │   " << txt << "   │▏" << std::endl;
    std::cout << "              └";

    for (int i=0; i<(txt.length()+6); i++) {
        std::cout << "─";
    }

    std::cout << "┘▏" << std::endl << "               ";

    for (int i=0; i<(txt.length()+7); i++) {
        std::cout << "▔";
    }
    std::cout << def << std::endl;

    return true;
}

void clearscr() {
    #ifdef _WIN32
        system("cls");
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        puts("\x1b[2J\x1b[1;1H");
    //add some other OSes here if needed
    #else
        #error "OS not supported."
        //you can also throw an exception indicating the function can't be used
    #endif
}

void zzz(int ms) {
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void error(const std::string txt) {
    std::cerr << red << "[e] " << txt << def << std::endl;
}

void log(const std::string txt) {
    std::cerr << blue << "[i] " << txt << def << std::endl;
}

void enter() {
    std::cout << "press ENTER to continue..";
    while(getchar()!='\n');
    //getchar();
    std::cout << std::endl;
}

bool ask_yn(const std::string txt) {
    std::string result;
    std::cout << txt << " [" << green << "Y" << def << "/" << red << "N" << def << "] > ";
    std::cin >> result;
    while (result != "Y" && result != "y" && result != "N" && result != "n") {
        error("enter 'y' or 'n'");
        std::cout << txt << " [" << green << "Y" << def << "/" << red << "N" << def << "] > ";
        std::cin >> result;
    }
    return (result == "Y" || result == "y");
}

int ask_int(int min = 0, int max = INT_MAX) {
    std::string str;
    int nb;
    std::cin >> str;
    try {
        nb = stoi(str);
    } catch (std::invalid_argument e){
        nb = min-1;
    }
    while(nb < min || nb > max) {
        error("invalid number: " + str);
        std::cout << "try again: " << std::endl << "> ";
        std::cin >> str;
        try {
            nb = stoi(str);
        } catch (std::invalid_argument e){
            nb = min-1;
        }
    }
    return nb;
}

void bye() {
    clearscr();
    box("    ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("b   ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("by  ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("bye ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("bye!", blue);
    std::cout << std::flush;
    zzz(777);
    clearscr();
    std::cout << std::flush;
}