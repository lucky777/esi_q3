#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "generator.h"

#define MIN_FRAMES 5
#define MAX_FRAMES 20
#define MIN_PL_SIZE 2
#define MAX_PL_SIZE 10
#define MIN_SENSORS 3
#define MAX_SENSORS 6

#define MIN_VALUE 0
#define MAX_VALUE RAND_MAX

#define TIME_RAND 1

unsigned rand_uint(unsigned min, unsigned max)
{
    return min + rand() % (max - min);
}

size_t nb_frames = 0;

void prepare_data(void)
{
#if TIME_RAND
        srand((unsigned) time(NULL));
#endif
    nb_frames = rand_uint(MIN_FRAMES, MAX_FRAMES + 1);
}

unsigned *read_frame(void)
{
    if (!nb_frames) {
        return NULL;
    }

    size_t payload_size = rand_uint(MIN_PL_SIZE, MAX_PL_SIZE + 1);
    unsigned *data = malloc(sizeof(unsigned) * (payload_size * 2 + 2));

    data[0] = rand_uint(MIN_SENSORS, MAX_SENSORS + 1);
    data[1] = payload_size;
    unsigned mean_ofs_x = rand_uint(10, 100);
    unsigned mean_ofs_y = rand_uint(10, 100);
    for (size_t i = 0; i < payload_size; ++i) {
        data[2 * i + 2] = rand_uint(0, 20) + mean_ofs_x;
        data[2 * i + 3] = rand_uint(0, 20) + mean_ofs_y;
    }

    --nb_frames;
    return data;
}

void discard_frame(unsigned **frame_ptr)
{
    free(*frame_ptr);
    *frame_ptr = NULL;
}