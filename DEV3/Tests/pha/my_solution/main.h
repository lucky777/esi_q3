#ifndef MAIN_H
#define MAIN_H

#include <iostream>

typedef struct Point {
    unsigned x;
    unsigned y;
} Point;

typedef struct SensorData {
    unsigned id;
    unsigned size;
    Point* points;
} SensorData;

struct SensorData* find_sensor(SensorData*, size_t, unsigned);
size_t aggregate(SensorData** sensor_data);
void display(SensorData*, size_t);
void discard_sensor_data(SensorData**, size_t);

#endif