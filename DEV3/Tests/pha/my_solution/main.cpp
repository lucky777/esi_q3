#include <iostream>
#include <stdlib.h>
#include "main.h"
#include "generator.h"
#include "lucky_tools.hpp"

using namespace std;

int main() {
    SensorData* sensor_data;
    size_t size = aggregate(&sensor_data);
    display(sensor_data, size);
    discard_sensor_data(&sensor_data, size);
    return 0;
}

struct SensorData* find_sensor(struct SensorData* sensor_data, size_t size, unsigned id) {
    for (int i=0; i<size; i++) {
        if (sensor_data[i].id == id)
            return &(sensor_data[i]);
    }
    return NULL;
}

size_t aggregate(SensorData** sensor_data) {
    size_t nb_sensors = 0;
    SensorData* tmp;

    *sensor_data = (SensorData*)malloc(10*sizeof(SensorData));
    log("memory allocated for sensor_data (10)");
    
    prepare_data();
    
    unsigned* new_frame = read_frame();

    while(new_frame != NULL) {
        tmp = find_sensor(*sensor_data, nb_sensors, new_frame[0]);
        if(tmp == NULL) {
            nb_sensors++;
            if(nb_sensors>10) {
                *sensor_data = (SensorData*)realloc(*sensor_data, (nb_sensors*sizeof(SensorData)));
                log("sensor_data memory reallocated (" + to_string(nb_sensors-1) + "->" + to_string(nb_sensors) +")");
            }
            (*sensor_data)[nb_sensors-1].id = new_frame[0];
            (*sensor_data)[nb_sensors-1].size = new_frame[1];
            (*sensor_data)[nb_sensors-1].points = (Point*)malloc(new_frame[1]*sizeof(Point));
            log("memory allocated for points (" + to_string(new_frame[1]) + ")");
            for(int i=0; i<new_frame[1]; i++) {
                (*sensor_data)[nb_sensors-1].points[i] = Point();
                (*sensor_data)[nb_sensors-1].points[i].x = new_frame[2+2*i];
                (*sensor_data)[nb_sensors-1].points[i].y = new_frame[3+2*i];
            }
        } else {
            //Nope tmp->size += frame[1];
            tmp->points = (Point*)realloc(tmp->points, ((tmp->size + new_frame[1]) * sizeof(Point)));
            log("points memory reallocated (" + to_string(tmp->size) + "->" + to_string(tmp->size+new_frame[1]) + ")");
            for(int i=0; i<new_frame[1]; i++) {
                tmp->points[i+tmp->size] = Point();
                tmp->points[i+tmp->size].x = new_frame[2 + 2*i];
                tmp->points[i+tmp->size].y = new_frame[3 + 2*i];
            }
            tmp->size += new_frame[1];
        }
        new_frame = read_frame();
    }

    return nb_sensors;
}

void display(SensorData* sensor_data, size_t size) {
    for(int i=0; i<size; i++) {
        cout << "sensor " << sensor_data[i].id << endl;
        cout << sensor_data[i].size << " points" << endl;
        for(int j=0; j<sensor_data[i].size; j++) {
            cout << "(" << sensor_data[i].points[j].x << ", " << sensor_data[i].points[j].y << ")" << endl;
        }
        cout << endl;
    }
}

void discard_sensor_data(SensorData** sensor_data, size_t size) {
    for(int i=0; i<size; i++) {
        if((*sensor_data)[i].size > 0) {
            free((*sensor_data)[i].points);
            log("points memory free");
        }
    }
    free(*sensor_data);
    log("sensor_data memory free");
    *sensor_data = NULL;
    cout << endl;
    log("ok");
    cout << endl << "bisou!" << endl << endl;
}