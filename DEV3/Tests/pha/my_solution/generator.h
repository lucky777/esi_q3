#ifndef GENERATOR_H
#define GENERATOR_H

#include <stdlib.h>

typedef unsigned *frame_ptr;

void prepare_data(void);

unsigned *read_frame(void);

void discard_frame(unsigned **frame_ptr);

#endif // GENERATOR_H
