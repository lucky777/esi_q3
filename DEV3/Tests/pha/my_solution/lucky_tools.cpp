#include <iostream>
#include <climits>
#include <thread>
#include <chrono>
#include "lucky_tools.hpp"

Color::Modifier::Modifier(Code pCode) : code(pCode) {}

void Color::Modifier::mod(Code pCode) {
    this->code = pCode;
}

std::string Color::Modifier::use() {
    return "\033[" + std::to_string(this->code) + "m";
}

std::ostream& Color::operator<<(std::ostream& os, const Color::Modifier& mod) {
    return os << "\033[" << mod.get_code() << "m";
}

Color::Code Color::Modifier::get_code() const {
    return this->code;
}

bool box(std::string txt) {
    return box(txt, def);
}

bool box(std::string txt, Color::Modifier color) {

    Color::Modifier col(Color::FG_DEFAULT);

    try {
        col.mod(color.get_code());
    } catch (...) {
        error("color  not found!");
    }

    std::cout << std::endl << col << "              ┌";

    for (int i=0; i<(txt.length()+6); i++) {
        std::cout << "─";
    }

    std::cout << "┐" << std::endl;
    std::cout << "              │   " << txt << "   │▏" << std::endl;
    std::cout << "              └";

    for (int i=0; i<(txt.length()+6); i++) {
        std::cout << "─";
    }

    std::cout << "┘▏" << std::endl << "               ";

    for (int i=0; i<(txt.length()+7); i++) {
        std::cout << "▔";
    }
    std::cout << def << std::endl;

    return true;
}

void clearscr() {
    #ifdef _WIN32
        system("cls");
    #elif defined(unix) || defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        puts("\x1b[2J\x1b[1;1H");
    //add some other OSes here if needed
    #else
        #error "OS not supported."
        //you can also throw an exception indicating the function can't be used
    #endif
}

void zzz(int ms) {
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void error(const std::string txt) {
    std::cerr << red << "[e] " << txt << def << std::endl;
}

void log(const std::string txt) {
    std::cerr << blue << "[i] " << txt << def << std::endl;
}

void enter() {
    std::cout << "press ENTER to continue..";
    while(getchar()!='\n');
    getchar();
    std::cout << std::endl;
}

bool ask_yn(const std::string txt) {
    std::string result;
    std::cout << txt << " [" << green << "Y" << def << "/" << red << "N" << def << "] > ";
    std::cin >> result;
    while (result != "Y" && result != "y" && result != "N" && result != "n") {
        error("enter 'y' or 'n'");
        std::cout << txt << " [" << green << "Y" << def << "/" << red << "N" << def << "] > ";
        std::cin >> result;
    }
    return (result == "Y" || result == "y");
}

int ask_int(int min = 0, int max = INT_MAX) {
    std::string str;
    int nb;
    std::cin >> str;
    try {
        nb = stoi(str);
    } catch (std::invalid_argument e){
        nb = min-1;
    }
    while(nb < min || nb > max) {
        error("invalid number: " + str);
        std::cout << "try again: " << std::endl << "> ";
        std::cin >> str;
        try {
            nb = stoi(str);
        } catch (std::invalid_argument e){
            nb = min-1;
        }
    }
    return nb;
}

void bye() {
    clearscr();
    box("    ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("b   ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("by  ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("bye ", blue);
    std::cout << std::flush;
    zzz(140);
    clearscr();
    box("bye!", blue);
    std::cout << std::flush;
    zzz(777);
    clearscr();
    std::cout << std::flush;
}