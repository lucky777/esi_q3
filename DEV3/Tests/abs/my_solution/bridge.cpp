#include <iostream>
#include <stdlib.h>
#include "bridge.h"
#include "lucky_tools.hpp"
using namespace std;

Int_Array* next_bridge(Int_Array* a) {

    static int* last_ref;
    static unsigned size_left;   //Taille restante du tableau  à parcourir

    bool bridge_found;
    unsigned bridge_start, bridge_end;

    int* ref;

    if(a != NULL) {     //Si a n'est pas NULL, on commence au début de a
        ref = a->arr;
        size_left = a->length; //Il reste tout le tableau à parcourir
    } else {
        ref = last_ref; //Si a est NULL on veut continuer à partir du dernier endroit enregistré
    }

    unsigned cpt = 0;

    bridge_found = false;

    while(!bridge_found && cpt < size_left-3) {  //Si on est à 3 places de la fin on ne pourra plus avoir de bridge (min 4)
        while(cpt < size_left-3 && ref[cpt] >= ref[cpt+1]) {    //On ignore tout ce qui n'est pas croissant au début
            cpt++;                                          //size_left-3 car on ne peut pas avoir un bridge qui commence 3 places avant la fin (min 4)
        }
        if(cpt == size_left-3) return NULL; //On est arrivé 3 places avant la fin, il ne reste plus de bridge on retourne NULL;

        //On a trouvé le début d'un bridge potentiel
        bridge_start = cpt;
        while(cpt < size_left-3 && ref[cpt] < ref[cpt+1]) cpt++;
        
        if(cpt == size_left-3) return NULL;

        if(ref[cpt] == ref[cpt+1]) {   //On a bien trouvé un milieu de bridge
            cpt++;
            while(cpt < size_left-2 && ref[cpt] == ref[cpt+1]) {  //On vérifie combien de fois il y a le même nombre qui suit
                cpt++;
            }
            if(cpt == size_left-2) return NULL; //On est arrivé 2 places avant la fin, il ne reste plus de bridge on retourne NULL;

            //On a trouvé un milieu de bridge valide, il faut vérifier la fin maintenant:
            if(ref[cpt] > ref[cpt+1]) {  //On a bien trouvé une fin de bridge
                bridge_found = true;
                cpt++;
                while(cpt < size_left-1 && ref[cpt] > ref[cpt+1]) {  //On vérifie combien de nombres sont décroissants après
                    cpt++;
                }
                bridge_end = cpt;
            }
        }
    }

    if(!bridge_found) {
        return NULL;
    } else {
        Int_Array* result;

        result = (Int_Array*)malloc(sizeof(Int_Array)); //On alloue pour l'int_array
        result->length = (bridge_end - bridge_start + 1); //Nombre de nombres trouvés
        result->arr = (int*)malloc(result->length * sizeof(int));  //On alloue pour le tableau d'int

        for(int i=0; i<result->length; i++) {
            result->arr[i] = ref[bridge_start+i]; //On ajoute les nombres du pont
        }
        
        last_ref = &(ref[bridge_end]); //On enregistre le dernier emplacement
        size_left = size_left - bridge_end; //On enregistre la taille restante
        return result;
    }
}