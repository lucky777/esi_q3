#ifndef INT_ARRAY_H
#define INT_ARRAY_H
#include <stdlib.h>

typedef struct Int_Array {
    int* arr;
    size_t length;
} Int_Array;

void create_array(Int_Array*, int*, size_t);
void print_arr(const Int_Array*);
void free_arr(Int_Array**);

#endif