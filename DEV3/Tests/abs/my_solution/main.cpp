#include <iostream>
#include <stdlib.h>
#include "int_array.h"
#include "bridge.h"
#include "lucky_tools.hpp"
using namespace std;

int main() {
    Int_Array ia;
    int tmp[] = {4, 4, 1, 2, 1, 2, 2, 1, 2, 3, 3, 3, 3, 2, 1, 0, -1, 0, 1, 1};
    create_array(&ia, tmp, 20);

    cout << endl << "Array:" << endl;

    print_arr(&ia);

    Int_Array* bridge;
    bridge = next_bridge(&ia);

    while(bridge != NULL) {
        cout << endl;
        log("Bridge found:");
        cout << green;
        print_arr(bridge);
        cout << def;
        bridge = next_bridge(NULL);
    }

    box("No more bridge found");

    return 0;
}