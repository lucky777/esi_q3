#include <iostream>
#include <stdlib.h>
#include "int_array.h"

void create_array(Int_Array* a, int* tmp, size_t size) {
    a->arr = (int*)malloc(size * sizeof(int));
    for(int i=0; i<size; i++) {
        a->arr[i] = tmp[i];
    }
    a->length = size;
}

void print_arr(const Int_Array* a) {
    std::cout << "[";
    for(int i=0; i<a->length;i++) {
        std::cout << a->arr[i];
        if(i<a->length - 1) {   //Si on est pas au dernier élément on affiche ', '
            std::cout << ", ";
        }
    }
    std::cout << "]" << std::endl;
}

void free_arr(Int_Array** a) {
    free((*a)->arr);
    (*a)->arr = NULL;
}