#include <stdlib.h>
#include <string.h>
#include <errno.h>

void swap(void * lhs, void * rhs, size_t size)
{
    void * tmp = malloc(size);
    if (tmp != NULL)
    {
        memcpy(tmp, lhs, size);
        memcpy(lhs, rhs, size);
        memcpy(rhs, tmp, size);
        free(tmp);
    }
    else
    {
        errno = 100;
    }
}
