#ifndef RANDOM_H
#define RANDOM_H

// min inclus et max exclus
//
// si min_excl == max_excl : errno mis à 230, -1 retourné
//
// si max_excl < min_incl : ils sont permutés
//
// à partir d'ici min_incl et max_excl sont tels que
//           min_incl < max_excl
//
// + si max_excl <= 0 : errno mis à 231, -1 retourné
//
// + si 0 <= min_incl < max_excl : une valeur dans
//           [min_incl, max_excl) est retournée
//
// + si min_incl < 0 <= max_excl : une valeur dans
//           [0, max_excl) est retournée
//
int rand_int_old(int min_incl, int max_excl);

// min inclus et max exclus
//
// si min_excl == max_excl : errno mis à 230, -1 retourné
//
// si max_excl < min_incl : ils sont permutés
//
// à partir d'ici min_incl et max_excl sont tels que
//           min_incl < max_excl
//
// une valeur dans [min_incl, max_excl) est retournée
//
int rand_int(int min_incl, int max_excl);

#endif // RANDOM_H
