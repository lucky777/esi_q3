#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "generator.h"
#include "random.h"

#define MIN_COUNT 2000
#define MAX_COUNT 8000

#define MIN_VALUE 0
#define MAX_VALUE RAND_MAX

#define TIME_RAND 1

struct Data * data(unsigned * count)
{
    static bool first_time = true;
    static struct Data * result = NULL;

    if (first_time)
    {
#if TIME_RAND
        srand((unsigned) time(NULL));
#endif
        *count = rand_int(MIN_COUNT, MAX_COUNT);
        result = malloc(*count * sizeof (struct Data));
        struct Data * go = result;

        for (unsigned u = 0; u < *count; ++u)
        {
            struct Data tmp;
            unsigned base [] = { 8, 10, 16 };
            unsigned idx = rand_int(0, sizeof base / sizeof * base);
            tmp.base = base[idx];
            const char * base_s [] = { "%o", "%d", "%X" };
            sprintf(tmp.number, base_s[idx], rand_int(MIN_VALUE, MAX_VALUE));
            if (!rand_int(0, 10))
            {
                tmp.number[rand_int(0, strlen(tmp.number))] =
                    tmp.base == 16 ?
                    rand_int('G', 'Z' + 1) :
                    tmp.base == 8 ?
                    rand_int('8', '9' + 1) :
                    rand_int('A', 'F' + 1) ;
            }
            *go++ = tmp;
        }

        first_time = false;
    }
    else // !first_time
    {
        free(result);
        result = NULL;
    }

    return result;
}
