#ifndef SWAP_H
#define SWAP_H

#include <stddef.h>

// errno mis à 100 si l'échange échoue
void swap(void * lhs, void * rhs, size_t size);

#endif // SWAP_H
