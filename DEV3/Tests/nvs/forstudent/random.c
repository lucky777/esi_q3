#include <stdlib.h>
#include <errno.h>

#include "swap.h"

int rand_int(int min_incl, int max_excl)
{
    int result = -1;

    if (min_incl == max_excl)
    {
        errno = 230;
    }
    else
    {
        errno = 0;
        if (max_excl < min_incl)
        {
            swap(&min_incl, &max_excl, sizeof min_incl);
        }
        result = min_incl + rand() % (max_excl - min_incl);
    }

    return result;
}

int rand_int_old(int min_incl, int max_excl)
{
    int result = -1;

    if (max_excl <= 0)
    {
        errno = 231;
    }
    else
    {
        min_incl = min_incl < 0 ? 0 : min_incl;
        result = rand_int(min_incl, max_excl);
    }

    return result;
}
