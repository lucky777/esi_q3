// DEV3 Labo 1

#include "mathesi.h"
#include <iostream>
#include <iomanip>

// Exercice 1.1
bool isPrime(unsigned nb) {
    
    if (nb == 2) {
        return true;
    } else if (nb == 1 || nb%2 == 0) {
        return false;
    }

    // i*i < nb
    //Revient à la même chose que:
    // i < racine(nb)
    // Optimisation du temps de calcul du processeur

    for (int i=3; i*i<nb; i+=2) {
        if (nb%i==0) {
            return false;
        }
    }
    return true;
}

// Exercice 1.2
void printPrimes(unsigned nb1, unsigned nb2) {
    if (nb1 >= nb2) {
        std::cout << "First number must be lower than second number\n";
    } else {
        unsigned cpt = 0;
        for (int i=nb1; i<=nb2; i++) {
            if (cpt >= 9) {
                std::cout << std::endl;
                cpt = 0;
            }
            if (isPrime(i)) {
                std::cout << std::setw(5) << i;
            } else {
                std::cout << "    .";
            }
            cpt++;
        }

        // Continue if tab not filled
        /*
        if (cpt < 9) {
            for (int i=nb2+1; i<=nb2+(9-cpt); i++) {
                if (isPrime(i)) {
                    std::cout << "%5d", i);
                } else {
                    std::cout << "    .");
                }
            }
        }
        */
        std::cout << std::endl;
    }
}

// Exercice 1.3
unsigned nextPrime(unsigned nb) {
    while (true) {
        nb++;
        if (isPrime(nb)) {
            return nb;
        }
    }
}

void printPrimeFactor(unsigned nb, bool showPower) {
    unsigned powCpt, div=2;
    std::cout << nb << " = ";
    while (nb > 1) {
        while (nb%div != 0) {
            div = nextPrime(div);
        }
        if (showPower) {
            powCpt=1;
            nb = nb/div;
            while (nb%div == 0) {
                powCpt++;
                nb = nb/div;
            }
            if (powCpt == 1) {
                std::cout << div;
            } else {
                std::cout << div << "^" << powCpt;
            }
        } else {
            std::cout << div;
            nb = nb/div;
        }
        if (nb>1) {
            std::cout << " x ";
        }
    }
    std::cout << std::endl;
}

// Exercice 1.4
unsigned gcd(unsigned a, unsigned b) {
    if (a<b) {
        unsigned tmp = a;
        a = b;
        b = tmp;
    }
    if (b == 0) {
        return a;
    } else {
        return gcd(b, a%b);
    }
}

// Exercice 1.5
void printGcd() {
    for (unsigned i=423; i<=438; i+=3) {
        std::cout << "gcd(" << i << ", 135) = " << std::setw(2) << gcd(i, 135) << std::endl
               << " | gcd(" << i << ", 130) = " << std::setw(2) << gcd(i, 130) << std::endl
               << " | gcd(" << i << ", 125) = " << std::setw(2) << gcd(i, 125) << std::endl;
    }
}