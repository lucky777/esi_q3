#ifndef POLY_INT_H
#define POLY_INT_H

#include <vector>

class Polynomial
{
    std::vector<int> _coefficients;
    unsigned _degree;
};

#endif