// DEV3 Labo 1

#include "mathesi.h"
#include <stdio.h>
#include <stdlib.h>

// Exercice 1.1
bool isPrime(unsigned nb) {
    
    if (nb == 2) {
        return true;
    } else if (nb == 1 || nb%2 == 0) {
        return false;
    }

    // i*i < nb
    //Revient à la même chose que:
    // i < racine(nb)
    // Optimisation du temps de calcul du processeur

    for (int i=3; i*i<nb; i+=2) {
        if (nb%i==0) {
            return false;
        }
    }
    return true;
}

// Exercice 1.2
void printPrimes(unsigned nb1, unsigned nb2) {
    if (nb1 >= nb2) {
        printf("First number must be lower than second number\n");
    } else {
        unsigned cpt = 0;
        for (int i=nb1; i<=nb2; i++) {
            if (cpt >= 9) {
                printf("\n");
                cpt = 0;
            }
            if (isPrime(i)) {
                printf("%5d", i);
            } else {
                printf("    .");
            }
            cpt++;
        }

        // Continue if tab not filled
        /*
        if (cpt < 9) {
            for (int i=nb2+1; i<=nb2+(9-cpt); i++) {
                if (isPrime(i)) {
                    printf("%5d", i);
                } else {
                    printf("    .");
                }
            }
        }
        */
        printf("\n");
    }
}

// Exercice 1.3
unsigned nextPrime(unsigned nb) {
    while (true) {
        nb++;
        if (isPrime(nb)) {
            return nb;
        }
    }
}

void printPrimeFactor(unsigned nb, bool showPower) {
    unsigned powCpt, div=2;
    printf("%d = ", nb);
    while (nb > 1) {
        while (nb%div != 0) {
            div = nextPrime(div);
        }
        if (showPower) {
            powCpt=1;
            nb = nb/div;
            while (nb%div == 0) {
                powCpt++;
                nb = nb/div;
            }
            if (powCpt == 1) {
                printf("%d", div);
            } else {
                printf("%d^%d", div, powCpt);
            }
        } else {
            printf("%d", div);
            nb = nb/div;
        }
        if (nb>1) {
            printf(" x ");
        }
    }
    printf("\n");
}

// Exercice 1.4
unsigned gcd(unsigned a, unsigned b) {
    if (a<b) {
        unsigned tmp = a;
        a = b;
        b = tmp;
    }
    if (b == 0) {
        return a;
    } else {
        return gcd(b, a%b);
    }
}

// Exercice 1.5
void printGcd() {
    for (unsigned i=423; i<=438; i+=3) {
        printf("gcd(%d, 135) = %2d | gcd(%d, 130) = %2d | gcd(%d, 125) = %2d\n", i, gcd(i, 135), i, gcd(i, 130), i, gcd(i, 125));
    }
}