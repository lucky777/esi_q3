// DEV3 Labo 1

#include "mathesi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

// Exercice 1.1
bool isPrime(unsigned nb) {
    
    if (nb == 2) {
        return true;
    } else if (nb == 1 || nb%2 == 0) {
        return false;
    }

    // i*i < nb
    //Revient à la même chose que:
    // i < racine(nb)
    // Optimisation du temps de calcul du processeur

    for (int i=3; i*i<nb; i+=2) {
        if (nb%i==0) {
            return false;
        }
    }
    return true;
}

// Exercice 1.2
void printPrimes(unsigned nb1, unsigned nb2) {
    if (nb1 >= nb2) {
        printf("First number must be lower than second number\n");
    } else {
        unsigned cpt = 0;
        for (int i=nb1; i<=nb2; i++) {
            if (cpt >= 9) {
                printf("\n");
                cpt = 0;
            }
            if (isPrime(i)) {
                printf("%5d", i);
            } else {
                printf("    .");
            }
            cpt++;
        }

        // Continue if tab not filled
        /*
        if (cpt < 9) {
            for (int i=nb2+1; i<=nb2+(9-cpt); i++) {
                if (isPrime(i)) {
                    printf("%5d", i);
                } else {
                    printf("    .");
                }
            }
        }
        */
        printf("\n");
    }
}

// Exercice 1.3
unsigned nextPrime(unsigned nb) {
    while (true) {
        nb++;
        if (isPrime(nb)) {
            return nb;
        }
    }
}

void printPrimeFactor(unsigned nb, bool showPower) {
    unsigned powCpt, div=2;
    printf("%d = ", nb);
    while (nb > 1) {
        while (nb%div != 0) {
            div = nextPrime(div);
        }
        if (showPower) {
            powCpt=1;
            nb = nb/div;
            while (nb%div == 0) {
                powCpt++;
                nb = nb/div;
            }
            if (powCpt == 1) {
                printf("%d", div);
            } else {
                printf("%d^%d", div, powCpt);
            }
        } else {
            printf("%d", div);
            nb = nb/div;
        }
        if (nb>1) {
            printf(" x ");
        }
    }
    printf("\n");
}

// Exercice 1.4
unsigned gcd(unsigned a, unsigned b) {
    if (a<b) {
        unsigned tmp = a;
        a = b;
        b = tmp;
    }
    if (b == 0) {
        return a;
    } else {
        return gcd(b, a%b);
    }
}

// Exercice 1.5
void printGcd() {
    for (unsigned i=423; i<=438; i+=3) {
        printf("gcd(%d, 135) = %2d | gcd(%d, 130) = %2d | gcd(%d, 125) = %2d\n", i, gcd(i, 135), i, gcd(i, 130), i, gcd(i, 125));
    }
}

//Labo 4

//Exercice 4.1
unsigned* primeFactorA(unsigned* count, unsigned nb) {

    unsigned cpt=0, div=2;
    unsigned* primes = (unsigned*)malloc(nb * sizeof(unsigned));

    while(nb > 1) {
        while(nb%div != 0) {
            div = nextPrime(div);
        }
        while(nb%div == 0) {
            primes[cpt] = div;
            cpt++;
            nb = nb/div;
        }
    }

    *count = cpt;

    primes = (unsigned*)realloc(primes, (size_t)cpt);

    return primes;
}

//Exercice 4.2
unsigned primeFactorB(unsigned** factor, unsigned** multiplicity, unsigned nb) {
    //'unsigned factor[]' => 'unsigned* factor'
    //Pour passer un pointeur de tableau en paramètre dans une fonction
    //Il faudra donc passer un pointeur de pointeur: 'unsigned** factor'
    //Ainsi on pourra récupérer la valeur hors de la fonction
    //Car on n'a pas créé une copie du tableau, mais on a donné directement son adresse

    //nb:           nombre            84
    //factor:       nombres premiers {2, 3, 7}
    //multiplicity: puissances       {2, 1, 1}

    unsigned cpt=0, pow_cpt, div=2;
    
    *factor = (unsigned*)malloc(nb * sizeof(unsigned));
    *multiplicity = (unsigned*)malloc(nb * sizeof(unsigned));

    while(nb > 1) {
        while(nb%div != 0) {
            div = nextPrime(div);
        }

        (*factor)[cpt] = div;

        pow_cpt=0;

        while(nb%div == 0) {
            pow_cpt++;
            nb = nb/div;
        }

        (*multiplicity)[cpt] = pow_cpt;

        cpt++;
    }

    *factor = (unsigned*)realloc(*factor, (size_t)cpt);
    *multiplicity = (unsigned*)realloc(*multiplicity, (size_t)cpt);

    return cpt;
}

//Exercice 4.3
struct PrimeFactor* primeFactorC(unsigned* count, unsigned nb) {

    struct PrimeFactor* result;
    result = (struct PrimeFactor*)malloc(nb * sizeof(struct PrimeFactor));

    unsigned cpt=0, pow_cpt, div=2;

    while(nb > 1) {
        while(nb%div != 0) {
            div = nextPrime(div);
        }

        result[cpt].value = div;

        pow_cpt=0;

        while(nb%div == 0) {
            pow_cpt++;
            nb = nb/div;
        }

        result[cpt].multiplicity = pow_cpt;

        cpt++;
    }

    result = (struct PrimeFactor*)realloc(result, (size_t)cpt);

    *count = cpt;

    return result;
}

//Exercice 4.4
void primeFactorD(struct PrimeFactorization* pf) {
    //pf.nb = 84
    //pf.count = 0
    //pf.primeFactors = NULL
    pf->primeFactors = primeFactorC(&pf->count, pf->nb);
    //pf.nb = 84
    //pf.count = 3
    //pf.primeFactors = {{2, 2}, {3, 1}, {7, 1}}
}