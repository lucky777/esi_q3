// DEV3 Labo 3

#include "td3.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
    char txt[14] =  "ok!";
    const char s1[] = {'b', 'l', 'b', 'l', '\0'};
    char s2[] = {'b', 'l', 'b', 'l', '\0'};
    char s3[] = {'b', 'l', 'b', 'l', '!', '\0'};
    char dest[15] = "..............";
    char dest2[15] = "..............";
    char dest3[15] = "..............";
    char src[] = "blbl";
    char test[] = "This is a test! blbl :)";
    printf("%lu char in %s\n", strlen(txt), txt);
    printf("comparaison between %s and %s = %d\n", s1, s2, strcmp(s1, s2));
    printf("comparaison between %s and %s = %d\n", s1, s3, strcmp(s1, s3));
    printf("comparaison between %s and %s = %d\n", "test", "test", strcmp("test", "test"));
    printf("Copy of %s = %s\n", src, strcpy(dest, src));
    printf("Copy of %s with 3 chars = %s\n", src, strncpy(dest2, src, 3));
    printf("Copy of %s with 7 chars = %s\n", src, strncpy(dest3, src, 7));
    printf("Concatenation of %s and %s = ", txt, s1);
    printf("%s\n", strcat(txt, s1));
    printf("Concatenation of 3 chars of %s and %s = ", txt, s1);
    printf("%s\n", strncat(txt, s1, 3));
    printf("Strtok on: %s\n", test);
    char* token = strtok(test, " ");
    while(token) {
        printf("- %s\n", token);
        token = strtok(NULL, " ");
    }
    return 0;
}

// Exercice 3.1
size_t strlen(const char *txt) {
    size_t cpt = 0;
    while (txt[cpt] != '\0') {
        cpt++;
    }
    return cpt;
}

int strcmp(const char *lhs, const char *rhs) {
    unsigned cpt = 0;
    while (lhs[cpt] == rhs[cpt]) {
        if (lhs[cpt] == '\0' && rhs[cpt] == '\0') {
            return 0;
        }
        cpt++;
    }
    if (lhs[cpt] < rhs[cpt]) {
        return -1;
    } else {
        return 1;
    }
}

char* strcpy(char* dest, const char* src) {
    unsigned cpt=0;
    while(src[cpt] != '\0') {
        dest[cpt] = src[cpt];
        cpt++;
    }
    dest[cpt] = '\0';
    return dest;
}

char* strncpy(char* dest, const char* src, size_t nb) {
    bool end_src = false;
    for(int i=0; i<nb; i++) {
        if (end_src) {
            dest[i] = '\0';
        } else {
            dest[i] = src[i];
            end_src = (src[i] == '\0');
        }
    }
    return dest;
}

char* strcat(char* dest, const char* src) {
    unsigned cpt_dest=0, cpt_src=0;
    while(dest[cpt_dest] != '\0') {
        cpt_dest++;
    }
    while(src[cpt_src] != '\0') {
        dest[cpt_dest] = src[cpt_src];
        cpt_src++;
        cpt_dest++;
    }
    dest[cpt_dest] = '\0';
    return dest;
}

char* strncat(char* dest, const char* src, size_t nb) {
    unsigned cpt_dest=0;
    while(dest[cpt_dest] != '\0') {
        cpt_dest++;
    }
    for(int i=0; i<nb; i++) {
        if(src[i] == '\0') {
            break;
        }
        dest[cpt_dest] = src[i];
        cpt_dest++;
    }
    dest[cpt_dest] = '\0';
    return dest;
}

char* strtok(char* src, const char* delim) {
    
    static char* last_index;
    unsigned cpt=0;
    char* index;

    if (src != NULL) {
        index = src;
    } else {
        index = last_index;
    }
    //Y a-t-il un mot à sortir?
    while(index[cpt] == *delim && index[cpt] != '\0') {
        cpt++;
    }
    if (index[cpt] == '\0') {
        return NULL; //On est arrivé au bout, il n'y a que des délimiteurs (Rien à sortir)
    }
    //On veut sortir le mot
    unsigned save_cpt = cpt; //On sauve l'emplacement du début du mot à renvoyer
    while(index[cpt] != *delim && index[cpt] != '\0') {
        cpt++;
    }
    if (index[cpt] == *delim) {
        index[cpt] = '\0';
        last_index = &index[cpt+1]; //On sauvegarde le prochain emplacement
    } else {
        last_index = &index[cpt];
    }

    return &index[save_cpt];
}





/*


if (src != NULL) {
    x = src
} else {
    x = last_index
}

x. JIFE%IGM%EZ
x = griùmigrùe
x print gjiremohgiremg
igmreimogre

*/