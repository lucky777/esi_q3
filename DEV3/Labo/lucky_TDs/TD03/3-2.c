#include "3-2.h"
#include "mathesi.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    /*
    printf("argc=%d\n", argc);

    for(int i=0; i<argc; i++) {
        printf("argv[%d]=%s\n", i, argv[i]);
    }
    */

    unsigned nb;

    if(argc < 1) {
        printf("FATAL ERROR: UNKNOWN");
        exit(1);
    }
    if(argc == 1) {
        printf("error: no arguments found\n\tTry: ./3-2 777\n");
        exit(1);
    }
    if(argc > 3) {
        printf("error: too many arguments\n\t./3-2 [-v] [integer]\n");
        exit(1);
    }

    if(argc == 2) {
        if(atoi(argv[1]) == 0) {
            printf("error: please enter a valid integer greater than 0\n");
            exit(1);
        }
        nb = (unsigned)atoi(argv[1]);
        printPrimeFactor(nb, false);
    } else {
        if(atoi(argv[1]) == 0 && atoi(argv[2]) == 0) {
            printf("error: please enter a valid integer greater than 0\n");
            exit(1);
        }
        if(lucky_strcmp(argv[1], "-v") != 0 && lucky_strcmp(argv[2], "-v") != 0) {
            printf("error: invalid argument\n\tPlease use -v\n");
            exit(1);
        }
        if(atoi(argv[1]) != 0) {
            nb = (unsigned)atoi(argv[1]);
        } else {
            nb = (unsigned)atoi(argv[2]);
        }
        printPrimeFactor(nb, true);
    }
    return 0;
}

int lucky_strcmp(const char *lhs, const char *rhs) {
    unsigned cpt = 0;
    while (lhs[cpt] == rhs[cpt]) {
        if (lhs[cpt] == '\0' && rhs[cpt] == '\0') {
            return 0;
        }
        cpt++;
    }
    if (lhs[cpt] < rhs[cpt]) {
        return -1;
    } else {
        return 1;
    }
}