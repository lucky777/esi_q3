#ifndef TD2
#define TD2

#include <stdbool.h>
#include <stddef.h>

void arrayIntPrint(const int [], unsigned);
void arrayIntSort(int [], unsigned, bool);
void arrayIntSort2(int [], size_t);
int compare_ints_ascending(const void*, const void*);
int compare_ints_descending(const void*, const void*);
int compare_ints_mod3(const void*, const void*);
void arrayIntSortGeneric(int data [], unsigned nbElem, int (*comp)(const void *, const void *));

#endif