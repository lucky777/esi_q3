#include "labo5_node.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
    printf("60 crédits!\n");
    return 0;
}

/*!
 * \brief Creates a node
 *
 * \param value of value_t type, is a double
 * \return a struct SLNode pointer
 */
struct SLNode * newSLN(value_t value) {
    struct SLNode * node;
    node->value = value;
    node->next = NULL;
    return node;
}

/*!
 * \brief Deletes a node. Frees its memory
 */
void deleteSLN(struct SLNode * * adpSLN) {
    free(*adpSLN);
}

/*!
 * \brief Returns the next node.
 * \param pSLN a struc node pointer we want to know the next of
 * \return a struct node pointer, the following node of param
 */
struct SLNode * nextSLN(const struct SLNode * pSLN) {
    return pSLN->next;
}

/*!
 * \brief Set the reference of the next node, in the 'next' attribute of the current node
 *
 * \param pSLN the current node
 *
 * \param pNewNext the following node
 */
void setNextSLN(struct SLNode * pSLN, struct SLNode * pNewNext) {
    pSLN->next = pNewNext;
}

/*!
 * \brief Gets the value of a given node
 *
 * \param pSLN the node we want the value of
 *
 * \return a valuet_t type, the value of the given node
 */
value_t valueSLN(const struct SLNode * pSLN) {
    return pSLN->value;
}

/*!
 * \brief Changes the value of a given node
 *
 * \param pSLN a struct node, the node we want to change the value of
 *
 * \param newValue a value_t type, the new value we want to put in the given node
 */
void setValueSLN(struct SLNode * pSLN, value_t newValue) {
    pSLN->value = newValue;
}

/*!
 * \brief Accès à un élément suivant en position donnée.
 *
 * \param pSLN adresse du struct SLNode dont on désire accèder à
 *             un suivant.
 * \param distance position de l'élément désiré.
 *
 * \return adresse de l'élément `distance` positions après celui
 *         d'adresse `pSLN` ou `NULL` s'il n'y en a pas.
 */
struct SLNode * forwardSLN(struct SLNode * pSLN, size_t distance) {
    if (distance == 0) {
        return pSLN;
    }
    return forwardSLN(pSLN->next, distance--);
}