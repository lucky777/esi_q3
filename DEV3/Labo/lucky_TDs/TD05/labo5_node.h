#ifndef LABO5_NODE
#define LABO5_NODE
#include "slnode.h"
#include "value_t.h"
#include "slnode_utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct SLNode * newSLN(value_t value);
void deleteSLN(struct SLNode * * adpSLN);
struct SLNode * nextSLN(const struct SLNode * pSLN);
void setNextSLN(struct SLNode * pSLN, struct SLNode * pNewNext);
value_t valueSLN(const struct SLNode * pSLN);
void setValueSLN(struct SLNode * pSLN, value_t newValue);

#endif