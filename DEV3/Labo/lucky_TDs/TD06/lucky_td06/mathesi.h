#ifndef MATHESI
#define MATHESI
#include <stdbool.h>
#include <iostream>
#include <iomanip>

bool isPrime(unsigned);
void printPrimes(unsigned, unsigned);
unsigned nextPrime(unsigned);
void printPrimeFactor(unsigned, bool);
unsigned gcd(unsigned, unsigned);
void printGcd();

std::pair<int, int> euclidianDivision(int dividend, int divisor);

#endif