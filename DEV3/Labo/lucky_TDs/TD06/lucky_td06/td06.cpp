#include "mathesi.h"
#include <iostream>

int main() {
    std::cout << "11 isPrime: " << (isPrime(11) ? "True" : "False") << std::endl ;
    printPrimes(200, 349);

    for (int i = 1; i <= 42; i++) {
        std::pair<int, int> foo = euclidianDivision(42, i);

        std::cout << "42 = " << foo.first << " * " << i << " + " << foo.second;
        std::cout << std::endl;
    }

    return 0;
}