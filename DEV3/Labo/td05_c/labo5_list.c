#include "labo5_list.h"
#include "labo5_node.h"
#include "slcircularlist.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
    printf("Tu as mérité un chocolat!\n");
    return 0;
}

struct SLCircularList * newSLCL(void) {
    
}

void deleteSLCL(struct SLCircularList * * adpSLCL);

void clearSLCL(struct SLCircularList * pSLCL);

struct SLNode * entrySLCL(const struct SLCircularList * pSLCL);

bool emptySLCL(const struct SLCircularList * pSLCL);

struct SLNode * pushSLCL(struct SLCircularList * pSLCL,
                         value_t value);

struct SLNode * insertSLCL(struct SLCircularList * pSLCL,
                           struct SLNode * pSLN,
                           value_t value);

struct SLNode * popSLCL(struct SLCircularList * pSLCL);

struct SLNode * eraseSLCL(struct SLCircularList * pSLCL,
                          struct SLNode * pSLN);